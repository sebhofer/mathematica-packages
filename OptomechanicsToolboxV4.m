(* ::Package:: *)

(* ::Title:: *)
(*Optomechanics Toolbox V4*)


(* :Title: OptomechanicsToolboxV4.m  *)

(* :Mathematica Version: Mathematica 9 *)

(* :Author: Sebastian Hofer *)

(* :Package Version: 0.1 *)

(* :Discussion:

*)


(* ::Text:: *)
(**)


(* ::Section:: *)
(*Package Setup Begin*)


BeginPackage["OptomechanicsToolboxV4`",{"GeneralToolbox`","SystemModel`","LinearSystems`"}];


optomechanicalCavity::usage="optomechanicalCavity[{\[Omega],\[CapitalDelta],g,\[Kappa],\[Gamma],\!\(\*SubscriptBox[\(n\), \(mech\)]\),\!\(\*SubscriptBox[\(n\), \(opt\)]\)}] returns systemModel for an optomechanical cavity system."
(*Options:
- Hamiltonian
  - \"NonDemolition\": Full linear coupling
  - \"BeamSplitter\": beam-splitter dynamics
  - \"DownConversion\": down-conversion dynamics
- Damping
  - \"BrownianMotion\"
  - \"Symmetric\"
- Measurement: True, False
- Feedback:
  - \"Full\"
  - \"Mechanical\"
  - \"Optical\"
  - False*)


angularFrequency::usage="angularFrequency[\[Lambda]] gives the angular frequency of light of wavelength \[Lambda].";
photonFlux::usage="photonFlux[P,\[Omega]] gives the photon flux of light of power P at a frequency \[Omega].";
cavityDecayRate::usage="cavityDecayRate[F,L] gives cavity decay rate of optical cavity with Finesse F and length L.";
cavityFinesse::usage="cavityFinesse[\[Kappa],L] gives Finesse of optical cavity with decay rate \[Kappa] and length L.";
singlePhotonCouplingRate::usage="singlePhotonCouplingRate[\!\(\*SubscriptBox[\(\[Omega]\), \(c\)]\),\!\(\*SubscriptBox[\(\[Omega]\), \(m\)]\),m,L] gives the single photon coupling strength of an optomechanical cavity with a resonance frequency \!\(\*SubscriptBox[\(\[Omega]\), \(c\)]\) with length L and a mechanical oscillator with mass m.";
linearCouplingRate::usage="linearCouplingRate[P,\!\(\*SubscriptBox[\(\[Omega]\), \(\(0\)\(,\)\)]\)\[CapitalDelta],\[Kappa],\!\(\*SubscriptBox[\(g\), \(0\)]\)] gives interaction strengh of a linearized interaction \!\(\*SubscriptBox[\(H\), \(om\)]\)=2\[CenterDot]\!\(\*SubscriptBox[\(x\), \(m\)]\)\!\(\*SubscriptBox[\(x\), \(c\)]\) for input power P, detuning \[CapitalDelta], cavity decay \[Kappa] and single photon coupling \!\(\*SubscriptBox[\(g\), \(0\)]\).";
zeroPointMotion::usage="zeroPointMotion[\[Omega],m] gives the ground state extension of a harmonic oscillator with resonance frequency \[Omega] and mass m.";
bosonicOccupationNumber::usage="bosonicOccupationNumber[\[Omega],T] gives the mean occupation number for Bose-Einstein statistics.";


Begin["`Private`"];


(* ::Section:: *)
(*Physical parameters*)


(*physical constants*)
SOL=299792458; (*m/s*)
PCR=1.054571628251774`*^-34; (*J s*)
BMC=1.3806504`*^-23; (*J/K*)


angularFrequency[\[Lambda]_]:=2Pi SOL/\[Lambda];

photonFlux[P_,\[Omega]0_]:=P/(PCR*\[Omega]0);

cavityDecayRate[FN_,L_]:=SOL Pi/(2L FN);
cavityFinesse[\[Kappa]_,L_]:=SOL Pi/(2L \[Kappa]);

singlePhotonCouplingRate[\[Omega]c_,\[Omega]m_,meff_,L_]:=\[Omega]c/L*zeroPointMotion[\[Omega]m,meff];
linearCouplingRate[P_,\[Omega]0_,\[CapitalDelta]_,\[Kappa]_,g0_]:=g0*Sqrt[2\[Kappa] photonFlux[P,\[Omega]0]]/Sqrt[\[CapitalDelta]^2+\[Kappa]^2];

bosonicOccupationNumber[\[Omega]m_,T_]:=(Exp[PCR \[Omega]m/(BMC*T)]-1)^(-1);
zeroPointMotion[\[Omega]m_,m_]:=Sqrt[PCR/(m*\[Omega]m)];

(*aliases*)
inputPhotonFlux=photonFlux;
omCouplingRate=linearCouplingRate;


(* ::Section:: *)
(*Transformation matrices*)


TMQA::usage="Transformation from quadratures to amplitudes.";
TMAQ::usage="Transformation from amplitudes to quadratures.";
TMRot::usage="Multidimensional rotation matrix.";
TMId::usage="Identity matrix.";

TM1=1/Sqrt@2*{{1,I},{1,-I}};(*transformation from quadratures -> amplitudes*)
TMQA[nrmodes_]:=blockDiagonalMatrix@Table[TM1,{i,1,nrmodes}];
TMAQ[]:=ConjugateTranspose@TMQA[];
TMAQ[nrmodes_]:=ConjugateTranspose@TMQA[nrmodes];
TMRot[angles__]:=blockDiagonalMatrix[RotationMatrix/@{angles}];
TMId[nrmodes_]:=IdentityMatrix[nrmodes];


(* ::Subsection:: *)
(*Standard Dynamics*)


dampingMatrixIdentifiers={"Symmetric","BrownianMotion"};
DQ[1][\[Gamma]_,\[Kappa]_]=DiagonalMatrix[{\[Gamma],\[Gamma],\[Kappa],\[Kappa]}];(*\[Gamma]=amplitude decay*)
DQ[2][\[Gamma]_,\[Kappa]_]=DiagonalMatrix[{0,2\[Gamma],\[Kappa],\[Kappa]}];(*2\[Gamma]=power decay*)
dampingMatrix[\[Gamma]_,\[Kappa]_,options:OptionsPattern[]]:=
	Block[{opt=OptionValue[Damping]},
		If[opt==Automatic,opt=ReplaceAll[Damping,Options[dampingMatrix]]];
		Switch[opt,
			Evaluate@dampingMatrixIdentifiers[[1]],DQ[1][\[Gamma],\[Kappa]],
			Evaluate@dampingMatrixIdentifiers[[2]],DQ[2][\[Gamma],\[Kappa]],
			_,Message[dampingMatrix::wid];Throw[wid]]
	];
Options[dampingMatrix]={Damping->"BrownianMotion"};
dampingMatrix::wid="Unknown damping identifier. Possible identifiers: "<>ToString@dampingMatrixIdentifiers;


radiationPressureMatrixIdentifiers={"NonDemolition","BeamSplitter","DownConversion"};
BS[gb_]={{0,0,-I gb,0},{0,0,0,I gb},{-I gb,0,0,0},{0,I gb,0,0}};
DC[gd_]={{0,0,0,-I gd},{0,0,I gd,0},{0,-I gd,0,0},{I gd,0,0,0}};
radiationPressureMatrix[g_,options:OptionsPattern[]]:=
	Block[{opt=OptionValue[Hamiltonian]},
		Return@Switch[opt,
			Evaluate@radiationPressureMatrixIdentifiers[[1]],(BS[g]+DC[g]),
			Evaluate@radiationPressureMatrixIdentifiers[[2]],BS[g],
			Evaluate@radiationPressureMatrixIdentifiers[[3]],DC[g],
			_,Message[radiationPressureMatrix::wid];Throw[wid]];
	];
Options[radiationPressureMatrix]={Hamiltonian->"NonDemolition"};
radiationPressureMatrix::wid="Unknown Hamiltonian identifier. Possible identifiers: "<>ToString@radiationPressureMatrixIdentifiers;


(*noise variance*)
(*vacuum state = 1/2!*)
noiseMatrix[nmech_,nopt_:0,options:OptionsPattern[]]:=Module[{nr},
	blockDiagonalMatrix[{(2nmech+1)*IdentityMatrix[2],(2nopt+1)*IdentityMatrix[2]}]/2
]
Options[noiseMatrix]={};


S0[\[Omega]_,\[CapitalDelta]_]={{-I \[Omega],0,0,0},{0,I \[Omega],0,0},{0,0,-I \[CapitalDelta],0},{0,0,0,I \[CapitalDelta]}};
optomechanicalCavity[{\[Omega]_,\[CapitalDelta]_,g_,\[Kappa]_,\[Gamma]_,nmech_,nopt_:0},options:OptionsPattern[]]:=
	Module[{sq,srp,dq,wq,hm,mm,nm,gm,qm,rm},
		(*dynamics*)
		srp=radiationPressureMatrix[g,Evaluate@FilterRules[{options},Options[radiationPressureMatrix]]];
		sq=ConjugateTranspose@TMQA[2].(S0[\[Omega],\[CapitalDelta]]+srp).TMQA[2];
		dq=dampingMatrix[\[Gamma],\[Kappa],Evaluate@FilterRules[{options},Options[dampingMatrix]]];
		wq=noiseMatrix[nmech,nopt,Evaluate@FilterRules[{options},Options[noiseMatrix]]];
		(*measurement*)
		If[OptionValue[Measurement],
			hm=Sqrt[2\[Kappa]]{{0,0,1,0},{0,0,0,1}};
			mm=Transpose@{{0,0,1,0},{0,0,0,1}}/2;
			nm=IdentityMatrix@Length@hm/2,
			hm=mm=nm={}
		];
		(*feedback*)
		If[OptionValue[Feedback]=!=False,
			gm=ArrayFlatten[{{#1*IdentityMatrix@2},{#2*IdentityMatrix@2}}]&@@
				Switch[OptionValue[Feedback],
					"Mechanical",{1,0},
					"Optical",{0,1},
					{_Integer,_Integer},OptionValue[Feedback],
					_,{1,1}
				];
			qm=DiagonalMatrix[{1,1,1,1}];
			rm=IdentityMatrix@Last@Dimensions@gm,
			gm=qm=rm={}
		];
		(*return*)
		systemModel@{{sq-dq,gm,-Sqrt[2dq],hm},{wq,nm,mm},{qm,rm}}
	];
Options[optomechanicalCavity]=Flatten@Join[
	{Measurement->True,Feedback->Full},
	Options/@{radiationPressureMatrix,dampingMatrix,noiseMatrix}];


(* ::Section:: *)
(*Package Setup End*)


End[];

EndPackage[];
