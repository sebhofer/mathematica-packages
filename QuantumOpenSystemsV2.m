(* ::Package:: *)

(* Mathematica Package *)

BeginPackage["QuantumOpenSystemsV2`", {"LinearSystems`","SystemModel`","SLHModel`","GeneralToolbox`","QuantumCVAlgebra`"}]

masterEquationToSystemModel::usage = "masterEquationToSystemModel[{R,\[CapitalLambda]}] convert (S,L,H)-model to systemModel, where S is assumed to be the identity (as only diffusive measurements are discribed by systemModels). 
R encodes the Hamiltonian, H=\!\(\*FractionBox[\(1\), \(2\)]\)X.R.X, while \[CapitalLambda] describes the measurement channel, L=\[CapitalLambda].X, where X collects the phase space operators.
If no unitary evolution is present set R={}. This only works for systems in Lindblad form (positive coefficients)!"

slhToSystemModel::usage = "slhToSystemModel[slh] converts slhModel slh to a systemModel"

physicalProcessQ::usage = "physicalProcessQ[sm] tests if systemModel sm describes a quantum-mechanical process, i.e., a CP map."

physicalMeasurementQ::usage = "physicalMeasurementQ[sm] tests if systemModel sm describes a quantum-mechanical measurement process, 
i.e., if the decoherence is compatible with the measurement."

estimatorRiccatiEquation::usage = "estimatorRiccatiEquation[sm] returns a list {{a,b},{q,r,p}} of matrices to be used with RiccatiSolve[{a,b},{q,r,p}].
systemModelRiccatiEquation[sm,\"Compact\"] returns a list {a,q,c} of matrices which correspond to the Riccati equation \!\(\*SuperscriptBox[\(a\), \(T\)]\).x+x.a+q-x.c.x."

controllerRiccatiEquation::usage = "controllerRiccatiEquation[sm] returns lists of matrices to be used with RiccatiSolve[{a,b},{a,r}]."


Begin["`Private`"] (* Begin Private Context *)


(*developer function from QuantumCVAlgebra*)
sQuOrder = QuantumCVAlgebra`Private`strictQuOrder;


(*conversion from slhModel to systemModel*)
(*helper functions for masterEquationToSystemModel*)
sympmat[n_] := blockDiagonalMatrix@ConstantArray[{{0, 1}, {-1, 0}}, n];(*symplectic matrix*)
reordMat[n_] := Module[{ev},
(*matrix to reorder columns from (X1,X2,...,P1,P2,...) to (X1,P1,...)*)
(*multiply from right*)
  ev[nn_,o_: 0] := UnitVector[2*nn, 2 # + o] & /@ Range[nn];
  ArrayFlatten[{{ev[n, -1]}, {ev[n, 0]}}]
];
noiseInputMat[lmat_?MatrixQ] := Module[{sym,opdim,cnum},
  {cnum,opdim} = Dimensions[lmat];(*{number of input channels,dimensions of operators}*)
  sym = sympmat[opdim/2];(*symplectic matrix*)
  (*reordered expressions from Edwards & Belavkin, arxiv.org/abs/quant-ph/0506018*)
  sym.ArrayFlatten@{{-Transpose@myIm@lmat,Transpose@myReal@lmat}}.reordMat@cnum
];

(*Generate state space model fo Gaussian system for given Hamiltonian and jump operators*)
(*This is basically following Edwards & Belavkin, arxiv.org/abs/quant-ph/0506018*)
Clear@masterEquationToSystemModel
masterEquationToSystemModel[{R_List,L_List,KK_List}, {lD_List}, {q_List,r_List}, OptionsPattern[]] :=
    Module[{a, b, bn, c, m, w, j,lR,lL,lN,cnum,dim},
      lR = If[Length@R===0,ConstantArray[0,{#,#}&@Dimensions[L][[2]]],R]; (*Hamiltonian matrix*)
      lL = If[Length@#===0,ConstantArray[0,{1,Dimensions[L][[2]]}],#]&@L[[OptionValue[OutputChannels]]]; (*specify
      observed channels*)
      {cnum,dim} = {1,1/2}*Dimensions[lL];(*{number of input channels,dimensions of operators}*)
      j = sympmat@dim; (*symplectic matrix*)

      (*unconditional dynamics*)
      a = j.(lR + myIm[ConjugateTranspose@L.L]);
      (*measurement corresponding to observed channels*)
      (*Note that this assumes we are measuring the quadrature dA+Conjugate@dA*)
      (*Transformation of the jump operators L->I*L will thus measure -I(dA-Conjugate@dA)*)
      c = 2 myReal[lL];
      (*noise matrices*)
      Switch[OptionValue@StandardForm,
        1,
      (*defines noise process dV, following Edwards & Belavkin*)
        bn = IdentityMatrix[2*dim]; (*process noise input*)
        lN = -j.myReal[ConjugateTranspose@L.L].j; (*process noise covariance*)
        m = -j.Transpose@myIm[lL], (*noise correlation*)
        2,
      (*NOTE: This is not very well tested yet!*)
      (*uses (X1in,P1in,X2in,P2in,...) as noise process*)
        bn = noiseInputMat@lL; (*process noise input*)
        lN = IdentityMatrix[2*cnum]; (*process noise covariance*)
        m = (Inverse@reordMat[#].ArrayFlatten[{{IdentityMatrix@#}, {0*IdentityMatrix@#}}])&@cnum; (*noise correlation*)
      ];
      w = IdentityMatrix@cnum; (*measurement noise covariance*)

      (*empty measurement matrices if requested*)
      If[!OptionValue@Measurement,c = m = w = {}];
      (*feedback*)
      b = If[Length@KK != 0, 2 j.myReal@KK, {}];(*deterministic input*)
      (*return*)
      systemModel[{{a, b, bn, c, lD}, {lN, w, m}, {q, r}}]
    ];
masterEquationToSystemModel[{R_List, L_List},opts:OptionsPattern[]]:=
    masterEquationToSystemModel[{R,L,{}}, {{}}, {{}, {}}, opts];
Options[masterEquationToSystemModel] = {Measurement -> True, OutputChannels -> All, StandardForm -> 1};


(*helper functions for slhToSystemModel*)
wrapop[q_] := Nest[List, q[], 1 - sQuOrder@q];
padmat[m_List,len_] := PadRight[m, {Length@m,2*len}];
padmat[{},_]:={};
(*canMat[mat_?MatrixQ] :=
  If[TrueQ@Simplify@ComplexExpand[mat == ConjugateTranspose[mat]], (mat + Transpose@mat)/2,Message[canMat::nonh]; $Aborted];*)
canMat[mat_?MatrixQ] :=
  If[HermitianMatrixQ[mat,SameTest->(Simplify[SameQ[#]]&)], (mat + Transpose@mat)/2,Message[canMat::nonh]; $Aborted];
canMat[0]={};
canMat[nonmat_]:=(Message[canMat::nonm];Throw[$Failed,canMat]);
canMat::nonh = "Matrix must be Hermitian.";
canMat::nonm = "Argument must be a matrix or 0.";

Clear@slhToSystemModel;
slhToSystemModel[{h:_quOp|_quSum,l:_quList},opts:OptionsPattern[]]:=
  Module[{dim},
    (*TODO: CHECK H conversion!!*)
    (*check arguments*)
    If[
      Not@And[
        Or[sQuOrder@h===2||sQuOrder@h==0,Message[slhToSystemModel::hsord];False], (*H must be 2nd order*)
        Or[Equal@@quDimensions[l],Message[slhToSystemModel::lsdim];False],   (*operators in L must all have equal dimensions*)
        Or[MatchQ[Union@quOrder@l,{0...,1...,{1}...}],Message[slhToSystemModel::lsord];False] (*L must be a list of 1st order ops, {1} is for quSums*)
      ],
      Throw[$Failed,slhToSystemModel]
    ];
    dim=If[sQuOrder@h==0,First@First@quDimensions@l,First@quDimensions@h];
    (*return*)
    (*Note the convention H=X.R.X/2!! This must be corrected by a factor 2!!*)
    (*TODO: check if wrapop is even needed if all operators in L have equal dimension*)
    masterEquationToSystemModel[{2*canMat@h[],padmat[wrapop/@First@l,dim]},opts]
  ];
slhToSystemModel[slh_slhModel, opts:OptionsPattern[]]:=
    slhToSystemModel[{slh["H"],slh["L"]}, opts, Sequence@@slhModelOptions@slh];
Options@slhToSystemModel = Options@masterEquationToSystemModel;
Unprotect[systemModel];
systemModel[slh_slhModel]:=slhToSystemModel[slh]
Protect[systemModel];

slhToSystemModel::hsord="Only Hamiltonians of second order are supported.";
slhToSystemModel::lsord="Jump operators must be of order 0 or 1.";
slhToSystemModel::lsdim="Jump operators must all have equal dimensions";


Clear@physicalProcessQ;
(* This tests if system evolves under a cp map (i.e. the dynamics can be described by a Lindblad MEQ)
see Wiseman&Milburn Quantum Measurement and Control*)
physicalProcessQ[sm_systemModel,options:OptionsPattern[{Verbose -> False}]] := 
	Module[{j, f = sm["F"]},
    j = sympmat[Length@f/2];
	  (*j = blockDiagonalMatrix@ConstantArray[{{0, 1}, {-1, 0}}, Length@f/2];*)
	  positiveSemiDefiniteMatrixQ[sm["L"].sm["W"].Transpose@sm["L"] - I (f.j + j.Transpose@f)/2,options]
	];


Clear@physicalMeasurementQ;
(* This tests if the continuous measurement does not violate Heisenberg uncertainty,
see Wiseman&Milburn Quantum Measurement and Control*)
physicalMeasurementQ[sm_systemModel,options : OptionsPattern[{Verbose -> False}]] := 
	Module[{j},
    j = sympmat[Length@sm["F"]/2];
	  (*j = blockDiagonalMatrix@ConstantArray[{{0, 1}, {-1, 0}}, Length@sm["F"]/2];*)
	  positiveSemiDefiniteMatrixQ[
	  	(sm["L"].(sm["W"] -sm["M"].Inverse@sm["N"].Transpose@sm["M"]).Transpose@sm["L"]) - 
	  	(j.Transpose@sm["H"].Inverse@sm["N"].sm["H"].Transpose@j/4), options]
	];


(*gives a list of matrices that can be used with RiccatiSolve (i.e. RiccatiSolve@@estimatorRiccatiEquation@sm)*)
estimatorRiccatiEquation[sm_systemModel] := List[
	{Transpose@sm["F"],Transpose@sm["H"]},
	{sm["L"].sm["W"].Transpose@sm["L"],sm["N"],sm["L"].sm["M"]}];
estimatorRiccatiEquation[sm_systemModel,"Compact"] := List[
	Transpose[sm["F"] - sm["L"].sm["M"].Inverse@sm["N"].sm["H"]],
	sm["L"].(sm["W"] - sm["M"].Inverse@sm["N"].Transpose[sm["M"]]).Transpose@sm["L"],
	Transpose@sm["H"].Inverse@sm["N"].sm["H"]];


(*gives a list of matrices that can be used with RiccatiSolve (i.e. RiccatiSolve@@controllerRiccatiEquation@sm)*)
controllerRiccatiEquation[sm_systemModel]:=	
	List[{sm["F"],sm["G"]},{sm["Q"],sm["R"]}];
controllerRiccatiEquation[sm_systemModel,"Compact"]:=	
	List[sm["F"],sm["Q"],sm["G"].Inverse@sm["R"].Transpose@sm["G"]];


End[] (* End Private Context *)

EndPackage[]
