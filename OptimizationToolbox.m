(* ::Package:: *)

(* ::Title:: *)
(*Optomechanics Toolbox: Optimization*)


(* ::Text:: *)
(*
IMPORTANT:
- This should be ported to use Batch.m
- Fix names!!

TODO : (in no particular order)
- add ContinueRun to continue from last point (in tmp-file)
- change argument syntax for constrains to match NMinimize!!!
- downcase data, options, parameters,... ??
- elaborate RedoRun to take different arguments, start from arbitrary datapoint
- make Load... check for non-existent files!!
- Include/write usage commands for all exported functions.
- REMOVE SetOptimizedFunction when possible!!
- test if SaveData[Method->"Replace"] works correctly!
- TmpSave doesn't seem to work!
- find out what needs to be protected
*)
(**)
(*CHANGES:*)
(**)
(*SMALL RECENT CHANGES (delete these on commit!):*)


(* ::Section:: *)
(*Package Setup Begin*)


BeginPackage["OptimizationToolbox`",{"Parallel`Developer`"}];


SetDateString::usage="Set file identifier to current timestamp. This is needed to assign an unique name to each run. SetDateString is automatically called by StartRun, but not by Optimization.";
SetupFileStructure::usage="Use this to set up DataDir, DataPrefix, DataSuffix, OutSuffix, TmpDataInfix. All of these are initialized to default values on load.";
PrintFileStructure;
SetObjectiveFunction::usage="SetOptimizationFunction[expr] defines expr to be the function to be optimized.";
ObjectiveFunction::usage="Holds expression object to optimization.";

Optimize::usage ="Optimize[{x\[Rule]\!\(\*SubscriptBox[\(x\), \(0\)]\),y\[Rule]\!\(\*SubscriptBox[\(y\), \(0\)]\),...},{u\[Rule]\!\(\*SubscriptBox[\(u\), \(0\)]\),v\[Rule]\!\(\*SubscriptBox[\(v\), \(0\)]\),...}] optimize ObjectiveFunction with respect to {x,y,...} with initial conditions {\!\(\*SubscriptBox[\(x\), \(0\)]\),\!\(\*SubscriptBox[\(y\), \(0\)]\),...}, where {u->\!\(\*SubscriptBox[\(u\), \(0\)]\),...} are fixed parameters.
Optimize[{x\[Rule]\!\(\*SubscriptBox[\(x\), \(0\)]\),y\[Rule]\!\(\*SubscriptBox[\(y\), \(0\)]\),...},{u\[Rule]\!\(\*SubscriptBox[\(u\), \(0\)]\),v\[Rule]\!\(\*SubscriptBox[\(v\), \(0\)]\),...},{w,{\!\(\*SubscriptBox[\(w\), \(0\)]\),\!\(\*SubscriptBox[\(w\), \(1\)]\),...}}] generates a list by effectively calling Optimize[{x\[Rule]\!\(\*SubscriptBox[\(x\), \(0\)]\),y\[Rule]\!\(\*SubscriptBox[\(y\), \(0\)]\),...},{u\[Rule]\!\(\*SubscriptBox[\(u\), \(0\)]\),v\[Rule]\!\(\*SubscriptBox[\(v\), \(0\)]\),...,w\[Rule]\!\(\*SubscriptBox[\(w\), \(i\)]\)}] successively.
Optimize[{x\[Rule]\!\(\*SubscriptBox[\(x\), \(0\)]\),y\[Rule]\!\(\*SubscriptBox[\(y\), \(0\)]\),...},{u\[Rule]\!\(\*SubscriptBox[\(u\), \(0\)]\),v\[Rule]\!\(\*SubscriptBox[\(v\), \(0\)]\),...},{{w,{\!\(\*SubscriptBox[\(w\), \(0\)]\),\!\(\*SubscriptBox[\(w\), \(1\)]\),...}},{z,{\!\(\*SubscriptBox[\(z\), \(0\)]\),\!\(\*SubscriptBox[\(z\), \(1\)]\),...}}] generates a table.

Note:
- The objective function has to be defined by running SetObjectiveFunction[] first.
- The second argument {u->\!\(\*SubscriptBox[\(u\), \(0\)]\),...} has to be set up such that ReplaceRepeated[OptimizationFunction,{x->\!\(\*SubscriptBox[\(x\), \(0\)]\),...,u->\!\(\*SubscriptBox[\(u\), \(0\)]\),...}] yields a numerical expression.

Options:
- Fallback: Messages from this message group make the algorithm fall-back to NMinimize. See Check[] for more information.
- TimeConstraints: Evaluation of NMinimize is aborted after given time (in seconds). Result of FindMinimum is returned.
- MemoryConstraints
- Compiled
- All other options are passed to FindMinimum/NMinimize.";

InitParallel::usage="Initializes given number of kernels and distributes necessary definitions. Must be run AFTER SetObjectiveFunction!";
StartRun::usage=
"Starts optimization run and saves data and log files. Takes same arguments as Optimize.";
RedoRun::usage="Loads parameters from specified file and passes them to StartRun. Complementary or supplementary options can be given.";
LoadData::usage=
"LoadData[filenameid_,suf_:DataSuffix] loads datafile filenameid.suf from data directory and returns optimized data.";
LoadArguments::usage=
"LoadArguments[filenameid_,suf_:DataSuffix] loads datafile filenameid.suf from data directory and returns {Paramters,InitialValues,Iterators}";
OpenFile::usage="Open specified from Datadir in FrontEnd";
LoadObjectiveFunction;


(*all these are saved to .dat file and should not have the packages' private context*)
f;(*holding minimum value of ObjectiveFunction*)
Data;(*data*)
InitialValues;
Iterators;(*list of values for column/table*)
Parameters;(*all other parameters*)
options;

(*options*)
Constraints;(*optimization constraints*)
Fallback;
MinimizationFunction;

(*Enable this for testing/debuging only!*)
(*OptField; OptColumn;*)


Begin["`Private`"];


(* ::Section:: *)
(*Initialization*)


(* ::Text:: *)
(*Initialize DateStr and all file/directory names on load. SetDateString is called on each StartRun to guarante new file name, but not on Optimize!*)


(*DefaultDatadir="~/Documents/projects/teleportation/numerics/data/optimization/";*)
DefaultDatadir=Directory[];
DefaultDataprefix="opt-";
DefaultDatasuffix=".dat";
DefaultOutsuffix=".out";
DefaultTmpdatainfix="-tmp";


SetDateString[]:=(DateStr=ToString@AbsoluteTime@DateString[]);


SetupFileStructure[options:OptionsPattern[]]:=
	Module[
		{
			datadir=OptionValue["DataDir"],
			dataprefix=OptionValue["DataPrefix"],
			datasuffix=OptionValue["DataSuffix"],
			outsuffix=OptionValue["OutSuffix"],
			tmpdatainfix=OptionValue["TmpDataInfix"]
		},
		{Datadir,Dataprefix,Datasuffix,Outsuffix,Tmpdatainfix}=
			{datadir,dataprefix,datasuffix,outsuffix,tmpdatainfix};
	];
Options[SetupFileStructure]=
	{"DataDir"->DefaultDatadir,"DataSuffix"->DefaultDatasuffix,"DataPrefix"->DefaultDataprefix,
	"OutSuffix"->DefaultOutsuffix,"TmpDataInfix"->DefaultTmpdatainfix};
SetAttributes[SetupFileStructure,{HoldAll}];


PrintFileStructure[]:={"DataDir"->Datadir,"DataSuffix"->Datasuffix,"DataPrefix"->Dataprefix,
	"OutSuffix"->Outsuffix,"TmpDataInfix"->Tmpdatainfix};


OutFilename:=FileNameJoin[{Datadir,Dataprefix<>DateStr<>Outsuffix}];
DataFilename:=FileNameJoin[{Datadir,Dataprefix<>DateStr<>Datasuffix}];
TmpFilename:=FileNameJoin[{Datadir,Dataprefix<>DateStr<>Tmpdatainfix<>Datasuffix}];


SetDateString[];
SetupFileStructure[];


(* ::Text:: *)
(*Define FindMinimum message group; this defines the default messages on which to fall back to NMinimize*)


AppendTo[$MessageGroups,"Fallback":>{FindMinimum::eit,FindMinimum::cvmit,FindMinimum::lstol,FindMinimum::nrnum,Optimize::frcefb}];


(* ::Section:: *)
(*Optimization Functions*)


OptField[init_List,params_List:{},options:OptionsPattern[]]:=
	Module[
		{
			ret,args,objfunc,tinit,
			res,res2,
			time=0,time2=0,
			linit=init/.Rule->List,
			constraints=OptionValue[Constraints],
			tmpsavefreq=OptionValue[TmpSaveFreq],
			tmpfilename=OptionValue[TmpFileName],
			workingprecision=OptionValue[WorkingPrecision],
			compile=OptionValue[Compiled],
			fallback=OptionValue[Fallback],
			timeconstraint=OptionValue[TimeConstraint],
			memoryconstraint=OptionValue[MemoryConstraint],
			tolerance=1/2
		},

		(*parsing options*)
		If[fallback===None||fallback===False,fallback={}];
		If[fallback===Automatic,fallback=Options[Optimize,Fallback][[1,2]](*"Fallback"*)];
		If[constraints===Automatic,constraints=True];

		(*error handling*)
		If[!ValueQ[ObjectiveFunction],Message[Optimize::notdeff];Return@$Failed];
		If[SameQ[init,{}],Message[Optimize::arge,1,List];Return@$Failed];
		If[!And@@((Head@#===Rule)&/@Flatten@init),Message[Optimize::argr,1];Return@$Failed];
		If[!And@@((Head@#===Rule)&/@Flatten@params),Message[Optimize::argr,2];Return@$Failed];

		If[!ValueQ@itotal,itotal=1];
		If[!ValueQ@ttotal,ttotal=0];

		(*compiling ObjectiveFunction*)
		If[((compile===Automatic&&workingprecision===MachinePrecision)||(compile===True)),
			objfunc=Compile[Evaluate[{#,_Real}&/@init[[1;;,1]]],Evaluate[ObjectiveFunction//.params]];
		If[$MachinePrecision<workingprecision,Message[Optimize::cmpp,workingprecision]],
			objfunc=Function[Evaluate[init[[1;;,1]]],Evaluate[ObjectiveFunction//.params]]];

		(*starting minimization*)
		If[constraints===True,
			args=Apply[objfunc,init[[1;;,1]]],
			args=({Apply[objfunc,init[[1;;,1]]],constraints//.params})];

		Block[{$MaxPrecision,$MinPrecision},
			(*fix precision if requested*)(*TEST THIS*)
			If[OptionValue[FixPrecision],$MaxPrecision=$MinPrecision=workingprecision];
			MemoryConstrained[
				(*fall back to NMinimize if FindMinimum does not converge*)
				Check[
					(*if option Fallback->ForceFallback go to NMinimize straight away, otherwise try FindMinimum first*)
					If[fallback==="ForceFallback",
						Message[Optimize::frcefb],
						{time,res}=
						With[{linit=Sequence[linit]},
							AbsoluteTiming@FindMinimum[args,
								linit,
								Evaluate@Union@FilterRules[{options,WorkingPrecision->workingprecision},Options@FindMinimum]]
						]
					];
			
					(*fall back to NMinimize if it fails*)
					Message[Optimize::fbck];
					Print["KID ",$KernelID,", n=",itotal,", Calling NMinimize"];
			
					(*set initial region*)(*CHANGE THIS!!*)
					tinit=Rationalize[{#[[1]],#[[2]](1-tolerance),#[[2]](1+tolerance)}&/@linit];(*CHANGE THIS - No hardcoded values!!*)
					(*DEBUG - test rationalization*)
					(*Print[tinit];*)

					(*FindMinimize results*)
					If[res,SaveData[StringInsert[tmpfilename,"-orig",-(Length@Datasuffix+1)],Join[{f->res[[1]]},res[[2]],params],Method->"Append"]];
	
					TimeConstrained[
						{time2,res2}=
							AbsoluteTiming@NMinimize[args,tinit,
								Evaluate@Union@FilterRules[{options,WorkingPrecision->workingprecision},Options@NMinimize]
							]; 
						res=res2;time=time+time2,
						timeconstraint,
						time=time+timeconstraint;
						Message[Optimize::tcons];
						Print["KID ",$KernelID,", n=",itotal,", Time constraint of ",timeconstraint,"s was not met by NMinimize. Returning FindMinimum's result"]
					],
					(*fall back when these messages are called*)
					Evaluate@fallback
				],
				memoryconstraint,
				Print["KID ",$KernelID,", n=",itotal,", Memory constraint was reached, skipping step."]
			]
		];

		(*postprocessing result*)
		If[MatchQ[res,h_[a_?NumberQ,h_[g_[_,_?NumberQ]..]]/;(h===List&&g===Rule)],(*'..' is for repeated patterns!!*)

			(*succeeded*)
			ret=Join[{f->res[[1]]},res[[2]],params];
			If[res[[1]]<0,
				Print["WARNING f<0! KID ",$KernelID,", n=",itotal,", ",time,"s;"],
				Print["KID ",$KernelID,", n=",itotal,", ",time,"s;"]
			];
			If[tmpsavefreq!=0&&Mod[itotal,tmpsavefreq]==0&&ValueQ@restmparray,
				AppendTo[restmparray,ret]; SaveData[tmpfilename,restmparray,Method->"Replace"];
			],

			(*failed*)
			ret=$Failed;
			Print["ERROR OptField KID ",$KernelID,", n=",itotal,", ",time,"s"];
		];

		itotal++;
		ttotal=ttotal+time;
		Return@ret;
	];


OptColumn[init_List,params_List,iterator_List,options:OptionsPattern[]]:=
	Module[
		{
			res,rep,
			dq={},
			linit=init,optvars=init[[All,1]],
			myoptions=options,toptions,
			constraints=OptionValue[Constraints]
		},

		(*Print["Enter OptColum, KID: ",$KernelID];*)
		(*error handling*)
		If[SameQ[init,{}],Message[Optimize::arge,1,List];Return@$Failed];
		If[SameQ[iterator,{}],Message[Optimize::arge,3,List];Return@$Failed];
		If[!MatchQ[iterator,h_[_?(Head@#===Symbol&),h_[__]]/;h===List],Message[Optimize::argg,3];Return@$Failed];
		If[!And@@((Head@#===Rule)&/@Flatten@init),Message[Optimize::argr,1];Return@$Failed];
		If[!And@@((Head@#===Rule)&/@Flatten@params),Message[Optimize::argr,2];Return@$Failed];

		(*preprocessing options*)
		myoptions=DeleteCases[options,Rule[Constraints,__]];
		If[constraints===Automatic,constraints=Options[OptColumn,Constraints][[1,2]]];

		(*DEBUG*)
		(*Print[constraints];*)

		Do[
			rep=Prepend[params,iterator[[1]]->iterator[[2,i]]];

			(*process constrains*)
			Switch[constraints,
				{Automatic,n_/;NumericQ[n]&&NonNegative[n]&&n<1},
				(*Constraints->{Automatic,n}: sets constraints to lastvalue*(1-n)<x<lastvalue*(1+n)*)
				If[dq=={},
					toptions=Append[myoptions,Constraints->True],
					Block[
						{
							tconstraints,
							c=constraints[[2]],
							dql=Last[optvars/.dq]
						},

						tconstraints=And@@Map[Apply[#2<#1<#3&,#]&,(*input: {{variable name, lower constraint, upper constraint}}*)
							Map[Apply[{#1,#2 (1-c),#2 (1+c)}&,#]&,(*input: {{variable name,last value},...}*)
								Partition[Riffle[optvars,dql],2]]];				
						toptions=Append[myoptions,Constraints->tconstraints];
					]
				],
				{Derivative,n_/;NumericQ[n]&&NonNegative[n]},
				(*Constraints->{Derivative,n}: sets constraints by lineary extrapolating function; 
				  constraints = lastvalue +/- n*lastslope; lastslope is averaged over 3 points*)
				If[Length[dq]<3,
					toptions=Append[myoptions,Constraints->True],
					Block[
						{
							tconstraints,
							c=constraints[[2]],
							itstep=Abs[iterator[[2,i]]-iterator[[2,i-1]]],
							dql=Last[optvars/.dq],
							dqd=Map[MovingAverage[#,2]&,Map[ListDerivative,Map[{iterator[[1]],#}/.dq&,optvars]][[All,All,2]]][[All,-1]],
							poz=If[#<0,0,#]&
						},

						tconstraints=And@@Map[Apply[#2<#1<#3&,#]&,(*input: {{variable name, lower constraint, upper constraint}}*)
							Map[Apply[{#1,poz[#2-c*itstep*Abs@#3],#2+c*itstep*Abs@#3}&,#]&,
								Partition[Flatten@Riffle[optvars,Partition[Riffle[dql,dqd],2]],3]]];				
						toptions=Append[myoptions,Constraints->tconstraints];
					]
				],
				(*Default*)
				_,toptions=Append[myoptions,Constraints->constraints](*pass on constrains if not matched*)
			];
			
			(*DEBUG*)
			(*Print["toptions: ",toptions];*)

			res=OptField[linit,rep,Evaluate@FilterRules[{toptions},Options@OptField]];
			If[res=!=$Failed,AppendTo[dq,res];linit=FilterRules[Last@dq,init]],

			{i,1,Length@iterator[[2]]}
		];

		(*Print["Leave OptColum, KID: ",$KernelID];*)
		If[Length@dq==0,
			Print["ERROR OptColumn KID ",$KernelID];Return@$Failed,
			Return@dq];
	];


OptTablePar[init_List,params_List,iterator_List,options:OptionsPattern[]]:=
	Module[
		{
			restmp,pid,
			xvec={},yvec={},
			pids={},res={},
			tq=init,j=1
		},

		(*initialize global counter*)		
		itotal=0;

		(*error handling*)
		If[!And@@(MemberQ[$DistributedDefinitions,#]&/@
				{Hold@ObjectiveFunction,Hold@OptColumn,Hold@OptField,Hold@SaveData,Hold@PrintToFile}),
			Message[Optimize::nopar];Return@$Failed];
		If[!MatchQ[iterator,h_[h_[_?(Head@#===Symbol&),h_[___]],h_[_?(Head@#===Symbol&),h_[___]]]/;h===List],
			Message[Optimize::argg,3];Return@$Failed,
			xvec=iterator[[1,2]];yvec=iterator[[2,2]]];
		If[SameQ[init,{}],Message[Optimize::arge,1,List];Return@$Failed];
		If[SameQ[xvec,{}],Message[Optimize::arge,2,List];Return@$Failed];(*FIXTHIS*)
		If[SameQ[yvec,{}],Message[Optimize::arge,3,List];Return@$Failed];
		If[!And@@((Head@#===Rule)&/@Flatten@init),Message[Optimize::argr,1];Return@$Failed];
		If[!And@@((Head@#===Rule)&/@Flatten@params),Message[Optimize::argr,4];Return@$Failed];

		While[True,
			While[$QueueLength==0&&j<=Length@xvec,
				tq=OptField[FilterRules[tq,init],
					Join[{iterator[[1,1]]->xvec[[j]],iterator[[2,1]]->First@yvec},params],
					Evaluate@FilterRules[{options},Options@OptField]];
				If[tq===$Failed,
					Message[Optimize::noinit,xvec[[j]]],
					AppendTo[pids,#]&@
						With[{tq=tq,linit=FilterRules[tq,init],
								lparams=Join[{iterator[[1,1]]->xvec[[j]]},params],
								lyvec={iterator[[2,1]],Delete[yvec,1]}},
							ParallelSubmit[Join[{tq},
								OptColumn[linit,lparams,lyvec,Evaluate@FilterRules[{options},Options@OptColumn]]]]
						]
				];
				j++;
				If[!QueueRun[],Break[]];
			];

			If[Length@pids>=0,
				{restmp,pid,pids}=WaitNext@pids;
				If[restmp=!=$Failed,AppendTo[res,restmp]];
			];
			If[Length@pids==0,Break[]];
		];
		Return@Flatten[res,1];
	];


(*FIX THIS: should also use OptColumn!!*)
OptTableSer[init_List,params_List,iterator_List,options:OptionsPattern[]]:=
	Block[
		{res,linit,time,
		itotal=0,restmparray={},xvec={},yvec={},
		xymax={}},

		(*error handling*)
		If[!MatchQ[iterator,h_[h_[_?(Head@#===Symbol&),h_[___]],h_[_?(Head@#===Symbol&),h_[___]]]/;h===List],
			Message[Optimize::argg,2];Return@$Failed,
			xvec=iterator[[1,2]];yvec=iterator[[2,2]]];
		If[SameQ[init,{}],Message[Optimize::arge,1,List];Return@$Failed];
		If[SameQ[xvec,{}],Message[Optimize::arge,2,List];Return@$Failed];(*FIXTHIS*)
		If[SameQ[yvec,{}],Message[Optimize::arge,3,List];Return@$Failed];
		If[!And@@((Head@#===Rule)&/@Flatten@init),Message[Optimize::argr,1];Return@$Failed];
		If[!And@@((Head@#===Rule)&/@Flatten@params),Message[Optimize::argr,4];Return@$Failed];

		xymax=ConstantArray[0,{Length@xvec,Length@yvec}];
		For[j=1,j<=Length@yvec,j++,
			For[i=1,i<=Length@xvec,i++,
				If[i==1&&j==1,linit=init,
					If[i==1&&j!=1,linit=xymax[[1,j-1]],linit=xymax[[i-1,j]]]];
				xymax[[i,j]]=OptField[FilterRules[linit,init],
					Join[{iterator[[1,1]]->xvec[[i]],iterator[[2,1]]->yvec[[j]]},params],FilterRules[{options},Options@OptField]];
			(*Print[j,",",i,":",time,"s"];*)
			];
		];
		Return@Flatten[xymax,1];
	];


Optimize[init_List,params_List,options:OptionsPattern[]]:=
	OptField[init,params,Evaluate@FilterRules[{options},Options[OptField]]];
Optimize[init_List,params_List,val__/;(Length[SequenceHold@val]==1),options:OptionsPattern[]]:=
	OptColumn[init,params,val,FilterRules[{options},Options[OptColumn]]];
Optimize[init_List,params_List,val__/;(Length[SequenceHold@val]==2),options:OptionsPattern[]]:=
	If[OptionValue[ForceSerial]==True,
		OptTableSer[init,params,{val},FilterRules[{options},Options[OptTableSer]]],
		OptTablePar[init,params,{val},FilterRules[{options},Options[OptTablePar]]]];


Optimize::argg="Argument `1` is not in the correct format.";
Optimize::arge="Argument `1` is empty, nonempty argument `2` is expected.";
Optimize::argw="Argument `1` is of type `2`, argument of type `3` is expected.";
Optimize::argr="Argument `1` is expected to be a list of replacement rules.";
Optimize::cmpp="Precision of compiled function will be restricted to MachinePrecision. Requested WorkingPrecision is `1`.";
Optimize::fbck="Falling back to NMinimize.";
Optimize::nopar="Required definitions are not distributed to parallel kernels. Run InitParallel[] first!";
Optimize::noinit="No initial conditions at x=`1`, skipping column.";
Optimize::notdeff="ObjectiveFunction is not defined. Run SetObjectiveFunction first.";
Optimize::tcons="Time constraint was not met. Aborting.";
Optimize::frcefb="Skipping FindMinimum. Use NMinimize instead.";


Options[OptTableSer]=Options[OptTablePar]=Options[OptColumn]=Options[OptField]=
	Join[Union[Options[FindMinimum],Options[NMinimize]],
		{Constraints->Automatic,TmpSaveFreq->10,TmpFileName->TmpFilename,Fallback->"Fallback",TimeConstraint->600,MemoryConstraint->Infinity,FixPrecision->False}];
Options[Optimize]=Append[Options[OptField],ForceSerial->False];
SetOptions[OptColumn,Constraints->{Automatic,1/2}];
SetOptions[OptTablePar,Constraints->{Automatic,1/2}];
SetOptions[OptTableSer,Constraints->{Automatic,1/2}];


(* ::Section:: *)
(*Helper Functions*)


SetObjectiveFunction[f_]:=(ObjectiveFunction=f);
SetOptimizedFunction[f_]:=(Message[SetOptimizedFunction::obs];SetObjectiveFunction[f]);(*obsolete, for reverse compatibility only, REMOVE WHEN SAVE*)
SetOptimizedFunction::obs="This function is obsolete! Use SetObjectiveFunction instead!";


ListDerivative[l_List]:=
	Module[{num,den},
		num=Differences@l[[All,2]];
		den=Differences@l[[All,1]];
		Return@Partition[Riffle[l[[All,1]],num/den],2];
	];


PrintToFile[expr_,filename_String,method_:"a",options:OptionsPattern[]]:=
	Block[{fh,pw=OptionValue[PageWidth]},
		If[!MemberQ[{"a","w"},method],Message[PrintToFile::optw];Return@$Failed];
		If[method==="a",fh=OpenAppend[filename,Evaluate@options],fh=OpenWrite[filename,Evaluate@options]];
		If[fh=!=$Failed,
			SetOptions[fh, PageWidth->pw];
			Write[fh,Release@expr];Close@fh];
		Return@expr;
	];


PrintHeader[init_List,otherrep_List,val__,outfilename_String,datafilename_String,comment_String,options___]:=
	Block[{xv,yv},
		Print["Running ",Check[FileNameTake@NotebookFileName[],"batch file."]];
		Print[DateString[]];
		Print["Note: ",comment];
		Print["This file: ",outfilename];
		Print["Data file: ",datafilename];
		Print["Precision of evaluated expression: ",Precision@ObjectiveFunction];
		Print["Iterator: ",InputForm@{val}];
		Print["InitialValues: ",InputForm@init];
		Print["Parameters: ",InputForm@otherrep];
		Print["Passed options: ",InputForm@DeleteCases[{options},OutFileComment->__]];
		(*OptColumn*)
		If[Length[SequenceHold@val]==1,
			Print["Number of rows: j=1,..,",Length@val[[2]],"; Total number of points: ",Length@val[[2]]]];
		(*OptTable*)
		If[Length[SequenceHold@val]==2,
			{xv,yv}={val};
			Print["Number of rows/columns: j=1,..,",Length@xv[[2]],"; i=1,..,",Length@yv[[2]],
				"; Total number of points: ",(Length@xv[[2]])*(Length@yv[[2]])]];
	];


SaveData[outfilename_String,dat_,options:OptionsPattern[]]:=
	Block[
		{
			filename=outfilename,
			Data=dat,
			method=OptionValue[Method]
		},
		If[!MemberQ[{"Append","Replace"},method],
			Message[save::methw,method,Options[SaveData,Method][[1,2]]];
			method=ToString@Options[SaveData,Method][[1,2]]];
		If[FileExistsQ[filename]&&method==="Replace",DeleteFile[filename]];(*does this work correctly?*)
		Save[filename,Data];
	];


SaveArguments[outfilename_String,init_,pars_,it_,opts_List,OptionsPattern[]]:=
	Block[
		{
			filename=outfilename,
			InitialValues=init,
			Parameters=pars,
			Iterators=it,
			Options=opts,
			method=OptionValue[Method]
		},
		If[!MemberQ[{"Append","Replace"},method],
			Message[save::methw,method,Options[SaveData,Method][[1,2]]];
			method=ToString@Options[SaveArguments,Method][[1,2]]];
		If[FileExistsQ@filename&&method==="Replace",DeleteFile[filename]];
		Save[filename,InitialValues];
		Save[filename,Parameters];
		Save[filename,Iterators];
		Save[filename,options];
	];


OpenFile[filename_]:=
	Block[
		{
			fullname,Data
		},
		fullname=FileNameJoin[{Datadir,filename}];
		Return@NotebookOpen@fullname;
	];


LoadData[filenameid_,suf_:Datasuffix]:=
	Block[
		{
			fullname,Data,
			(*these are defined in .dat file and should not leak out*)
			InitialValues,Parameters,Iterators,options,ObjectiveFunction
		},
		fullname=FileNameJoin[{Datadir,ToString@filenameid<>suf}];
		Get@fullname;
		Return@Data;
	];


LoadObjectiveFunction[filenameid_,suf_:Datasuffix]:=
	Block[
		{
			fullname,Data,
			(*these are defined in .dat file and should not leak out*)
			InitialValues,Parameters,Iterators,options,ObjectiveFunction
		},
		fullname=FileNameJoin[{Datadir,ToString@filenameid<>suf}];
		Get@fullname;
		Return@ObjectiveFunction;
	];


LoadArguments[filenameid_,suf_:Datasuffix]:=
	Block[{fullname,InitialValues,Parameters,Iterators,options,ObjectiveFunction},
		fullname=FileNameJoin[{Datadir,ToString@filenameid<>suf}];
		Get@fullname;
		(*throw error if any of required arguments are not found*)
		Check[
			If[!ValueQ[#],Message[LoadArguments::reqm,#]]&/@{Unevaluated@InitialValues,Unevaluated@Iterators},
			Return@$Failed,
			LoadArguments::reqm
		];
		(*if optional argument is not found, reset it to {} and issue warning*)
		If[!ValueQ[#],Message[LoadArguments::optm,#];#={}]&/@{Unevaluated@Parameters,Unevaluated@options};
		Return[{InitialValues,Parameters,Iterators,options}];
	];


InitParallel[forcereload_:False,nkernels_:8]:=
	Block[{kdiff,res},
		If[forcereload,AbortKernels[];CloseKernels[]];
		If[(kdiff=nkernels-$KernelCount)>0,res=LaunchKernels[kdiff],res=Kernels[]];
		If[!ValueQ[ObjectiveFunction],Message[Optimize::notdeff];Return@$Failed];
		DistributeDefinitions[ObjectiveFunction,OptColumn,OptField,SaveData,PrintToFile];
		SetSharedVariable[itotal,restmparray,ttotal];
		ttotal=itotal=0; restmparray={};
		Return@res;
	];


StartRun[init_List,params_List,val___,options:OptionsPattern[]]:=
	Block[
		{
			res,str,time,optfunc,parallelq,outfilename,datafilename,tmpfilename,
			ttotal=0,
			DateStr=SetDateString[],
			writeoutfile=OptionValue[WriteOutFile],
			forceserial=OptionValue[ForceSerial],
			outfilecomment=OptionValue[OutFileComment],
			suppresserrors=OptionValue[SuppressErrors]
		},
		outfilename=OutFilename;
		datafilename=DataFilename;
		tmpfilename=TmpFilename;
		If[!ValueQ@restmparray,restmparray={}];

		If[($KernelCount>1&&!forceserial),
			parallelq=True,forceserial=True;parallelq=False];
		If[suppresserrors,
			Off[FindMinimum::eit];
			If[parallelq,ParallelEvaluate[Off[FindMinimum::eit]]]];

		CheckAbort[
			If[writeoutfile,
				Unprotect@Print;
				With[{fn=outfilename},Print[x__]:=PrintToFile[Hold@x,fn,"a",FormatType->StandardForm]];
				If[parallelq,DistributeDefinitions[Print]]];
			PrintHeader[init,params,val,outfilename,datafilename,outfilecomment,options];
			If[parallelq,Print["Parallel evaluation"],Print["Serial evaluation"]];

			{time,res}=AbsoluteTiming@Optimize[init,params,val,
				FilterRules[{options,TmpFileName->tmpfilename,ForceSerial->forceserial},Options@Optimize]];

			(*write out data*)
			If[res=!=$Failed,
				SaveData[datafilename,res];
				Save[datafilename,ObjectiveFunction];
				Save[datafilename,repl];
				SaveArguments[datafilename,init,params,Hold@val,{options}],
				Print["Fatal error occured. Aborting."]];

			(*Print Footer*)
			Print["Finished calculation in ",time," seconds = ",N[time/3600,3] ," hours"];
			Print["Total net CPU time ",ttotal," seconds = ",N[ttotal/3600,3] ," hours"];
			If[parallelq,Print["Speedup: ",N[ttotal/time,3],"x"]];
			If[writeoutfile&&!MemberQ[Attributes@Print,Protected],
				Clear@Print;Protect@Print],
			
			(*Clean up after abort*)
			Print["Aborted."];
			If[writeoutfile&&!MemberQ[Attributes@Print,Protected],
				Clear@Print;Protect@Print];
			Return@$Aborted;
		];
		Return@res;
	];


RedoRun[filename_String,options:OptionsPattern[]]:=
	Module[{linit,lparams,literator,lopts},
		{linit,lparams,literator,lopts}=LoadArguments[filename];
		If[Positive@Length@{options},lopts=Complement[lopts,FilterRules[lopts,{options}]]];
		Print["StartRun is called with the following arguments (plus passed options):"];
		Print[{linit,lparams,literator,lopts}];
		StartRun[linit,lparams,Sequence@@literator,Join[lopts,{options}]];
	];


save::methw="Method must either be Append or Replace, but `1` was supplied. Falling back to default (`2`).";
PrintToFile::optw="Argument 3 is not member of {a,w}.";
LoadArguments::reqm="No value for required argument `1` could be loaded. Aborting.";
LoadArguments::optm="No value for optional argument `1` could be loaded. Returning {} instead.";


Options[StartRun]=Options[RedoRun]=Join[Options[OptField],{ForceSerial->False,WriteOutFile->True,OutFileComment->"None",SuppressErrors->False}];
Options[PrintToFile]=Options[OpenWrite];
Options[SaveData]=Options[SaveArguments]={Method->"Append"};
SetOptions[PrintToFile,PageWidth->Infinity];


(* ::Section:: *)
(*Data Analysis Functions*)


ExtractOpt[dat_,var_List]:=Sort[var/.dat,(#1[[1]]<#2[[1]])&];
ExtractData[dat_List,axes_List,vars_List]:=Map[ExtractOpt[dat,Append[axes,#]]&,vars];


SelectRow[dat_List,val_?NumberQ]:={#[[1]],#[[3]]}&/@Select[dat,#[[2]]==val&];
SelectRow[dat_List,val_List]:=SelectRow[dat,#]&/@val;
SelectColumn[dat_List,val_?NumberQ]:={#[[2]],#[[3]]}&/@Select[dat,#[[1]]==val&];
SelectColumn[dat_List,val_List]:=SelectColumn[dat,#]&/@val;


(* ::Text:: *)
(*Outdated!!*)


EPRPlot[dat_,xaxis_List,yaxis_List,varmap_List:VariableMap,func_:ObjectiveFunction,options:OptionsPattern[]]:=
	Module[{p,xp,yp,lfunc,lxaxis=xaxis,lyaxis=yaxis},
		lxaxis[[1]]=xp;
		lyaxis[[1]]=yp;
		lfunc=func/.{(xaxis[[1]]/.varmap)->xp,(yaxis[[1]]/.varmap)->yp}/.dat//Chop;
		p=ContourPlot[Chop@lfunc,Evaluate@lxaxis,Evaluate@lyaxis,
			Sequence[options,PlotRange->{0,2},Epilog->({Red,PointSize[.015],Evaluate@Point[{xaxis[[1]],yaxis[[1]]}/.varmap/.dat]})]];
		Return@p;
	];
EPRPlotTable[dat_,xaxis_,yaxis_,ind_,varmap_List:VariableMap,func_:ObjectiveFunction,options:OptionsPattern[]]:=
	Table[EPRPlot[dat[[ind[[1]]]],xaxis,yaxis,varmap,func,options],ind];


Options[EPRPlot]=Options[ContourPlot];
SetOptions[EPRPlot,PlotRange->{0,2}];
Options[EPRPlotTable]=Options[EPRPlot];


(* ::Section:: *)
(*Package Setup End*)


End[];
(*What do I need to protect??*)
Protect[f];
(*Protect[SetDateString,SetFileStructure,SetObjectiveFunction,f,Optimize,StartRun,LoadData];
Protect[Constraints,TmpSaveFreq,TmpFileName];
Protect[ForceSerial,WriteOutFile,OutFileComment,SuppressErrors,DataDir,DataSuffix,DataPrefix,OutSuffix,TmpDataInfix];*)
EndPackage[];
