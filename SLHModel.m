(* ::Package:: *)

(* :Title: SLHModel.m  *)

(* :Author: Sebastian Hofer *)

(* :Discussion:

Simple data structure describing open quantum systems in the SLH formalism (see for example doi:10.1109/TAC.2009.2031205)
All operators must be defined as quOp's from the QuantumCVAlgebra package.

The SLH model contains the following elements:
- S = scattering matrix: 2d quadratic quList with the same length as L
- L = list of jump operators: 1d quList
- H = system Hamiltonian: quOp quadratic in quadrature operators (linear terms can be treated by going to a displaced frame)
*)

(* Example: 1d damped harmonic oscillator

x=quOp[1,{1,{1,0}}];
p=quOp[1,{1,{0,1}}];
slhm=slhModel[{},quList[{Sqrt[k](x+I p)}],f(x^2+p^2)/2]

*)

BeginPackage["SLHModel`",{"QuantumCVAlgebra`","GeneralToolbox`"}]
(* Exported symbols added here with SymbolName::usage *)  

slhModel::usage = "slhModel[S,L,H] describes a Hudson-Parthasarathy model for a quantum stochastic system (S=scattering matrix, L=list of jump operators, H=Hamiltonian)."
slhModelSeriesConnect::usage = "slhModelSeriesConnect[slh_1,slh_2] connect outputs of system 1 to inputs of system 2.
slhModelSeriesConnect[slh_1,...,slh_n] connect systems 1 to n in series."
slhModelConcatenate::usage = "slhModelConcatenate[slh_1,...,slh_n] concatenate systems 1 to n (without connecting them)."
slhModelOptions::usage = "slhModelOptions[slh] returns list of options of slh."

OutputChannels::usage = "Defines measured channels (takes same arguments as Part)";

Begin["`Private`"] (* Begin Private Context *) 

Unprotect[slhModel];Clear[slhModel];
With[
	{
		(*internal systemModel structure*)
		sqp={S:_quList|{{}}, L:_quList|{{}}, H:_quOp|_quSum|{}}
	},

	
	(*define slhModel*)
	(*Note: An empty L is represented by {{}} to be consistent with quList[{...}], 
	        but is translated to {} on output!*)
	slhModel/:slhModel[{{{}},{{}},_}, OptionsPattern[]]["S"|"L"]:={};
	slhModel/:slhModel[{{{}},L_,_}, OptionsPattern[]]["S"]:=quList@ConstantArray[quOp[0,0],{1,1}*Length@@L];
	slhModel/:slhModel[{S_,{{}},{}}, OptionsPattern[]]["L"]:=quList@ConstantArray[quOp[0,0],Length@@S];
	slhModel/:slhModel[{S_,{{}},{}}, OptionsPattern[]]["H"]:=quOp[0,0];
	slhModel/:slhModel[{S_,{{}},H_/;!MatchQ[H,{}]}, OptionsPattern[]]["L"]:=quList@ConstantArray[quOp[0,0],Length@@S];
	slhModel/:slhModel[{_,L_,{}}, OptionsPattern[]]["H"]:=quOp[0,0];
	slhModel/:slhModel[sqp, OptionsPattern[]]["S"]:=S;
  slhModel/:slhModel[sqp, OptionsPattern[]]["S","out"]:=
      With[{channels=OptionValue[OutputChannels]},
        S[[channels, channels]]];
	slhModel/:slhModel[sqp, OptionsPattern[]]["L"]:=L;
  slhModel/:slhModel[sqp, OptionsPattern[]]["L","out"]:=
      With[{channels=OptionValue[OutputChannels]},
        L[[channels, channels]]];
	slhModel/:slhModel[sqp, OptionsPattern[]]["H"]:=H;
  slhModel/:slhModel[sqp, opts:OptionsPattern[]]["options"]:={opts};
  slhModel/:slhModel[args:sqp, OptionsPattern[]][opts:OptionsPattern[]]:=slhModel[args, opts];


  (*Options*)
  Options[slhModel] = {OutputChannels -> All};
  slhModelOptions[slhModel[sqp, opts:OptionsPattern[]]] := Flatten@{opts};


	(*constructor*)
	lreplace[{}]:={{}}; lreplace[l_quList]:=l;
	slhModel/:slhModel[S:_quList|{}, L:_quList|{}, H:_quOp|_quSum|{}, opts:OptionsPattern[]]:=slhModel[{lreplace@S,
    lreplace@L,H}, opts];
	
	
	(*custom format*)
	Attributes@dispf = {HoldFirst};
	dispf[mat_,sup_]:=DisplayForm@SuperscriptBox[mat,sup];
	Format[sm:slhModel[sqp, OptionsPattern[]]]:=dispf[{MatrixForm@sm["S"],Evaluate@sm["L"],Evaluate@sm["H"]},
    "\[ScriptS]\[ScriptL]\[ScriptH]"];
	
	
	(*define datatypes and enforce stict typing*)
	(*Note: this has to be done after all other definitions!*)
	slhModel[args_, OptionsPattern[]] /; Not[Or[MatchQ[args,sqp],Message[slhModel::strw];False]]:=Throw[$Failed, slhModel]; (*structure must match*)
	slhModel[sqp, OptionsPattern[]] /; Not[
			Or[Length@@S==0,Length@Dimensions@S==2 && Equal@@Dimensions@S,Message[slhModel::sdim];False] (*S must be a square matrix*)
			&& Or[Length@@L==0,Length@Dimensions@L==1,Message[slhModel::ldim];False] (*L must be a vector*)
			&& Or[Length@@L==0,Length@@S==0,Length@@S==Length@@L,Message[slhModel::sldim];False] (*dimensions of S and L must match*)
			&& Or[Equal@@Select[Flatten@quDimensions@L,Positive],Message[slhModel::lopdim];False] (*operator dimensions in L must be equal or zero*)
			&& Or[Length@H==0,Length@@L==0,quDimensions@H=={0},Union@quDimensions@L=={{0}},
				Select[Flatten@Union@quDimensions@L,Positive] == quDimensions@H,Message[slhModel::hlopdim];False] (*operator dimensions of H and L must be equal, or zero*)
		]:=Throw[$Failed, slhModel];
	slhModel::strw = "Incorrect argument structure.";
	slhModel::sdim = "S must be a square matrix.";
	slhModel::ldim = "L must be a 1 dimensional list or {}.";
	slhModel::sldim = "Array dimensions of S and L must match.";
	slhModel::lopdim = "Operator dimensions in L must all be equal.";
	slhModel::hlopdim = "Operator dimensions in L and H must be equal.";
]

slhModelSeriesConnect[slh1_slhModel,slh2_slhModel]:=
    Module[{sdim=Max[Length@#,1]&/@{slh1["S"],slh2["S"]}},
      If[!Developer`EmptyQ@Union[slhModelOptions@slh1,slhModelOptions@slh2],
        Message[slhModelTransform::ignropts]
      ];
      slhModel[
        slh2["S"].slh1["S"],
        slh2["L"]+slh2["S"].slh1["L"],
        With[{p1=slh2["S"].slh1["L"]/(2 I),p2=ConjugateTranspose@slh2["S"].slh2["L"]/(2 I)},
          slh1["H"]+slh2["H"]+ (Conjugate@slh2["L"].p1-Conjugate@slh1["L"].p2)
        ]
      ]
    ];
slhModelSeriesConnect[slhlast_slhModel,slhrest:_slhModel..]:=Fold[slhModelSeriesConnect,slhlast,{slhrest}];

(*
shiftOutputs[list:{__Integer},n_Integer:1]:=list+n;
shiftOutputs[a_Integer;;b_Integer,n_Integer:1]:=Span[a+n,b+n];
shiftOutputs[OutputChannels->def_,n_Integer:1]:=OutputChannels->shiftOutputs[def,n];
shiftOutputs[{},_]:={};
*)

slhModelConcatenate[slhl1_slhModel,slhl2_slhModel]:=
    Module[{sdim=Max[Length@#,1]&/@{slhl1["S"],slhl2["S"]},slh1,slh2},
      If[!Developer`EmptyQ@Union[slhModelOptions@slh1,slhModelOptions@slh2],
        Message[slhModelTransform::ignropts]
      ];
      slh1=changeDim[slhl1,sdim[[2]]];
      slh2=shiftModes[changeDim[slhl2,sdim[[1]]],sdim[[1]]];
      slhModel[
        quList@ArrayFlatten[{{slh1["S"][[1]],quOp[{0,0},0]},{quOp[{0,0},0],slh2["S"][[1]]}}],
        quList@Join[slh1["L"][[1]],slh2["L"][[1]]],
        slh1["H"]+slh2["H"]
      ]
    ];
slhModelConcatenate[slhfirst_slhModel,slhrest:_slhModel..]:=Fold[slhModelConcatenate,slhfirst,{slhrest}];

slhModelTransform::ignropts="Model options are ignored.";

(*overload quantumCVAlgebra` methods*)
slhModel /: quDimensions[slh_slhModel]:=List@Max[{quDimensions@slh["H"],quDimensions@slh["L"]}];


(*Developer*)
changeDim[slh_slhModel,n_Integer:1]:=slhModel@QuantumCVAlgebra`Private`changeDim[slh[[1]],n];
shiftModes[slh_slhModel,n_Integer:1]:=slhModel@QuantumCVAlgebra`Private`shiftModes[slh[[1]],n];


End[] (* End Private Context *)

Protect[slhModel];

EndPackage[]
