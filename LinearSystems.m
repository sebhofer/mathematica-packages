(* ::Package:: *)

(* :Title: LinearSystems.m  *)

(* :Author: Sebastian Hofer *)


BeginPackage["LinearSystems`", {"SystemModel`","GeneralToolbox`"}]
(* Exported symbols added here with SymbolName::usage *)  

outputSpectralDensity::usage = "outputSpectralDensity[systemmodel,x] gives the spectrum of the output as a function of x."
stateSpectralDensity::usage = "stateSpectralDensity[systemmodel,x] gives the spectrum of the state variables as a function of x."
crossSpectralDensity::usage = "crossSpectralDensity[systemmodel,x] gives the cross spectral density between the output and the state."
steadyState::usage="steadyState[systemmodel] returns the symmetrized intracavity covariance matrix \[LeftAngleBracket]\!\(\*SubscriptBox[\(x\), \(i\)]\)\!\(\*SubscriptBox[\(x\), \(j\)]\) + \!\(\*SubscriptBox[\(x\), \(j\)]\)\!\(\*SubscriptBox[\(x\), \(i\)]\)\[RightAngleBracket]/2 for the systemModel sm. Shot noise unit is 1/2.";
stableQ::usage="stableQ[systemmodel] returns True iff systemmodel is stable (i.e. iff all eigenvalues of systemmodel[\"F\"] have negative real parts), by applying the Routh-Hurwitz theorem. This returns conditions for the system to be stable, and can thus be used with symbolic matrices.";

Begin["`Private`"] (* Begin Private Context *) 

stateTransferFunction[sm_systemModel,x_]:=<
	-Inverse[I*x*IdentityMatrix@Length@sm["F"]+sm["F"]];

stateSpectralDensity[sm_systemModel,x_]:=
	Module[{tf},
		tf[y_]:=stateTransferFunction[sm,y].sm["L"];
		(*return*)
		tf[x].sm["W"].Transpose[tf[-x]]
	];

outputSpectralDensity[sm_systemModel,x_]:=
	Module[{tf},
		tf[y_]:=stateTransferFunction[sm,y].sm["L"];
		(*return*)
		sm["H"].stateSpectralDensity[sm,x].Transpose@sm["H"]+sm["N"]
		+(sm["H"].tf[x].sm["M"]+Transpose[sm["H"].tf[-x].sm["M"]])
	];	

crossSpectralDensity[sm_systemModel,x_]:=
	Module[{tf}, 
		tf[y_]:=stateTransferFunction[sm,y].sm["L"];
		(*return*)
		stateSpectralDensity[sm,x].Transpose@sm["H"]+tf[x].sm["M"]
	]
	
	
(*steady state solutions*) 
steadyState[sm_systemModel,options:OptionsPattern[]]:=
	Module[
		{method=OptionValue[Method]},
		If[(method===Automatic)&&(Or@@InexactNumberQ/@Flatten[List@@sm]),method="Lyapunov",method="Integral"];
		(*return*)
		Switch[method,
			"Integral",steadyStateIntegral[sm,Evaluate@FilterRules[{options},Options@steadyStateIntegral]],
			"Lyapunov",steadyStateLyapunov[sm,Evaluate@FilterRules[{options},Options@steadyStateLyapunov]],
			_,Message[steadyState::umeth,method];steadyState[sm,Evaluate@FilterRules[Options@steadyState,Method->_][[1]]]
		]
	];
Options[steadyState]={Method->Automatic};
steadyState::umeth="Unknown method `1`. Falling back to default.";
steadyState::instab="System is not stable. Resulting steady state will not be correct.";


steadyStateLyapunov[sm_systemModel,OptionsPattern[]]:=
	LyapunovSolve[sm["F"],-sm["L"].sm["W"].Transpose@sm["L"]];


steadyStateIntegral[sm_systemModel,options:OptionsPattern[]]:=
	Module[
		{x,h,Q,num,cov,IDdim},
		(*the sign of ix depends on the definition of the fourier transform!
		here: a (x)~\[Integral]\[DifferentialD]t a (t) E^ixt, x (x)~\[Integral]\[DifferentialD]t x (t) E^ixt,... -> -ix*)
		IDdim=IdentityMatrix@Dimensions@sm["F"];
		h=Simplify@Det[I*x*IDdim+sm["F"]];(*-->denominator*)
		Q=Simplify[h*Inverse[I*x*IDdim+sm["F"]]];(*-->numerator matrix*)
		num=Q.sm["L"].sm["W"].Transpose@sm["L"].Transpose[Q/.x->-x];
		cov=gradshteynIntegralMatrix[num,h/.x->-x,x]/(2*Pi);
		(*return*)
		cov
	];
Options[steadyStateIntegral]={};


stableQ[sm_systemModel]:=And@@routhHurwitzCriteria@sm["F"];


End[] (* End Private Context *)

EndPackage[]
