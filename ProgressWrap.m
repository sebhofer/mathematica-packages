(* ::Package:: *)

(* :Title: ProgressWrap.m  *)

(* :Author: Sebastian Hofer *)

(* :Discussion:

Monitor sequential computations and display progress as a progress bar.

Examples:

progressTable[Pause[0.5];i,{i,0,5}]

f[i_]:=(Pause[0.5];i);
{fwrapped,counter}=progressWrap[f];
progressWrapMonitor[counter,6,Table[fwrapped[i],{i,0,5}]]
*)


BeginPackage["ProgressWrap`",{"GeneralToolbox`"}]


counterInit::usage="counterInit[] returns a counter.";
counterIncrement::usage="counterIncrement[counter] increments counter.";
counterState::usage="counterState[counter] queries the state of counter.";
counterReset::usage="counterReset[counter] resets the counter to 0.";
progressWrap::usage="progressWrap[sym] returns {wsym,counter} where wsym is the wrapped version of sym. Calling wsym increments the counter. The counter state can be queried with counterState[counter].";
progressMap::usage="progressMap[f,exp,{lvl}] maps f over exp on level lvl and displays a progress bar.";
progressTable::usage="progressTable[exp,iterator] evaluates table and displays a progress bar.";
progressParallelTable::usage="progressParallelTable[exp,iterator] evaluates table on parallel kernels and displays a
progress bar.";
progressFoldList::usage="progressFoldList[f,x,list] evaluates FoldList and displays a progress bar.";
progressWrapMonitor::usage="progressWrapMonitor[counter, length, exp] creates Monitor and evaluates exp.
progressWrapMonitor[counter, length][exp] same as above in operator form.";
progressWrapParallelSetup::usage="progressWrapParallelSetup[counter] does necessary setup for parallel computation.";
(*options*)
IteratorFunction::usage="Option specifying iterator for Table like functions";
ParallelSetup::usage="Option triggering progressWrapParallelSetup";

Begin["`Private`"]


counterInit[]:=Module[{val=0,counter},
	counterState[counter]:=val;
	counterReset[counter]:=(val=0);
	counterIncrement[counter]:=(++val);
	counter
];
SetSharedFunction[counterIncrement];


Clear@progressWrapParallelSetup
progressWrapParallelSetup[count_]:=(SetSharedVariable@count; DistributeDefinitions["ProgressWrap`Private`"]; DistributeDefinitions["ProgressWrap`"]);
Attributes@progressWrapParallelSetup={HoldAll};


progressWrapBar[count_, len_] := {
	ProgressIndicator[Dynamic[counterState@count], {0, len}],
	{Dynamic[counterState@count], len}
};


progressWrapMonitor[count_, len_] := Function[, Monitor[#, progressWrapBar[count, len]], HoldAll];
progressWrapMonitor[count_, len_, exp_] := Monitor[exp, progressWrapBar[count, len]];
Attributes[progressWrapMonitor] = {HoldAll};


Clear@progressWrap
progressWrap[sym_]:=Module[{count=counterInit[],wrapped},
	(*evaluate sym first, then increment*)
	wrapped[exp___]:=First@{sym@exp,counterIncrement[count]};{wrapped,count}];


Clear@progressMap
progressMap[f_,exp_]:=progressWrap[f,exp,{1}];
progressMap[f_,exp_,{lvl_}]:=Module[{len,wfunc,count},
	{wfunc,count}=progressWrap[f];
	len=Length@Level[Normal@exp,{lvl}];
	Monitor[
		Map[wfunc,exp,{lvl}], progressWrapBar[count, len]
  ]
];
progressMap[f_,exp_]:=progressMap[f,exp,{1}];


Clear[progressTable,lenE]
(*calculates number of elements for each iterator*)
lenE[{___,imax_}]:=imax;
lenE[{_,i0_,imax_}]:=imax-i0+1;
lenE[{_,i0_,imax_,di_}]:=Floor[(imax-i0)/di]+1;
lenE[{_,ilist_List}]:=Length@ilist;

progressTable[exp_,int__,OptionsPattern[]]:=Module[{len,count},
	len=Times@@lenE/@{int};
	count=counterInit[];
	If[OptionValue[IteratorFunction]===ParallelTable || OptionValue[ParallelSetup], 
		progressWrapParallelSetup@count];
	Monitor[
		(*evaluate exp first, then increment*)
		OptionValue[IteratorFunction][First@{exp,counterIncrement@count},int], progressWrapBar[count, len]
  ]
];
Attributes@progressTable={HoldAll};
Options@progressTable={IteratorFunction->Table, ParallelSetup->False};
SyntaxInformation[progressTable]=SyntaxInformation[Table];


progressParallelTable[exp_,int__]:=progressTable[exp,int,IteratorFunction->ParallelTable];
Attributes@progressParallelTable={HoldAll};
SyntaxInformation[progressParallelTable]=SyntaxInformation[Table];


progressFoldList[f_,x_,l_List]:=Module[{len,count,wfunc},
	{wfunc,count}=progressWrap[f];
	len=Length@l;
	count=counterInit[];
	Monitor[
		FoldList[wfunc,x,l], progressWrapBar[count, len]
  ]
];


End[]


EndPackage[]
