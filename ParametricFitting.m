(* ::Package:: *)

(* ::Title:: *)
(*Parametric curve fitting*)


(* :Title: ParametricFitting.m  *)

(* :Mathematica Version: Mathematica 8 *)

(* :Author: Sebastian Hofer *)

(* :Package Version: 0.1 *)

(* :Discussion:

	Todos:
	- NAntiderivative...: Implement Method->Interpolation - faster??
	      
*)

BeginPackage["ParametricFitting`"]
(* Exported symbols added here with SymbolName::usage *)  

unfoldData;
normalizeData;
normalizeFunction;
parametricFit::usage=
"parametricFit[data, {fx, fy}, {{a, a0}, ...}, {t, t0, t1}, MinimizationMethod \[Rule] \"FindMinimum\"]
parametricFit[data, {{fx, fy}, constraints}, {{a, a0}, ...}, {t, t0, t1}, MinimizationMethod \[Rule] \"FindMinimum\"]
parametricFit[data, {{fx, fy}, constraints}, {{a, a0}, ...}, {t, t0, t1}, MinimizationMethod \[Rule] \"NMinimize\"]
  
Notes:
 - First argument data is a list of tuples, i.e. {{x1,y1},{x2,y2},...}
 - The third argument defines the fit parameters, see the documentation of FindMinimum/NMinimize for usage instructions.
 - The last argument {t, t0, t1} refers to the parametric dependence of the fit functions, i.e. fx = fx[t], fy = fy[t]; The parameter is minimized over the interval {t0,t1}.
   
Options:
 - MinimizationMethod: see above
 - FitMethod:
   - \"Sampling\" (default): fx and fy are (more or less) uniformly sampled over the interval {t0,t1}. The number of sample points can be adjusted by using CurveLengthOptions\[Rule]{SamplePoints\[Rule]n} (default is n=300).
   - \"Minimization\": Normally faster but less robust.
 - CurveLengthOptions: List of options used for calculation the curve length
	- SamplePoints: Number of points sampled for interpolation of curve length.
	- InitialPoints: Specifies values for fit parameters for which curve length is evaluated. Can be one of the following
		- {u\[Rule]u0,v\[Rule]v0,...}
		- {u0, v0,...} in order of appearance
		- Automatic
 - Scaling: {sx,sy} scales x-,y-axis by 1/sx, 1/sy respectively. (Only works for FitMethod\[Rule]\"Sampling\".)
 - SamplePoints: Number of points sampled from the objective function at each point in the fit parameter space.
 - All other (appropriate) options are passed to FindMinimum/NMinimize.
";
inverseCurveLength::usage="Calculates the inverse curvelength of the parametric function {fx[t],f[t]} in a given range {t,\!\(\*SubscriptBox[\(t\), \(0\)]\),\!\(\*SubscriptBox[\(t\), \(1\)]\)}.";


Begin["`Private`"]
(* Begin Private Context *) 



(* ::Subsection:: *)
(*Fitting Data*)


nAntiderivativeInverse[funcn_,range_List,options:OptionsPattern[]]:=
	Block[
		{
			stepsize,totallength,tdata,
			x1=0,
			steps=OptionValue[SamplePoints]
		},
		stepsize=Abs[Subtract@@Rest@range]/steps;
		totallength=NIntegrate[funcn,range];
		tdata=
			Table[{NIntegrate[funcn,Evaluate@{First@range,x1,x0}]/totallength,-x1+(x1=x0)},
				{x0,First@Rest@range,Last@Rest@range,stepsize}];
		(*return*)				
		Interpolation@Table[Total@tdata[[1;;n]],{n,1,Length@tdata}]
	];
Options[nAntiderivativeInverse]=Join[{SamplePoints->300},Options[NIntegrate]];


inverseCurveLength[{fx_,fy_},range_List,pars_List,options:OptionsPattern[]]:=
	Block[
		{
			dfx=Derivative[1][fx]//.pars,
			dfy=Derivative[1][fy]//.pars
		},
		(*return*)
		nAntiderivativeInverse[
			Sqrt[dfx[First@range]^2+dfy[First@range]^2],
			range,Evaluate@FilterRules[{options},Options[nAntiderivativeInverse]]]
	];
Options[inverseCurveLength]=Options[nAntiderivativeInverse];


generateFitData[fx_,fy_,fscale_,parameters_,options:OptionsPattern[]]:=
	Module[
		{samplepoints=OptionValue[SamplePoints]},
		(*return*)
		Table[{fx[fscale@x,Sequence@@parameters],fy[fscale@x,Sequence@@parameters]},{x,0,1,1/samplepoints}]
	];
Options[generateFitData]={SamplePoints->300};


totalLeastSquares[data_,fdata_]:=Total[Min/@Outer[EuclideanDistance,data,fdata,1]];


findClosestPoints[data_,fdata_]:=
	Block[{order},
		order=Ordering[#,1]&/@Outer[EuclideanDistance,data,fdata,1];
		(*return*)
		Table[{data[[i]],First@fdata[[order[[i]]]]},{i,1,Length[data]}]
	];


normalizeData[data_,scale_List,OptionsPattern[]]:=
	Block[{pos=OptionValue[Position]},
		pos=If[pos===All,Range@Length@scale,pos];
		(*return*)
		Divide[#,ReplacePart[ConstantArray[1,Last@Dimensions@data],Thread[Rule[pos,scale]]]]&/@data
	];
	
normalizeData[data_,options:OptionsPattern[]]:=
	Block[{pos=OptionValue[Position]},
		pos=If[pos===All,Range@Last@Dimensions@data,pos];
		(*return*)
		normalizeData[data,Sow@Map[Max,Transpose[data][[pos]]],options]
	];
normalizeFunction[func_,scale_List,options:OptionsPattern[]]:=Flatten@normalizeData[{func},scale,options];
Options[normalizeData]=Options[normalizeFunction]={Position->All};


(*NormalizeData[data_,scale_List,pos_List]:=Divide[#,Normal@SparseArray@Thread[Rule[pos,scale]]]&/@data;
NormalizeData[data_,pos_List]:=NormalizeData[data,Sequence@@Sow@Map[Max,Transpose[data][[pos]]],pos];*)
(*NormalizeData[data_,pos_List]:=NormalizeData[data,Sequence@@Sow@Map[Max,Transpose[data][[pos]]],pos];*)
(*NormalizeData[data_,scale__]:=Divide[#,{scale}]&/@data;*)
(*NormalizeData[data_,scale_List]:=NormalizeData[data,Sequence@@scale]*)


unfoldData[data_List,{fx_,fy_},varrange_List,options:OptionsPattern[]]:=
	Block[
		{
			t,pos,fscale,ffx,ffy,dfx,dfy,
			samplepoints=OptionValue[SamplePoints]
		},
		ffx=Function[Evaluate@First@varrange,Evaluate@fx];
		ffy=Function[Evaluate@First@varrange,Evaluate@fy];
		dfx=Derivative[1][ffx];
		dfy=Derivative[1][ffy];

		fscale=nAntiderivativeInverse[
			Sqrt[dfx[First@varrange]^2+dfy[First@varrange]^2],
			varrange,Evaluate@FilterRules[{options},Options[nAntiderivativeInverse]]];
		t=Table[{fscale@x,{ffx@fscale@x,ffy@fscale@x}},{x,0,1,1/samplepoints}];
		pos=Position[t,{_,#}]&/@findClosestPoints[data,t[[All,2]]][[All,2]];
		
		(*return*)
		Partition[Riffle[t[[Flatten@pos,1]],#],2]&/@Transpose@data
	];
Options[unfoldData]={SamplePoints->2000};


parametricFitSampling[data_List,{func_List,constraints___},fitparameters_List,varrange_List,options:OptionsPattern[]]:=
	Block[
	{
		fitparametername,init,initpoints,mf,args,
		fx,fy,dfx,dfy,
		fscale,ferror,fe,gfd,
		minres,sdata,sfunc,
		varlist=Prepend[First/@fitparameters,First@varrange],
		method=OptionValue[MinimizationMethod],
		compile=OptionValue[Compile],
		curvelengthoptions=OptionValue[CurveLengthOptions],
		scaling=OptionValue[Scaling]
	},
	fitparametername=Switch[#,List[__List],First/@#,List[__],#]&@fitparameters;
	mf=Switch[method,"FindMinimum",FindMinimum,"NMinimize",NMinimize,_,FindMinimum];

	(*options for curve length - TEST THIS!!!*)
	initpoints=FilterRules[curvelengthoptions,InitialPoints];
	init=Switch[If[initpoints=!={},initpoints,Automatic],
			List[_Rule..],ReplaceAll[fitparametername,initpoints],
			List[_?AtomQ..],initpoints,
			Automatic,Switch[#,(*Automatic*)
				List[__List?(Length@#==2&)],Last/@#,
				List[__List?(Length@#==3&)],Map[(First@#+Last@#)/2&,Rest/@#]]&@fitparameters,
			_,Message[curvelength::unropt]
		];

	(*Rescale data + functions*)
	sdata=normalizeData[data,scaling];
	sfunc=normalizeFunction[func,scaling];

	(*DEBUG*)
	(*Print[fitparametername];
	Print[init];*)

	fx=Function[Evaluate@varlist,Evaluate@First@sfunc];
	fy=Function[Evaluate@varlist,Evaluate@Last@sfunc];
	dfx=Function[Evaluate@varlist,Evaluate@D[fx@@varlist,First@varrange]];
	dfy=Function[Evaluate@varlist,Evaluate@D[fy@@varlist,First@varrange]];

	(*DEBUG*)
	(*Print[fx];
	Print[dfx];*)

	fscale=nAntiderivativeInverse[
		Sqrt[dfx[First@varrange,Sequence@@init]^2+dfy[First@varrange,Sequence@@init]^2],
		varrange,Evaluate@FilterRules[{curvelengthoptions},Options[nAntiderivativeInverse]]];
	gfd=Switch[compile,
		True,Compile[Evaluate@Map[{#,_Real}&,fitparametername],
			Evaluate@generateFitData[fx,fy,fscale,fitparametername,
				Evaluate@FilterRules[{options},Options[generateFitData]]]],
		False,Function[Evaluate@Map[#&,fitparametername],
			Evaluate@generateFitData[fx,fy,fscale,fitparametername,
				Evaluate@FilterRules[{options},Options[generateFitData]]]]];
	ferror[pars__]:=totalLeastSquares[sdata,gfd@pars];
	fe[pars__]:=ferror[pars]/;And@@NumericQ/@{pars};

	(*DEBUG*)
	(*Print[gfd[Sequence@@init]];
	Print[ferror[Sequence@@init]];
	Print[{fe[Sequence@@fitparametername],constraints}];*)
	
	If[constraints===Null,
		args=fe[Sequence@@fitparametername],
		args={fe[Sequence@@fitparametername],constraints}];
	minres={Sow[#1,Residuals],#2}&@@mf[args,Evaluate@fitparameters];
	
	(*return*)
	Last@minres
];


parametricFitMinimization[data_List,{func_List,constraints___},fitparameters_List,varrange_List,options:OptionsPattern[]]:=
	Block[
	{
		fitparametername,mf,args,
		fx,fy,ferror,fe,
		minres,sdata,sfunc,
		lconstraints=constraints,
		varlist=Prepend[First/@fitparameters,First@varrange],
		tparameters=Array[Unique[`tp],Length@data],
		method=OptionValue[MinimizationMethod],
		compile=OptionValue[Compile],
		scaling=OptionValue[Scaling]
	},
	fitparametername=Join[Switch[#,List[__List],First/@#,List[__],#]&@fitparameters,tparameters];
	mf=Switch[method,"FindMinimum",FindMinimum,"NMinimize",NMinimize,_,FindMinimum];

	(*Rescale data + functions*)
	sdata=normalizeData[data,scaling];
	sfunc=normalizeFunction[func,scaling];

	(*DEBUG*)
	(*Print[fitparametername];*)

	fx=Function[Evaluate@varlist,Evaluate@First@sfunc];
	fy=Function[Evaluate@varlist,Evaluate@Last@sfunc];

	(*DEBUG*)
	(*Print[fx];
	Print@fitparameters;*)

	(*This uses the wrong function (not rescaled)!!*)
	ferror=Switch[compile,
		True,Compile[Evaluate@Map[{#,_Real}&,fitparametername],
			Evaluate@Total@Map[EuclideanDistance[func/.First@varlist->First@#,Last@#]^2&,Thread[List[tparameters,data]]]],
		False,Function[Evaluate@Map[#&,fitparametername],
			Evaluate@Total@Map[EuclideanDistance[func/.First@varlist->First@#,Last@#]^2&,Thread[List[tparameters,data]]]]];
	(*Print["fitparametername:",fitparametername];*)
	(*ferror=Switch[compile,
		True,Compile[Evaluate@Map[{#,_Real}&,fitparametername],
			Evaluate@Total@Map[EuclideanDistance[{fx[First@#,Sequence@@Rest@varlist],fy[First@#,Rest@varlist]},Last@#]^2&,Thread[List[tparameters,data]]]],
		False,Function[Evaluate@Map[#&,fitparametername],
			Evaluate@Total@Map[EuclideanDistance[{fx[First@#,Sequence@@Rest@varlist],fy[First@#,Rest@varlist]},Last@#]^2&,Thread[List[tparameters,data]]]]];*)
	fe[pars__]:=ferror[pars]/;And@@NumericQ/@{pars};

	(*DEBUG*)
	(*Print[ferror[Sequence@@init]];
	Print[{fe[Sequence@@fitparametername],constraints}];*)
	lconstraints=If[MatchQ[varrange,List[_,_,_]],
		Join[{lconstraints},First@Rest@varrange<=#<=Last@varrange&/@tparameters],
		{lconstraints}];
	(*DEBUG*)
	(*Print[lconstraints];*)
	If[{lconstraints}==={},
		args=fe[Sequence@@fitparametername],
		args={fe[Sequence@@fitparametername],Sequence@@lconstraints}];
	(*Print@args;
	Print@Evaluate[Join[fitparameters,tparameters]];*)
	minres={Sow[#1,Residuals],#2}&@@mf[args,Evaluate[Join[fitparameters,tparameters]]];
	Sow[FilterRules[Last@minres,tparameters][[All,2]],Positions];
	
	(*return*)
	FilterRules[Last@minres,Except@tparameters]
];


(*SLOW - not useable atm*)
parametricFitMinimizationMultistage[data_List,{func_List,constraints___},fitparameters_List,varrange_List,options:OptionsPattern[]]:=
	Block[
	{
		fitparametername,mf,args,
		fx,fy,ferror,fe,femin,ferrorn,
		minres,sdata,sfunc,
		lconstraints=constraints,
		tconstraints,
		varlist=Prepend[First/@fitparameters,First@varrange],
		tparameters=Array[Unique[`tp],Length@data],
		method=OptionValue[MinimizationMethod],
		compile=OptionValue[Compile],
		scaling=OptionValue[Scaling]
	},
	fitparametername=Switch[#,List[__List],First/@#,List[__],#]&@fitparameters;
	mf=Switch[method,"FindMinimum",FindMinimum,"NMinimize",NMinimize,_,FindMinimum];

	(*Rescale data + functions*)
	sdata=normalizeData[data,scaling];
	sfunc=normalizeFunction[func,scaling];

	(*DEBUG*)
	(*Print[fitparametername];*)

	fx=Function[Evaluate@varlist,Evaluate@First@sfunc];
	fy=Function[Evaluate@varlist,Evaluate@Last@sfunc];

	(*DEBUG*)
	(*Print[fx];
	Print@fitparameters;*)
	tconstraints=Sequence@@If[MatchQ[varrange,List[_,_,_]],
		First@Rest@varrange<=#<=Last@varrange&/@tparameters,{}];

	ferror=Switch[compile,
		True,Compile[Evaluate@Map[{#,_Real}&,Join[fitparametername,tparameters]],
			Evaluate@Total@Map[EuclideanDistance[func/.First@varlist->First@#,Last@#]^2&,Thread[List[tparameters,data]]]],
		False,Function[Evaluate@Map[#&,Join[fitparametername,tparameters]],
			Evaluate@Total@Map[EuclideanDistance[func/.First@varlist->First@#,Last@#]^2&,Thread[List[tparameters,data]]]]];
	ferrorn[pars__]:=ferror[pars]/;And@@NumericQ/@{pars};
	If[{tconstraints}==={},
		femin[pars__]:=FindMinimum[ferrorn[pars,Sequence@@tparameters],tparameters,Evaluate@FilterRules[{options},Options[mf]]],
		femin[pars__]:=FindMinimum[{ferrorn[pars,Sequence@@tparameters],tconstraints},tparameters],Evaluate@FilterRules[{options},Options[mf]]];
	fe[pars__]:=First@femin[pars]/;And@@NumericQ/@{pars};
 
	(*DEBUG*)
	(*Print[ferror[Sequence@@init]];
	Print[Timing@ferrorn[.1,260,Sequence@@Reverse@Range[Length@tparameters]]];
	Print[Timing@fe[.1,260]];*)

	(*DEBUG*)
	(*Print[lconstraints];*)
	If[{lconstraints}==={},
		args=fe[Sequence@@fitparametername],
		args={fe[Sequence@@fitparametername],lconstraints}];
	minres={Sow[#1,Residuals],#2}&@@mf[args,Evaluate[fitparameters],Evaluate@FilterRules[{options},Options[mf]]];
	Sow[FilterRules[Last@minres,tparameters][[All,2]],Positions];
	
	(*return*)
	FilterRules[Last@minres,Except@tparameters]
];


parametricFit[data_List,func_List,fitparameters_List,varrange_,options:OptionsPattern[]]:=
	parametricFit[data,{func,Sequence[]},fitparameters,varrange,options];
parametricFit[data_List,{func_List,const___},fitparameters_List,varrange_,options:OptionsPattern[]]:=
	Switch[OptionValue[FitMethod],
		"Minimization",parametricFitMinimization[data,{func,const},fitparameters,varrange,Evaluate@FilterRules[{options},Options[parametricFitMinimization]]],
		"Sampling",parametricFitSampling[data,{func,const},fitparameters,varrange,Evaluate@FilterRules[{options},Options[parametricFitSampling]]],
		_,Message[parametricFit::fmndef]
	];
parametricFit::fmndef="No such fit method defined.";


parametricFitMinimization[data_List,func_List,fitparameters_List,varrange_,options:OptionsPattern[]]:=
	parametricFitMinimization[data,func,fitparameters,{varrange},options];
Options[parametricFitSampling]=Join[{MinimizationMethod->"FindMinimum",Compile->True,CurveLengthOptions->{},Scaling->{1,1}},
	Options[NMinimize],Options[FindMinimum],Options[generateFitData]];
Options[parametricFitMinimization]=Join[{MinimizationMethod->"FindMinimum",Compile->True,Scaling->{1,1}},Options[NMinimize],Options[FindMinimum]];
Options[parametricFitMinimizationMultistage]=Options[parametricFitMinimization];
Options[parametricFit]=Union[Options[parametricFitSampling],Options[parametricFitMinimization],{FitMethod->"Sampling"}];
curvelength::unropt="CurveLengthOptions does not have the correct form.";


End[] (* End Private Context *)

EndPackage[]
