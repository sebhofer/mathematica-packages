(* ::Package:: *)

(**)


(* ::Title:: *)
(*General Toolbox*)


(* :Title: GeneralToolbox.m  *)

(* :Mathematica Version: Mathematica 8 *)

(* :Author: Sebastian Hofer *)

(* :Package Version: 0.1 *)


BeginPackage["GeneralToolbox`"]


fPrint::usage = "fPrint[exp] prints and returns exp.";
fPrintTemporary::usage = "fPrintTemporary[exp] temporary prints and returns exp.";
fMatrixPrint::usage = "fMatrixPrint[list] prints lists in MatrixForm, other arguments in StandardForm.";
fPrintTiming::usage = "fPrintTiming[exp] times execution of expression, prints timing, and returns result.";
listSubtract::usage = "listSubtract[a,b] returns ordered list of elements from a which are not contained in b.
Example: listSubtract[{a,a,c,d,d,d},{a,a,d,e}] returns {c,d,d}";
listComplement::usage = "Example: listComplement[{a,a,c,d,d,d},{a,a,d,e}] returns {c,d,d,e}";
abs2::usage = "abs2[x] returns x*Conjugate@x.";
myRe::usage = "myReal[x] returns (x+Conjugate@x)/2 returns real part of  argument in expanded form.";
myIm::usage = "myIm[x] returns (x-Conjugate@x)/(2I) returns real part of  argument in expanded form.";
positiveSemiDefiniteMatrixQ::usage = "positiveSemiDefiniteMatrixQ checks if matrix is positive semidefinite.";
adjugateMatrix::usage = "adjugateMatrix[m] compute the adjugate of square matrix m";
blockDiagonalMatrix::usage="Takes a list or sequence of matrices and brings them into block form.";
selectMonotonicIncreasing::usage="selectMonotonicIncreasing[list] selects longest possible sublist from list with monotonic increasing elements.";
selectMonotonicDecreasing::usage="selectMonotonicDecreasing[list] selects longest possible sublist from list with monotonic decreasing elements.";
routhHurwitzCriteria::usage = "routhHurwitzCriteria[mat] checks if mat is a stable matrix, i.e. the real part of all its eigenvalues are negative.";
timeAvg::usage="timeAvg[func] evaluates func several times and returns average Timing.";
let::usage="let[{var1,var2,...},expr] expands to nested With.";
dFunction::usage="dFunction[pattern,body] creates anonymous function whose arguments must match pattern.";
defineFunction::usage="defineFunction[sym,body,varlist] defines DownValues to sym where vars is a List of symbols, i.e. {x1,x2,...}
defineFunction[sym,body,patternlist] defines DownValues to sym where vars is a List of patterns, e.g. {x1_,x2_,...}";
defineNFunction::usage="defineNFunction[sym,body,varlist] defines DownValues to sym where vars is a List of symbols, i.e. {x1,x2,...}. A NumericQ pattern test is automatically appended to all symbols.";
gradshteynIntegral;
gradshteynIntegralMatrix;
memoryMonitor::usage="memoryMonitor[] dynamically monitors used memory.";
memoryUsageHeavySymbols::usage="memoryUsageHeavySymbols[context,limit] displays symbols in context with memory usage
above limit.";
memoryUsageSymbol::usage="memoryUsageSymbol[symbol] displays memory usage of symbol.";
extractCases::usage="extractCases[exp, pat] extracts subexpressions from exp which match pat
 extractCases[exp, pat:>rep] extracts subexpressions and apply replacement rule.";
abortableTable::usage="abortableTable[exp,iterator] is equivalent to Table[exp,iterator] but returns a partial list
of results on abort.";
abortableParallelTable::usage="abortableParallelTable[exp,iterator] is equivalent to ParallelTable[exp,iterator] but
returns a partial list of results on abort. Note that definitions are not distributed automatically!";

(*obsolete*)
myReal::usage="Obsolete. Use myRe instead!";


Begin["`Private`"](* Begin Private Context *)


(* ::Section:: *)
(*Functional printing*)


fPrint[]:=(Print@"NullSequence";Unevaluated@Sequence[])
fPrint[""]:=(Print@"NullString";"")
fPrint[x_]:=(Print@x;x)
fPrint[x__]:=(Print@{x};Sequence@@{x})


fPrintTemporary[]:=(PrintTemporary@"NullSequence";Unevaluated@Sequence[])
fPrintTemporary[""]:=(PrintTemporary@"NullString";"")
fPrintTemporary[x_]:=(PrintTemporary@x;x)
fPrintTemporary[x__]:=PrintTemporary@x;


fMatrixPrint[x_List] := (Print@MatrixForm@x; x)
fMatrixPrint[x___] := fPrint@x;


(*Attributes@fPrintTiming={HoldFirst};
fPrintTiming[exp_]:=(Print@#1;#2)&@@AbsoluteTiming[exp]*)


Clear@fPrintTiming;
Attributes@fPrintTiming={HoldFirst};
With[{tfl=AbsoluteTiming|Timing|AccurateTiming,attr=Attributes@fPrintTiming},
	fPrintTiming[tf:tfl]:=Function[exp,fPrintTiming[exp,tf],attr];
	fPrintTiming[exp:Except[tfl],tf:tfl:AbsoluteTiming]:=(Print@#1;#2)&@@tf[exp];
]


(* ::Section:: *)
(*Complex numbers*)


(*complex numbers*)
abs2@x_:=Refine[x*Conjugate@x];
myRe[x_] := (x + Refine@Conjugate@x)/2;
myIm[x_] := -I*(x - Refine@Conjugate@x)/2;
myReal=myRe; (*for backwards compatibility*)


(* ::Section:: *)
(*Matrix functions*)


If[$VersionNumber>=10,
	positiveSemiDefiniteMatrixQ[mat_List/;SameQ@@Dimensions@mat]:=PositiveSemidefiniteMatrixQ[mat],
	positiveSemiDefiniteMatrixQ[mat_List/;SameQ@@Dimensions@mat]:=
		Or[PositiveDefiniteMatrixQ@mat, And@@NonNegative/@Eigenvalues@mat]
];
(*verbose for all versions*)
positiveSemiDefiniteMatrixQ[mat_List/;SameQ@@Dimensions@mat,Verbose->True]:=
	{positiveSemiDefiniteMatrixQ@mat, SingularValueList@mat};


(*this was adapted from the Combinatorica` package, which seems to be obsolete*)
adjugateMatrix[m_?MatrixQ]:=Module[{dim=Dimensions@m},
	Table[(-1)^(i+j)*Det[Drop[Transpose[Drop[m,{i}]],{j}]],{j,1,dim[[2]]},{i,1,dim[[1]]}]
]


(*creates a block diagonal matrix from a list of matrices*)
blockDiagonalMatrix[mats_List]:=
	Module[{pl=0,dms,tln,matsl},
		matsl=Cases[mats,Except[{}]];
		tln=Plus@@(Last[Dimensions[#]]&/@matsl);
		Join@@
			(PadRight[#,{First[dms=Dimensions[#]],tln},
			0,{0,First[{pl,pl+=dms[[2]]}]}]&/@matsl)
	];
blockDiagonalMatrix[mats__]:=blockDiagonalMatrix[{mats}];



(* ::Section:: *)
(*List functions*)


selectMonotonicIncreasing[dat_List,var_]:=
	Module[{pos,fd},
		fd=Which[
			MatchQ[dat,List[Rule[_,_]..]|List[List[Rule[_,_]..]..]],var/.dat,
			MatchQ[dat,List[List[_]..]],Flatten@dat,
			True,dat
		];
		pos=Flatten[Position[fd,#]&/@LongestCommonSequence[fd,Sort[fd]]];
		dat[[pos]]
	];
selectMonotonicIncreasing[dat_List,vars_List]:=Module[{ldat=dat},
	Scan[(ldat=selectMonotonicIncreasing[ldat,#])&,vars];
	ldat];
selectMonotonicIncreasing[dat_List,vars__]:=selectMonotonicIncreasing[dat,{vars}];
selectMonotonicIncreasing[dat_List]:=LongestCommonSequence[dat, Sort@dat];
Attributes[selectMonotonicIncreasing]={HoldRest};


selectMonotonicDecreasing[dat_List]:=LongestCommonSequence[dat, Reverse@Sort@dat]


listSubtract[a_List,b_List,OptionsPattern[]] :=
	Module[{sametest=OptionValue[SameTest]},
		Apply[Join,
			Table[#1, {#2}]&@@@
				Transpose@With[
					{u = Union[a,b,SameTest->sametest]},
					{
						u,
						Subtract[
							Last/@Sort@Tally[Join[a, u],sametest],
							Last/@Sort@Tally[Join[b, u],sametest]
						]
					}
				]
		]
	];
Options[listSubtract]={SameTest->SameQ};


listComplement[a_List, b_List] := 
  Join[listSubtract[a, b], listSubtract[b, a]];


(* ::Section:: *)
(*Control theory*)


(*Compute Hurwitz matrix*)
(*cf. to master thesis, Appendix A .2.*)
hurwitzMatrix[clist_List,order_Integer:0]:=
	Module[
		{
			myclist=Reverse@clist,
			mydim=Length[clist]-1,
			myorder,myc,H
		},
		myorder=If[order==0,mydim,order];
		If[myorder>mydim,Message[hurwitzMatrix::order];myorder=mydim];
		myc[n_]/;(NonNegative[n]&&(n<=mydim)):=myclist[[n+1]];
		myc[n_]=0;
		H[n_,m_]:=myc[2n-m];
		Table[H[n,m],{n,1,myorder},{m,1,myorder}]
	];
hurwitzMatrix[poly_,var_,order_Integer:0]:=hurwitzMatrix[CoefficientList[poly,var],order];
hurwitzMatrix::order="Given order must not be larger than length of polynomial. Using length of polynomial instead.";
hurwitzMatrix::usage="Returns the Hurwitz matrix of the polynomial Sum[clist[[i+1]]*x^i,{i,0,n}]. 
If order is given, the matrix is truncated to dimension order x order";


routhHurwitzCriteria[mat_List]:=
	Module[{dim,\[Lambda],poly},
		dim=Length[mat];
		poly=Det[\[Lambda] IdentityMatrix[dim]-mat];
		(*return*)
		routhHurwitzCriteria[poly,\[Lambda]]
	];


routhHurwitzCriteria[poly_,var_]:=
	Module[{clist,dim,RH},
		clist=CoefficientList[poly,var];
		dim=Length@clist-1;
		RH=Table[hurwitzMatrix[clist,n],{n,1,dim}];
		(*return*)
		Thread[Greater[Sow[Det/@RH],0]]
	];


(* ::Section:: *)
(*Integration*)


(*
	Computes integrals of the form Integrate[p[x]/(q[x]q[-x]),{x,-Infinity,Infinity}], 
	where q and p are polynomials of order n and 2n-2 respectivly, where all roots of 
    q lie in the upper half-plane. This uses the Hurwitz matrix from above.
	(cf. to master thesis, Appendix A .6.)
*)
gradshteynIntegral[numerator_,h_,var_]:=
	Module[
		{
			orderh=Exponent[h,var],
			ordern=Exponent[numerator,var]/2,
			gList,Hh,M
		},
		If[numerator===0,0,
			If[orderh<ordern,Message[gradshteynIntegral::worder,ordern,orderh];Throw[worder]];
			Hh=hurwitzMatrix[h,var,orderh];
			(*drop odd-order terms in numerator as they do not contribute (due to symmetric region of integration)!!*)
			gList=PadLeft[Reverse[Drop[#,{2,Length[#],2}]],orderh]&@CoefficientList[numerator,var];
			M=ReplacePart[Transpose@Hh,1->gList];
			(*return*)
			I (-1)^(orderh+1)Pi/Coefficient[h,var,orderh]*Det[M]/Det[Hh]
		]
	];
Attributes[gradshteynIntegral]={Listable};
gradshteynIntegral::worder="Order of numerator (`1`) does not match order of denominator (`2`).";


gradshteynIntegralMatrix[numerator_List,h_,var_]:=
	Module[{},
		Map[gradshteynIntegral[#,h,var]&,numerator]
	];


(* ::Section:: *)
(*Timing*)


SetAttributes[timeAvg, HoldFirst]
timeAvg[func_] := Do[If[# > 0.3, Return[#/5^i]] & @@ Timing@Do[func, {5^i}], {i, 0, 15}]


(* ::Section:: *)
(*Scoping*)


(* 
	Macro expansion to nested With constructs
	http://mathematica.stackexchange.com/questions/10432/how-to-avoid-nested-with
*)


Unprotect[let];
ClearAll[let];
SetAttributes[let, HoldAll];
SyntaxInformation[let] = {
   "ArgumentsPattern" -> {_, _}, 
   "LocalVariables" -> {"Solve", {1, Infinity}}
};
let /: (assign : SetDelayed | RuleDelayed)[
  lhs_,rhs : HoldPattern[let[{__}, _]]
] := 
  Block[{With}, 
    Attributes[With] = {HoldAll}; 
    assign[lhs, Evaluate[rhs]]
  ];
let[{}, expr_] := expr;
let[{head_}, expr_] := With[{head}, expr];
let[{head_, tail__}, expr_] := 
  Block[{With}, Attributes[With] = {HoldAll};
    With[{head}, Evaluate[let[{tail}, expr]]]];
letL=let;(*old name*)
Protect[let];


Clear@defineFunction
Attributes@defineFunction={HoldRest};
defineFunction[s_Symbol,body_,vars_]/;MatchQ[vars,{__Symbol}]:=With[{h=s@@(Pattern[#,Blank[]]&/@vars)},(h:=body)];
defineFunction[s_Symbol,body_,pats_](*/;MatchQ[pats,{(_Pattern|_PatternTest|_Condition)..}]*):= With[{h=s@@pats},
	(h:=body)];
defineFunction[s_Symbol[pats___],body_]:=defineFunction[s,body,{pats}];

Clear@defineNFunction
Attributes@defineNFunction={HoldRest};
defineNFunction[s_Symbol,body_,vars_]/;MatchQ[vars,{__Symbol}]:=With[{syml=Thread[(Pattern[#,Blank[]]&/@vars)?NumericQ]},defineFunction[s,body,syml]];


(* ::Section:: *)
(*Anonymous destructuring*)


ClearAll@dFunction
SetAttributes[dFunction,HoldAll]
dFunction[pattern_,body_][arg___]/;MatchQ[{arg},pattern]:={arg}/.pattern:>body;
RightArrowBar = dFunction;



(* ::Section:: *)
(*Pattern Matching*)


Clear[extractCases]
extractCases[exp_, pat_] := Part[exp, ##] & @@@ Position[exp, pat];
extractCases[exp_, pat : _Rule | _RuleDelayed] :=
    Part[exp, ##] & @@@ Position[exp, pat[[1]]] /. pat;


(* ::Section:: *)
(*Memory Consumption*)


Clear[$globalProperties];
$globalProperties = {OwnValues, DownValues, SubValues, UpValues,
  NValues, FormatValues, Options, DefaultValues, Attributes,
  Messages};

ClearAll[getDefinitions];
SetAttributes[getDefinitions, HoldAllComplete];
getDefinitions[s_Symbol] :=
    Flatten@Through[
      Map[Function[prop,
        Function[sym, prop[sym], HoldAll]], $globalProperties][
        Unevaluated[s]]];

ClearAll[symbolMemoryUsage];
memoryUsageSymbol[sname_String] :=
    ToExpression[sname, InputForm,
      Function[s, ByteCount[getDefinitions[s]], HoldAllComplete]];

ClearAll[heavySymbols];
memoryUsageHeavySymbols[context_, sizeLim_: 10^6] :=
    Pick[#, UnitStep[# - sizeLim] &@Map[symbolMemoryUsage, #], 1] &@
        Names[context <> "*"];

memoryMonitor[]:=DynamicModule[{pm = {}},
  Dynamic@Refresh[pm = Append[pm, MemoryInUse[]];
  If[Length[pm] > 120, pm = Drop[pm, 1]];
  ListPlot[pm/1024/1024, AxesLabel -> {"Time [s]", "Memory [MB]"},
    PlotRange -> {0, All}], UpdateInterval -> 1,
    TrackedSymbols :> {}]];


(*Due to Leonid Shifrin, http://mathematica.stackexchange.com/a/7571/368*)
ClearAll[abortableTable];
SetAttributes[abortableTable, HoldAll];
abortableTable[expr_, iter : {_Symbol, __} .., OptionsPattern[]] :=
    Module[{indices, indexedRes, sowTag, depth =  Length[Hold[iter]] - 1},
      Hold[iter] /. {sym_Symbol, __} :> sym /. Hold[syms__] :> (indices := {syms});
      indexedRes =  Replace[#, {x_} :> x] &@ Last@Reap[
        CheckAbort[OptionValue[IteratorFunction][Sow[{expr, indices}, sowTag], iter], Null],sowTag];
      AbortProtect[
        SplitBy[indexedRes, Array[Function[x, #[[2, x]] &], {depth}]][[##,1]] & @@
            Table[All, {depth + 1}]
      ]];
Options[abortableTable] = {IteratorFunction -> Do};

ClearAll[abortableParallelTable];
SetAttributes[abortableParallelTable, HoldAll];
abortableParallelTable[expr_, iter : {_Symbol, __} ..] := abortableTable[expr, iter, IteratorFunction -> ParallelDo];

End[] (* End Private Context *)

EndPackage[]
