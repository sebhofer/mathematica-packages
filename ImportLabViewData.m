(* Mathematica Package *)

BeginPackage["ImportLabViewData`"]
(* Exported symbols added here with SymbolName::usage *)  

(*data structures*)
labViewDataConfig;
labViewData;

generateLabViewDataConfig::usage="Generator for labViewDataConfig"

loadLabViewData;
labViewFourierTransform;
labViewPowerSpectrum;

Begin["`Private`"]
(* Begin Private Context *) 


With[
	{
		sqc=Sequence[
				{basedirectory_String,workingdirectory_String},
				{samplerate_?NumericQ,voltagegain_?NumericQ,voltageoffset_?NumericQ},
				{fullspan_List,frequencylimits_List},
				{dataformat_String}
		],
		sqf=Sequence[
				{filename_String},
				{samplerate_?NumericQ,voltagegain_?NumericQ,voltageoffset_?NumericQ},
				{fullspan_List,frequencylimits_List},
				data_
		]
	},
	(*sets defaults for directory*)
 	labViewDataConfig/:labViewDataConfig[sqc]["basedirectory"]:=basedirectory;
 	labViewDataConfig/:labViewDataConfig[sqc]["workingdirectory"]:=workingdirectory;
 	labViewDataConfig/:labViewDataConfig[sqc]["samplerate"]:=samplerate;
 	labViewDataConfig/:labViewDataConfig[sqc]["voltagegain"]:=voltagegain;
 	labViewDataConfig/:labViewDataConfig[sqc]["voltageoffset"]:=voltageoffset;
 	labViewDataConfig/:labViewDataConfig[sqc]["fullspan"]:=fullspan;
 	labViewDataConfig/:labViewDataConfig[sqc]["frequencylimits"]:=frequencylimits;
 	labViewDataConfig/:labViewDataConfig[sqc]["dataformat"]:=dataformat;
 	
 	labViewDataConfig/:labViewDataConfig[sqc]["filenames",pattern_String]:=
 		Last/@FileNameSplit/@FileNames[pattern,FileNameJoin[{basedirectory,workingdirectory}]];
 	labViewDataConfig/:labViewDataConfig[sqc]["filenames"]:=
 		Last/@FileNameSplit/@FileNames["*",FileNameJoin[{basedirectory,workingdirectory}]];
 	
 	(*per file config*)
 	labViewData/:labViewData[sqf]["filename"]:=filename;
 	labViewData/:labViewData[sqf]["samplerate"]:=samplerate;
 	labViewData/:labViewData[sqf]["voltagegain"]:=voltagegain;
 	labViewData/:labViewData[sqf]["voltageoffset"]:=voltageoffset;
 	labViewData/:labViewData[sqf]["fullspan"]:=fullspan;
 	labViewData/:labViewData[sqf]["frequencylimits"]:=frequencylimits;
 	labViewData/:labViewData[sqf]["data"]:=data;
 	labViewData/:labViewData[sqf]["data",part__]:=Partition[data,part];
 	labViewData/:labViewData[sqf]["info"]:=(
 		Print["Sample rate: ", samplerate];
 		Print["Sample points: ",Length[data]];
 		Print["Total time (s): ", N@Length[data]/samplerate];
 		Print["Minimum frequency bin (Hz): ",N@samplerate/Length[data]];
 		Print["Time bin: ",1/samplerate];
 	);
 	
  	(*creator*)
  	generateLabViewDataConfig[sqc]:=
  		With[
  			(*set inmutable fields*)
  			{
  				bs=labViewDataConfig[
  					{basedirectory,workingdirectory},
					{samplerate,voltagegain,voltageoffset},
					{fullspan,frequencylimits},
					{dataformat}
				]
			},
  			(*return*)
  			bs	
  		];
]

loadLabViewData[lvc_labViewDataConfig,filename_String]:=
	Module[
		{
			fullfn=FileNameJoin[{lvc["basedirectory"],lvc["workingdirectory"],filename}]	
		},
		labViewData[
			(*metadata*)
			{filename},
			{lvc["samplerate"],lvc["voltagegain"],lvc["voltageoffset"]},
			{lvc["fullspan"],lvc["frequencylimits"]},
			(*data*)
			lvc["voltagegain"]*BinaryReadList[fullfn,lvc["dataformat"]]+lvc["voltageoffset"]
		]
	]


labViewFourierTransform[lvd_labViewData]:=
	labViewFourierTransform[lvd["data"],lvd["samplerate"]];
labViewFourierTransform[DataIn_, SampleRateIn_,OptionsPattern[]] := 
  Module[
  	{ScaledLength, DataFourier,normalization},
  	normalization=Switch[OptionValue[Normalization],
  		Automatic,1,
  		"SampleRate",SampleRateIn
  	];
	ScaledLength=Length[DataIn]/SampleRateIn;
	DataFourier=Fourier[DataIn,FourierParameters->{-1, -1}];
	(*return*)
	Transpose[{
			(Range[Length[DataFourier]]-1)/ScaledLength,
			Abs[DataFourier]^2/normalization
	}]
];
Options[labViewFourierTransform]={Normalization->Automatic};


labViewPowerSpectrum[lvd_labViewData]:=
	labViewPowerSpectrum[lvd["data"],lvd["samplerate"]];
labViewPowerSpectrum[DataIn_, SampleRateIn_,OptionsPattern[]] := 
	Module[
	  	{
	  		ScaledLength, 
	  		DataFourier,
	  		SvvDoubleSided, SvvSingleSided,
	  		DCPart,
	  		ACPart,
	  		normalization
	    },
	    normalization=Switch[OptionValue[Normalization],
  			Automatic,1,
  			"SampleRate",SampleRateIn
  		];
	    ScaledLength = Length[DataIn]/SampleRateIn;
	    DataFourier = Fourier[DataIn, FourierParameters -> {-1, -1}];
	    SvvDoubleSided = (Abs[DataFourier]^2)/normalization;
	    DCPart = SvvDoubleSided[[1]];
	    ACPart = 
	     2*Take[SvvDoubleSided, {2, 
	        1+Ceiling[(Length[SvvDoubleSided]-1)/2]}];
	    SvvSingleSided = Flatten[{DCPart,ACPart}];
	    (*return single sided spectrum*) 
	    Transpose[{
	    	(Range[Length[SvvSingleSided]] - 1)/ScaledLength,
	    	SvvSingleSided
	    }]
	];
Options[labViewPowerSpectrum]={Normalization->Automatic}

End[] (* End Private Context *)

EndPackage[]