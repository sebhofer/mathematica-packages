(* ::Package:: *)

(* Mathematica Package *)

BeginPackage["QuantumModels`",{"SystemModel`"}]
(* Exported symbols added here with SymbolName::usage *)  

squeezingCavity::usage="squeezingCavity[\[Epsilon],\[Kappa],\[Phi]]"
beamSplitter::usage="beamSplitter[\[Theta],\[Xi],\[Eta]]"
homodyneDetection::usage="homodyneDetection[\[Phi]]"

Begin["`Private`"] (* Begin Private Context *) 


(* ::Subsection:: *)
(*Optical circuit models*)


(* ::Text:: *)
(*Circuit models are defined such that the output is not a valid measurement, but rather outputs as in the standard input-output relations. Thus they must be connected to measurement models to describe a valid measurement setup.*)


(* ::Subsubsection:: *)
(*active components*)


Clear@squeezingCavity;
squeezingCavity[\[Epsilon]_,\[Kappa]_,\[Phi]_]:=Module[{T,A,F,L,H,W,NM,M,G},
T={{1,1},{1,-1}/I}/Sqrt[2];
A={{0,Exp[I \[Phi]]},{Exp[-I \[Phi]],0}};
F=\[Epsilon]*FullSimplify[T.A.Inverse@T]-\[Kappa] IdentityMatrix[2];
L=-Sqrt[2\[Kappa]]IdentityMatrix@Length@F;
H=Sqrt[2\[Kappa]]{{1,0},{0,1}};
W=IdentityMatrix@Length@F/2;
NM=IdentityMatrix@Length@H/2;M=IdentityMatrix@2/2;
G=Transpose@{{1,0},{0,1}};
systemModel[{{F,G,L,H},{W,NM,M},{{},{},{}}}]
]


(* ::Subsubsection:: *)
(*passive components*)


Clear@beamSplitter
beamSplitter[\[Theta]_:Pi/4,\[Xi]_:0,\[Eta]_:0]:=Module[{F,H,G,L,D},
D={{Cos[\[Theta]]Exp[-I \[Xi]],0,Sin[\[Theta]]Exp[-I \[Eta]],0},{0,Cos[\[Theta]]Exp[I \[Xi]],0,Sin[\[Theta]]Exp[I \[Eta]]},{-Sin[\[Theta]]Exp[I \[Eta]],0,Cos[\[Theta]]Exp[I \[Xi]],0},{0,-Sin[\[Theta]]Exp[-I \[Eta]],0,Cos[\[Theta]]Exp[-I \[Xi]]}};
F={{0}};H={{0},{0},{0},{0}};G={{0,0,0,0}};L={{0}};
systemModel[{{F,G,L,H,D},{{},{},{}},{{},{},{}}}]]


Clear@thermalMode
thermalMode[n_]:=Module[{F,H,G,L,W,NM,id},
id=IdentityMatrix[2];
W=id*0;NM=(2n+1)id/2;F=id*0;H=id;G={{0},{0}};L=id*0;
systemModel[{{F,G,L,H},{W,NM,{}},{{},{},{}}}]];
thermalMode[n__]:=systemModelConcatenate@@thermalMode/@{n};


(* ::Subsubsection:: *)
(*measurement devices*)


Clear@homodyneDetection
homodyneDetection[\[Phi]_Symbol]:=Module[{F,H,G,L,D},
F={{0}};H={{0}};G={{0,0}};L={{0}};D=Normal@SparseArray[{{1,1},{1,2}}->{Cos[\[Phi]],Sin[\[Phi]]}];
systemModel[{{F,G,L,H,D},{{},{},{}},{{},{},{}}}]];
homodyneDetection[\[Phi]__Symbol]:=systemModelConcatenate@@homodyneDetection/@{\[Phi]};


End[] (* End Private Context *)

EndPackage[]
