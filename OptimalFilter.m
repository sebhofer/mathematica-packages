(* ::Package:: *)

(* ::Title:: *)
(*Kalman filtering - optimal control*)


(* :Title: OptimalFilter.m  *)

(* :Mathematica Version: Mathematica 8 *)

(* :Author: Sebastian Hofer *)

(* :Package Version: 0.9.1 *)

(* :Discussion:

*)



BeginPackage["OptimalFilter`",{"SystemModel`","GeneralToolbox`"}]

estimatorGain::usage = "estimatorGain[sm] calculates the gain of the Kalman filter for the system model sm.";
controllerGain::usage = "controllerGain[sm] calculates the gain of the optimal LQ regulator for the system model sm.";
estimatorVariance::usage = "estimatorVariance[sm] calculates the continuous steady-state estimation covariance matrix."
controllerVariance::usage = "controllerVariance[sm] calculates the continuous steady-state crontrol covariance matrix."
regulatorStability::usage = "regulatorStability[sm] checks the stability of the linear-quadratic regulator.";
estimatorStability::usage = "estimatorStability[sm] checks the stability of the Kalman estimator."
controllerStability::usage = "controllerStability[sm] checks the stability of the optimal controller."
estimatorInverseGain;
controllerInverseGain;

feedbackState::usage = "feedbackState[sm] returns the closed-loop unconditional covariance matrix corresponding to the systemModel sm.";
conditionalState::usage = "conditionalState[sm] returns the open-loop, conditional covariance matrix corresponding to the systemModel sm.";

controlSignalVariance::usage="controlSignalVariance[sm] returns expected mean-square of control signal, i.e., \[CapitalEpsilon][\!\(\*SuperscriptBox[\(u\), \(T\)]\)u].";

openLoopTransferFunction::usage = "kalmanTransferFunction[sm,var] calculates the transfer function of the steady-state Kalman filter."

kalmanUnconditionalState::usage = "Obsolete! Use feedbackState."
kalmanFilterTransferFunction::usage = "Obsolete! Use openLoopTransferFunction.";

Begin["`Private`"]


(* ::Section:: *)
(*Optimal Regulator/Kalman filter*)


(*
	Caching also improves the performance of estimatorGain and controllerGain,
	but of course increases memory requirements. Depending on the application
	one or the other is optimal.

	Note that cached values should be reset after changing the default options
	(as those are not tracked)!	
*)
estimatorVariance[sm_systemModel,opts:OptionsPattern[]]:=estimatorVariance[sm,opts]=
	estimatorVariance[sm,"NoCache",opts];
estimatorVariance[sm_systemModel,"NoCache",opts:OptionsPattern[]]:=
	RiccatiSolve[
		{Transpose@sm["F"],Transpose@sm["H"]},
		{sm["L"].sm["W"].Transpose@sm["L"],sm["N"],sm["L"].sm["M"]},opts];
Options[estimatorVariance]=Options[RiccatiSolve];
SyntaxInformation[estimatorVariance]={"ArgumentsPattern"->{_systemModel}}
conditionalState=estimatorVariance;
	
controllerVariance[sm_systemModel,opts:OptionsPattern[]]:=controllerVariance[sm,opts]=
	controllerVariance[sm,"NoCache",opts];
controllerVariance[sm_systemModel,"NoCache",opts:OptionsPattern[]]:=	
	RiccatiSolve[{sm["F"],sm["G"]},{sm["Q"],sm["R"]},opts];
	
estimatorGain[sm_systemModel,opts:OptionsPattern[]]:=estimatorGain[sm,opts]=
	estimatorGain[sm,"NoCache",opts];
estimatorGain[sm_systemModel,"NoCache",opts:OptionsPattern[]]:=	
	(estimatorVariance[sm,opts].Transpose@sm["H"]+sm["L"].sm["M"]).
	Inverse@sm["N"];

controllerGain[sm_systemModel,opts:OptionsPattern[]]:=controllerGain[sm,opts]=
	controllerGain[sm,"NoCache",opts];
controllerGain[sm_systemModel,"NoCache",opts:OptionsPattern[]]:=
	Inverse@sm["R"].Transpose@sm["G"].controllerVariance[sm,opts];
	
estimatorInverseGain[sm_systemModel,var_,opts:OptionsPattern[]]:=
	Module[
		{F,K,H,id},
		F = sm["F"];
		H = sm["H"];
		K = estimatorGain[sm,FilterRules[{opts},Options[estimatorGain]]];
		id = IdentityMatrix@Length@F;
		(*return*)
		If[OptionValue[Rationalize],
			-Inverse[I*var*id+SetPrecision[F-K.H,Infinity]],
   			-Inverse[I*var*id+(F-K.H)]
		]
   	];
Options[estimatorInverseGain]={Rationalize->False}~Join~Options[estimatorGain];
   		
controllerInverseGain[sm_systemModel,var_,opts:OptionsPattern[]]:=
	Module[
		{F,G,C,id},
		F = sm["F"];
		G = sm["G"];
		C = controllerGain[sm,FilterRules[{opts},Options[ControllerGain]]];
		id = IdentityMatrix@Length@F;
		(*return*)
		If[OptionValue[Rationalize],
			-Inverse[I*var*id+SetPrecision[F-G.C,Infinity]],
			-Inverse[I*var*id+(F-G.C)]
		]
   	];
Options[controllerInverseGain]={Rationalize->False}~Join~Options[controllerGain];


openLoopTransferFunction[sm_systemModel,var_]:=
	estimatorInverseGain[sm,var].estimatorGain[sm]
kalmanFilterTransferFunction[seq___]:=(Message[optf::obssym,"kalmanFilterTransferFunction"];openLoopTransferFunction[seq]);(*TODO:remove when safe!*);

(*second moment of estimate*)
estVar[sm_systemModel,opts:OptionsPattern[]]:=
	Module[{nm,eg},
		nm=sm["F"]-sm["G"].Inverse@sm["R"].Transpose@sm["G"].controllerVariance[sm,opts];
		eg=estimatorGain[sm,opts];
		(*return*)
		LyapunovSolve[nm,-eg.sm["N"].Transpose@eg]
	];

feedbackState[sm_systemModel,opts:OptionsPattern[]]:=estVar[sm,opts]+estimatorVariance[sm,opts];

controlSignalVariance[sm_systemModel,opts:OptionsPattern[]]:=
	With[{cg=controllerGain[sm,opts]},Tr[cg.estVar[sm,opts].Transpose[cg]]]

(*feedbackState[sm_systemModel,opts:OptionsPattern[]]:=
	Module[{nm,eg},
		nm=sm["F"]-sm["G"].Inverse@sm["R"].Transpose@sm["G"].controllerVariance[sm,opts];
		eg=estimatorGain[sm,opts];
		(*return*)
		LyapunovSolve[nm,-eg.sm["N"].Transpose@eg]+estimatorVariance[sm,opts]
	];*)
Options[feedbackState]=Options[estimatorVariance];
kalmanUnconditionalState[seq___]:=(Message[optf::obssym,"feedbackState"];feedbackState[seq]);(*TODO:remove when safe!*);


(*
	these give the same results as ObservabilityMatrix and ControllabilityMatrix
	if L is not appended to G in the StateSpaceModel!
*)
pairObservabilityMatrix[F_List,H_List]:=
	Transpose@ArrayFlatten@{Table[Transpose[H.MatrixPower[F,i]],{i,0,Length@F-1}]};
pairControllabilityMatrix[F_List,G_List]:=
	ArrayFlatten@{Table[MatrixPower[F,i].F,{i,0,Length@F-1}]};


(*
	Stability criteria for LQR problem (see Stengel p.473,p.476)
	Note: this has been modified to account for noise
	cross correlations by introducing T (see v.d.Heijden, p.303).
	This could also be modified to account for a coupled cost function
	(Stengel, p.473)
*)
controllerStability[sm_systemModel]:=
	Catch@Module[
		{
			F,G,Q,R,	(*systemModel variables*)
			D,QT
		},
		{F,G,Q,R}=sm[{"F","G","Q","R"}];
		QT=Q;
		
		(*controller*)
		(*check positivity*)
		If[Not@PositiveDefiniteMatrixQ@QT,
			Message[controllerStability::lqrstab,"Controller","Q not positive definite"];Throw[False]];
		If[Not@PositiveDefiniteMatrixQ@R,
			Message[controllerStability::lqrstab,"Controller","R not positive definite"];Throw[False]];
		(*decompose*)
		D=CholeskyDecomposition@QT;
		(*check detectability*)
		If[MatrixRank@pairObservabilityMatrix[F,D]!=Length@F,
			Message[controllerStability::lqrstab,"Controller","not detectable"];Throw[False]];
		(*check stabilizability*)
		If[MatrixRank@pairControllabilityMatrix[F,G]!=Length@F,
			Message[controllerStability::lqrstab,"Controller","not stabilizable"];Throw[False]];
			
		(*return*)
		True
	];
	
estimatorStability[sm_systemModel]:=
		Catch@Module[
		{
			F,H,L,M,NM,W,	(*systemModel variables*)
			T,D,WT
		},
		{F,H,L,M,NM,W}=sm[{"F","H","L","M","N","W"}];
		T=L.M.Inverse@NM;
		WT=L.(W-M.Inverse@NM.Transpose@M).Transpose@L;

		(*estimator*)
		(*check positivity*)
		If[Not@PositiveDefiniteMatrixQ@NM,
			Message[estimatorStability::lqrstab,"Estimator","N not positive definite"];Throw[False]];
		If[Not@positiveSemiDefiniteMatrixQ@WT,
			Message[estimatorStability::lqrstab,"Estimator","L.(W-M.1/N.M').L' not positive semi definite"];Throw[False]];
		(*decompose*)
		D=CholeskyDecomposition@WT;

		(*check detectability*)
		If[MatrixRank@pairObservabilityMatrix[F-T.H,H]!=Length@F,
			Message[estimatorStability::lqrstab,"Estimator","not detectable"];Throw[False]];
		(*check stabilizability*)
		If[MatrixRank@pairControllabilityMatrix[F-T.H,D]!=Length@F,
			Message[estimatorStability::lqrstab,"Estimator","not stabilizable"];Throw[False]];
			
		(*return*)
		True
	]
	
regulatorStability[sm_systemModel]:=
	And[controllerStability@sm,estimatorStability@sm];


(* ::Section:: *)
(*Messages*)


optf::obssym="This function is obsolete! Please change this to `1`!";
General::lqrstab="`1` is unstable: `2`";

End[]

EndPackage[]
