(* ::Package:: *)

(* :Title: QuantumCVAlgebra.m  *)

(* :Author: Sebastian Hofer *)

(* :Discussion:
Very basic (!) emulation of algebra of n-mode CV operators up to second order in the quadrature operators. 

Structure: X_i, P_i are the quadrature operators of the i-th mode (with [X_i, P_j]=I for i\[Equal]j)

- quOp:
  a1 X_m1 + b1 P_m1 + a2 X_m2 + b2 P_m2 + ... = 
  quOp[number of modes, {m1, {a1, b1}}, {m2, {a2, b2}},...]


Supported operations: x=quOp[1,{1,{1,0}}]; p=quOp[1,{1,{0,1}}];

- Addition: x+p
- Multiplication with scalar: 2*x
- Multiplication of operators by NonCommutativeMultiply (**): x^2 \[Equal] x**x

*)

(* Example: 1d damped harmonic oscillator

x=quOp[1,{1,{1,0}}];
p=quOp[1,{1,{0,1}}];
H-f(x^2+p^2)/2;

*)

BeginPackage["QuantumCVAlgebra`",{"GeneralToolbox`"}]
(* Exported symbols added here with SymbolName::usage *)  

quOp::usage = "quOp[dim, {mode1, {x1_factor, p1_factor}}, {mode2, {x2_factor, p2_factor}},...] defines a CV operator in the {x1,p1,x2,p2,...,x_dim,p_dim} basis of the form x1_factor*x1 + p1_factor*p1 + ....";
quSum::usage = "quSum[quOp1, quOp2, ...] defines a sum of quOp objects quOp1, quOp2,...";
quList::usage = "quList[{qu1, qu2, ...}] defines a (possibly nested) list of quOp or quSum objects qu1, qu2,...";
quDimensions::usage = "quDimensions[quObj] returns operator dimensions of quantum object (quOp, quSum, quList).";
quOrder::usage = "quOrder[quObj] return operator order of quantum object (quOp, quSum, quList).";
quCommutator::usage = "quCommutator[quOp1,quOp2] returns the commutator of two 0th or 1st order operators or quLists.";

Begin["`Private`"] (* Begin Private Context *)

Unprotect[quOp,quSum,quList];ClearAll[quOp,quSum,quList,quDimensions,quOrder];
With[{
		pat=Alternatives[{{_Integer,{_,_}}..},Except[_quOp|_quSum|_quList]],
		scalartest=MatchQ[#,Except[_quOp|_quSum|_quList]]&,
    specialtest=MatchQ[#,quOp[{0,0},_]]&
	},
	(*constructor*)
	quOp[0,a_?scalartest]:=quOp[{0,0},a]; (*special operators*)
	quOp[dim_Integer,def:{mode_Integer,_List}]:=quOp[{dim,1},{def}];
	quOp[dim_Integer,def:{_Integer,_List}..]:=Apply[NonCommutativeMultiply,quOp[dim,#]&/@{def}];


	(*special operators - keep order!*)
	zeroOp=quOp[{0,0},0];
	quOp/:quOp[_,pat]**quOp[_,0]:=zeroOp; (*multiplication with 0*)
	quOp/:quOp[_,0]**quOp[_,pat]:=zeroOp;
	quOp/:quOp[{dim_,ord_},def:pat]+quOp[_,0]:=quOp[{dim,ord},def]; (*addition of 0*)
	quOp/:quOp[{dim_,ord_},def:pat]**quOp[{0,0},a_?scalartest]:=a*quOp[{dim,ord},def]; (*multiplication with 1*)
	quOp/:quOp[{0,0},a_?scalartest]**quOp[{dim_,ord_},def:pat]:=a*quOp[{dim,ord},def];
  (*quOp/:quOp[_,{___,{_,{0,0}},___}]:=zeroOp; *)(*collapse operators with 0 entries only to zeroOp*)
	(*representation*)
	quOp/:quOp[{0,0},a_?scalartest][]:=a;

	
	(*(noncommutative) operator product*)
	quOp/:quOp[{dim_Integer,ord1_Integer},def1:pat]**quOp[{dim_Integer,ord2_Integer},def2:pat]:=
		quOp[{dim,ord1+ord2},Flatten[Sort@GatherBy[Join[def1,def2],#[[1]]&],1]];
	quOp/:quOp[def__]^2:=quOp[def]**quOp[def];

	
	(*basis representation*)
	eval[dim_]:=(ReplacePart[ConstantArray[{0,0},dim],#[[1]]->#[[2]]]&); (*convert definition to vector form*)
	opmatsym[mat_List]:=With[{partmat=Partition[mat,{2,2}]}, (*symmetrize matrix representation*)
		((#+Transpose@#)/2&@ArrayFlatten@ReplacePart[partmat,{i_,i_}->{{0,0},{0,0}}])+
		blockDiagonalMatrix@Diagonal@partmat];
	btrans[None,dim_Integer]:=IdentityMatrix[2*dim];
	btrans[trans_List,dim_Integer]/;(Equal@@Dimensions@trans&&Length@trans==2*dim):=trans;
	btrans[__]:=(Message[quOp::btrans];Throw[$Failed,btrans]);
	quOp::btrans = "BasisTrafo must be either None or square matrix with appropriate dimensions.";
	(*vector representation*)
	quOp/:quOp[{dim_Integer,1},def:pat][i_Integer|i_Span|i_List|All,opts:OptionsPattern[]]:=
		(btrans[OptionValue[{opts,Sequence@@Options@quOp},BasisTrafo],dim].Flatten[eval[dim]/@def])[[i]];
	quOp/:quOp[{dim_Integer,1},def:pat][opts:OptionsPattern[]]:=quOp[{dim,1},def][All,opts];
	(*matrix representation*)
	(*Note: basis trafo has to be done after symmetrisation!!*)
	quOp/:quOp[{dim_Integer,2},def:pat][opts:OptionsPattern[]]:=With[
		{tm=btrans[OptionValue[{opts,Sequence@@Options@quOp},BasisTrafo],dim]},
		tm.(opmatsym@Outer[Times,quOp[dim,def[[1]]][],quOp[dim,def[[2]]][]]).Transpose@tm];
	quOp/:quOp[{dim_Integer,2},def:pat][i_Integer|i_Span|i_List,j_Integer|j_Span|j_List,opts:OptionsPattern[]]:=
		quOp[{dim,2},def][opts][[i,j]];
	(*tensor representation is not available for dim>=3*)
	quOp/:quOp[{dim_Integer,ord_Integer},def:pat][___]/;(ord>=3):=
		(Message[quOp::trepna];quOp[{dim,ord},def]);
	quOp::trepna="Tensor representation is not available for operators of order 3 and above.";
	

	(*operator sum*)
	quOp/:quOp[{dim1:0,ord1:0},def1:pat]+quOp[{dim2_Integer,ord2_Integer},def2_]:= (*for special operators, dims need not match*)
		quSum[quOp[{dim1,ord1},def1],quOp[{dim2,ord2},def2]];
	quOp/:quOp[{dim_,1},{{mode_,def1_}}]+quOp[{dim_,1},{{mode_,def2_}}]:=quOp[dim,{mode,def1+def2}];(*add dim 1 operators directly*)
	quOp/:quOp[{dim_Integer,ord1_Integer},def1:pat]+quOp[{dim_Integer,ord2_Integer},def2:pat]:= (*for operators of equal dimension*)
		quSum[quOp[{dim,ord1},def1],quOp[{dim,ord2},def2]];
	quOp/:quOp[{dim1_Integer,ord1_Integer},def1:pat]+quOp[{dim2_Integer,ord2_Integer},def2:pat]/;dim1!=dim2:= (*addition for non-equal dimensions is not evaluated*)
		(Message[quSum::wdim];Throw[$Failed,quSum]);
	quSum::wdim = "Only operators with same dimension can be added.";
	
	
	(*Ordering is important!*)
	quSum/:quSum[def1__]+quSum[def2__]:=quSum[def1,def2];
	quSum/:quOp[def1__]+quSum[def2__]:=quSum[quOp[def1],def2];
	quSum/:quSum[op_quOp]:=op; (*single operator sum reduces to operator*)
	quSum/:quSum[a___,quOp[{0,0},0],b___]:=quSum[a,b]; (*zero op must fall out, but only if there are other terms!*)
	(*quSum/:quSum[a___,quOp[{dim_,1},{mode_,def1_}],quOp[{dim_,1},{mode_,def2_}],b___]:=quSum[a,quOp[{dim,1},{mode,def1+def2}],b]*) (*doesnt work*)
	Attributes@quSum = {Orderless,Flat,Listable};
	

	(*quSum product*)
	quSum/:quSum[sum:quOp[__]..]**quOp[op__]:=quSum@@Distribute[List[sum]**quOp[op],List];
	quSum/:quOp[op__]**quSum[sum:quOp[__]..]:=quSum@@Distribute[quOp[op]**List[sum],List];
	quSum/:quSum[sum1:quOp[__]..]**quSum[sum2:quOp[__]..]:=quSum@@Distribute[List[sum1]**List[sum2],List];
	quSum/:quSum[def__]^2:=quSum[def]**quSum[def];


	(*representation of operator sum*)
	quSum/:quSum[sum:quOp[__]..][seq___]:=Plus@@Through[{sum}[seq]];


	(*operator list*)
	quList/:quList[arg_quSum|arg_quOp]:=arg;
(*	quList/:quList[qul1__].quList[qul2__]:=quList@Inner[NonCommutativeMultiply,qul1,qul2,quSum];
	quList/:list1_List.quList[qul2__]:=quList@Inner[Times,list1,qul2,quSum];
	quList/:quList[qul1__].list2_List:=quList@Inner[Times,qul1,list2,quSum];
	quList/:quList[qul1_]+quList[qul2_]:=quList[qul1+qul2];*)
	quList/:quList[qul1_List].quList[qul2_List]:=quList@Inner[NonCommutativeMultiply,qul1,qul2,quSum];
	quList/:list1_List.quList[qul2_List]:=quList@Inner[Times,list1,qul2,quSum];
	quList/:quList[qul1_List].list2_List:=quList@Inner[Times,qul1,list2,quSum];
	quList/:quList[qul1_List]+quList[qul2_List]:=quList[qul1+qul2];
	(*representation*)
	quList /: quList[list_List][] := Map[
    Replace[#[], 0 :> ConstantArray[0, ConstantArray[2*Max[quDimensions@list], Max[quOrder@list]]]] &,
    list, {ArrayDepth@list}];

   (*scalar multiplication*)
	quOp/:a_*quOp[{dim_Integer,0},def:pat]/;scalartest@a:=
		quOp[{dim,0},a*def];
	quOp/:a_*quOp[{dim_Integer,1},def:pat]/;scalartest@a:=
		quOp[{dim,1},({#[[1]],a*#[[2]]}&/@def)];
	quOp/:a_*quOp[{dim_Integer,ord_Integer},def:pat..]/;(scalartest@a&&ord>1):=
		quOp[{dim,ord},{{def[[1,1]],a*def[[1,2]]},Sequence@@def[[2;;]]}];
	quSum/:a_*quSum[sum:quOp[__]..]/;scalartest@a:=quSum@@Sequence[a*{sum}];
	quList/:a_*quList[list_]/;scalartest@a:=quList[a*list];


	(*dimensions*)
	Attributes@quDimensions = {Listable};
	quDimensions@quOp[{dim_,_},_]:={dim}; (*returns operator dimensions*)
	quDimensions@quSum[seq__]:=Select[Flatten@Union[quDimensions@{seq}],Positive]; (*returns operator dimensions (except for 0)*)
	quDimensions@quList[list__]:=quDimensions@list; (*returns list of operator dimensions*)
	quList/:Dimensions@quList[list_]:=Dimensions@list; (*returns array dimensions*)
    quList/:Length@quList[list_]:=Length@list; (*returns array length*)
	(*order*)
	Attributes@quOrder = {Listable};
	quOrder@quOp[{_,ord_},__]:=ord;
	quOrder@quSum[def__]:=Union@quOrder@{def}; (*returns list of operator orders*)
	quOrder@quList[list_]:=quOrder@list; (*returns (nested) list of operator orders*)
	
	(*hermitian conjugate*)
	quOp/:Conjugate[quOp[{0,0},a_?scalartest]]:=quOp[0,Conjugate@a];
	quOp/:Conjugate[quOp[{dim_Integer,ord_Integer},def:pat]]:=quOp[{dim,ord},Reverse[Conjugate/@def]];
	quSum/:Conjugate[quSum[sum__]]:=quSum[Sequence@@Conjugate@{sum}];
	quList/:Conjugate[quList[list_]]:=quList[Conjugate@list];
	quList/:ConjugateTranspose[quList[list_]]:=quList[ConjugateTranspose@list];
	quList/:Transpose[quList[list_]]:=quList[Transpose@list];

  (*commutator for 0th and 1st order operators and quLists*)
  sympmat[n_Integer] := blockDiagonalMatrix@ConstantArray[{{0, 1}, {-1, 0}}, n];
  nestlist[a_List, depth_Integer: 2] := Nest[List, a, depth - ArrayDepth@a];
  quCommutator[a : _quOp | _quSum, b : _quOp | _quSum] :=
      Block[{dim = quDimensions@a, qupat = 0 | {0} | 1 | {1}},
        quOpFromList@Transpose[
              I*nestlist@a[].sympmat@First@dim.Transpose@nestlist@b[]][[1,
                1]] /; dim == quDimensions@b && MatchQ[quOrder@a, qupat] &&
            MatchQ[quOrder@b, qupat]];
  quCommutator[a_quOp, b_quList] := quList@Thread[quCommutator[a, First@b]];
  quCommutator[a_quList, b_quOp] := quList@Thread[quCommutator[First@a, b]];
  quCommutator[a_quList, b_quList] := quList@Outer[quCommutator, First@a, First@b];

	(**developer**)
  quIdentityMatrix[dim_Integer] :=
      quList@ReleaseHold@Map[Hold[quOp[0, #]] &, IdentityMatrix[dim], {2}];

  strictQuOrder@q_quOp:=quOrder@q;
  strictQuOrder@q_quSum:=Module[{ord=Union@quOrder@q},First@ord/;Length@ord==1];
  strictQuOrder@q_quList:=Module[{ord=Union@Flatten@quOrder@q},First@ord/;Length@ord==1];
  strictQuOrder[q:_quOp|_quSum|_quList]:=(Message[strictQuOrder::morder];Throw[$Failed,strictQuOrder]);
  strictQuOrder::morder="Mixed order operators not allowed.";

  changeDim[q_quOp?specialtest,___] := q;
  changeDim[q_quOp, n_Integer: 1] := MapAt[# + {n, 0} &, q, 1];
  changeDim[q_quList, n_Integer: 1] := quList@changeDim[q[[1]], n];
  changeDim[q_quSum, n_Integer: 1] := Plus@@changeDim[List @@ q, n];
  Attributes[changeDim] = {Listable};
  (*shift position of modes, n>0 = right-shift, n<0 = left-shift*)
  (*note that dimension of operator is not adapted correspondingly!*)
  finc[n_Integer][l : {{_Integer, _List} ..}] := # + {n, 0} & /@ l;
  shiftModes[q_quOp?specialtest,___] := q;
  shiftModes[q_quOp, n_Integer: 1] := MapAt[finc[n], q, 2];
  shiftModes[q_quList, n_Integer: 1] := quList@shiftModes[q[[1]], n];
  shiftModes[q_quSum, n_Integer: 1] := Plus@@shiftModes[List @@ q, n];
  Attributes[shiftModes] = {Listable};

  qufromid[{dim_}][scl_,{i_Integer}]:=scl*quOp[dim, {Ceiling[i/2], {Mod[i, 2], Mod[i + 1, 2]}}];
  qufromid[{dim_}][scl_,{i_Integer, j_Integer}]:=
      scl*quOp[dim,{Ceiling[i/2],{Mod[i,2],Mod[i+1,2]}}]**quOp[dim,{Ceiling[j/2],{Mod[j,2],Mod[j+1,2]}}];
  quOpDeconstruct[qu_quOp/;quOrder@qu==2]:=
      Plus@@Flatten@MapIndexed[qufromid@quDimensions@qu,qu[],{2}]; (*basis decomposition into (X,P) basis (as quSum)*)
  Clear@quOpFromList;
  quOpFromList[a_List]:=Block[ (*directly create quOp's from lists*)
    {depth=ArrayDepth@a,len=Length@a},
    Plus@@Flatten@MapIndexed[qufromid[{len/2}],a,{depth}]/;Mod[len,2]==0&&depth<= 2 (*order 1 and 2*)
  ];
  quOpFromList[scl_?scalartest] := quOp[0, scl]; (*special operators*)

	(*custom format*)
	Attributes@dispf = {HoldFirst};
	dispf[mat_,sup_]:=DisplayForm@SuperscriptBox[mat,sup];
	Format[qop:quOp[{0,0},a_?scalartest]]:=dispf[MatrixForm@{a},"\[ScriptQ]"]; (*order 0 operators*)
    Format[qop:quOp[{_,1},{{_Integer,{_,_}}}]]:=dispf[MatrixForm@{qop[]},"\[ScriptQ]"]; (*order 1 operators*)
	Format[qop:quOp[{_,2},{{_Integer,{_,_}}..}]]:=dispf[MatrixForm@qop[],"\[ScriptQ]"]; (*order 2 operators*)
    Format[qul:quList[l_List]]:=dispf[MatrixForm@l,"\[ScriptQ]\[ScriptL]"]; (*quList*)
	Format[qs_quSum?(Length@Union@quOrder@#==1&)]:=Switch[Union@quOrder@qs,
			{1},dispf[MatrixForm@{qs[]},"\[ScriptQ]"],
			{2},dispf[MatrixForm@qs[],"\[ScriptQ]"],
			_,qs (*FIXTHIS: this gives a recursion error when getting Information!!*)
		];
	(*Format[qs:quSum[def__]?(Length@Union@quOrder@#!=1&)]:=Defer[Plus[def]];*)
	
	(*define datatypes and enforce stict typing*)
	(*Note: this has to be done after all other definitions!*)
(*	quOp[args___] /; Not[
			Or[MatchQ[HoldComplete[args], HoldComplete[{_Integer,_Integer},pat]],Message[quOp::strw];False] (*structure must match*)
		]:=Throw[$Failed, quOp]; 
	quOp[{dim_Integer,ord_Integer},def:pat] /; Not[
			Or[dim==0, Max[def[[All,1]]] <= dim, Message[quOp::wdim];False] && (*dimensions must match, except for special operators*)
			Or[MatchQ[def,Except[_List]], Length[def] == ord, Message[quOp::word];False] (*order must match*)
		]:=Throw[$Failed, quOp];
	quSum[args__]/;Not[
			Or[MatchQ[HoldComplete[args],HoldComplete[__quOp]],Message[quSum::strw];False] (*structure must match*)
		]:= Throw[$Failed, quList];
	quSum[args__] /; Not[
			(*dimensions must match, only for non-special operators*)
			Or[Equal@@Select[quDimensions/@{args},Positive[#[[1]]]&],Message[quSum::nmdim];False]
		]:= Throw[$Failed, quSum];
	quList[args__]/;Not[
			Or[MatchQ[HoldComplete[args],HoldComplete[_List]] && MatchQ[Flatten@args, {(_quSum|_quOp)..}],Message[quList::strw];False] (*structure must match*)
		]:= Throw[$Failed, quList];
	General::strw = "Incorrect argument structure.";
	quOp::wdim = "Mode number must be smaller than or equal to dimension.";
	quOp::word = "Order number must match length of definition.";
	quSum::nmdim = "Operator dimensions in quSum must all be equal.";*)

	Options[quOp] = {BasisTrafo->None};
];

(*Protect[quOp,quSum,quList];*)

End[] (* End Private Context *)

EndPackage[]
