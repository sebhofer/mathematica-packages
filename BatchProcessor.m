(* ::Package:: *)

(* ::Title:: *)
(*Batch*)


(* :Title: Batch.m  *)

(* :Mathematica Version: Mathematica 8 *)

(* :Author: Sebastian Hofer *)

(* :Package Version: 0.94 *)

(*  :Discussion:

	TODOS:
	- memory is not released until computation is finished even in a Do loop!


	USE CASES:
	(*
		Use case 1, serial computation:
		A function f is iterated using Table. After every evaluation of f
		the result and the computation time is logged. If the intermediate
		results are not needed, replace logAndSaveIntermediate by logAbsoluteTiming.
		This is useful for small to medium sized computations, as the memory
		requirements for table can be large.
	*)
	
	(*load Batch*)
	Needs["BatchProcessor`"];
	
	(*customize log*)
	bs=generateBatchSetup[];
	printLogHeader[bs_batchSetup]:=With[
		{print=bs["logfunction"]},
		printLogHeaderGeneric[bs]; (*prints generic information: time/date, directory structure,...*)
		print["Hello World!"]
	];
	setupLog[bs]; (*if the log output should go into a file*)
	
	(*save details about run*)
	(*this should be BEFORE the run, in case it is aborted*)
	saveInputData[
		bs,
		{{"parameters",parameters},
		{"description","BatchProcessor example"},
		{"options",List@options}}
	];
	
	(*start run*)
	saveFinalData[bs,
		Table[logAndSaveIntermediate[bs,f[x,parameters,options]],{x,0,100}]
	];
		
	
	(*
		Use case 2, parallel computation:
		Similar to 1, but for parallel evaluation. Note that for writing
		the log, the parallel processes have to lock access to the logcounter
		variable. For computations where the actual computation of the result
		is fast, this produces a considerable overhead! Serial computation might
		be faster!
	*)
	
	(*Batch has to be loaded in master and parallel kernels*)
	Needs["BatchProcessor`"];
	(*if package is on non-standard path*)
	ParallelEvaluate[PrependTo[$Path,ToFileName[{"path","to","package"}]];]
	ParallelNeeds["BatchProcessor`"];
	
	(*customize log*)
	bs=generateBatchSetup[];
	printLogHeader[bs_batchSetup]:=With[
		{print=bs["logfunction"]},
		printLogHeaderGeneric[bs];
		print["Hello World!"]
	];
	(*customize log message to include kernel id.
	Note that $KernelID has to be passed to concurrentLogAndSave!*)
	logAbsoluteTiming::logmsg="n=`1` (KID=`3`), `2`s";
	setupLog[bs]; (*if the log output should go into a file*)
	
	(*save details about run*)
	(*this should be BEFORE the run, in case it is aborted*)
	saveInputData[
		bs,
		{{"parameters",parameters},
		{"description","BatchProcessor example"},
		{"options",List@options}}
	];
	
	(*distribute definitions to parallel kernels*)
	DistributeDefinitions[f];
	DistributeDefinitions["BatchProcessor`"]; (*this is done in setupLog*)
	
	(*start run*)
	saveFinalData[bs,
		ParallelTable[concurrentLogAndSaveIntermediate[bs,f[x],$KernelID],{x,0,100}]
	];
	
	
	(*
		Use case 3, interactive computations:
		newrun[] generates a new batchSetup every time its called, so all
		subsequent calls of the computation go to a new set of files. Useful for
		usage in notebook files when results of computation should be available
		at later times.
	*)
	
	(*interactively create a new setup for each run*)
	(*using different tags can be useful if there are several 
	configurations in a single notebook*)
	Needs["BatchProcessor`"];

	newrun["tag"]:=Module[{bs},
		bs=generateBatchSetup[];
		printLogHeader[bs_batchSetup]:=With[
			{print=bs["logfunction"]},
			printLogHeaderGeneric[bs];
			print["Hello World!"]
		];
   		setupLog[bs]; 
   		bs
   	];

   	(*start new run*)
	With[{bs=newrun["tag"], parameters={a->1,b->2}},
		saveInputData[bs,
   			{{"parameters", parameters}, 
   			{"description", "BatchProcessor example"}}
   		];
   		saveFinalData[bs,
   			Table[logAndSaveIntermediate[bs,f[x,parameters]],{x,0,100}]
     	];
   	];// AbsoluteTiming
   	
   	
   	(*
		Use case 4, serial computation for large datasets:
		Instead of using a Table, a Do loop is used to reduce memory
		requirements. We therefore record the data at every step
		to the final output file using logAndSave without using 
		saveFinalData in the end. Note that if saveFinalData is used
		afterwards, the data will be appended to the end of the file. 
	*)
	
	(*load Batch*)
	Needs["BatchProcessor`"];
	
	(*customize log*)
	bs=generateBatchSetup[];
	printLogHeader[bs_batchSetup]:=With[
		{print=bs["logfunction"]},
		printLogHeaderGeneric[bs]; (*prints generic information: time/date, directory structure,...*)
		print["Hello World!"]
	];
	setupLog[bs]; (*if the log output should go into a file*)
	
	(*save details about run*)
	saveInputData[
		bs,
		{{"parameters",parameters},
		{"description","BatchProcessor example"},
		{"options",List@options}}
	];
	
	(*start run*)
	Do[
		logAndSave[bs,f[x,parameters,options]],
		{x,0,100}
	]
*)




(* ::Section:: *)
(*Package setup begin*)


BeginPackage["BatchProcessor`"]


batchSetup::usage = "Data object used to store the configuration for a batch process."

generateObjectId::usage = "generateObjectId[prefix] takes a prefix and generates a object id based on the current time.";

generateBatchSetup::usage = "generateBatchSetup[objectid] generates a setupstructure based on given id.
generateBatchSetup[] uses default objectid (taken from Options[generateBatchSetup]).";

printToFile::usage = "printToFile[expr] prints expression to file instead of stdout.";

saveField::usage = "saveField[filename,objectid,field,expr] saves definition objectid[field]=expr to filename.";

saveData::usage = "saveData[setupstructure,dat] saves dat to datafile associated with setupstructure, using \"data\" as a fieldname .
saveData[objectid,dat] saves dat to file associated with objectid.";

fetchField::usage = "fetchField[setupstructure,field,rfield] loads field from object associated with setupstructure. If a running field is given the symbold resulting from field is mapped to running field. The running field can be given as a list or as {All}.
fetchField[objectid,field] loads field from object associated with objectid.";

fetchData::usage = "fetchData[setupstructure,rfield] returns data from datafile associated with setupstructure. If a running field is given the symbold resulting from field is mapped to running field. The running field can be given as a list or as {All}."

saveInputData::usage = "saveInputData[setupstructure,fieldname,dat] writes data to input file associated with setupstructure."

setupLog::usage = "setupLog[setupstructure] sets up logging to logfile associated with setupstructure."

printLogHeaderGeneric::usage = "printLogHeaderGeneric[setupstructure] prints predefined header."

printLogHeader::usage="printLogHeader[setupstructure] is called by setupLog[] and should be customized for each package. By default this calls printLogHeaderGeneric[].";

cleanLogSetup::usage = "cleanLogSetup[setupstructure] reverts setupLog[setupstructure] and cleans up spurious objects."

appendToLog::usage = "appendToLog[setupstructure,expr] appends expr to log file associated with setupstructure."

resetLogCounter::usage = "resetLogCounter[setupstructure] resets logcounter in setupstructure to 0."

preIncrementLogCounter::usage = "preIncrementLogCounter[setupstructure] increments logcounter in setupstructure and returns new value."

logAbsoluteTiming::usage = "logAbsoluteTiming[bs,expr] evaluates and times expr and logs timing to the logfile associated with bs. The printed message is defined by logAbsoluteTiming::logmsg, where `1` is the current value of bs[\"logfilecounter\"] and `2` is the timing value. Note that if this is used as a pure function the timing will not be accurate. 
logAbsoluteTiming[bs,expr,pr] passes sequence pr to print command. logAbsoluteTiming::logmsg has to be set accordingly, where `3` is replaced by the first expr in pr."

saveIntermediateData::usage = "saveIntermediateData[setupstructure,dat] saves dat to tmpfile associated with setupstructure."

logAndSaveIntermediate::usage = "logAndSaveIntermediate[setupstructure,dat] uses logAbsoluteTiming to log timing and saves intermediate data to tmp file. Note that if this is used as a pure function the timing will not be accurate.
logAndSaveIntermediate[setupstructure,dat,pr] appends pr to log."

logAndSave::usage = "concurrentLogAndSave[setupstructure,dat] uses logAbsoluteTiming to log timing and saves data to output file. Note that if this is used as a pure function the timing will not be accurate.
logAndSave[setupstructure,dat,pr] appends pr to log."

concurrentLogAndSaveIntermediate::usage = "concurrentLogAndSaveIntermediate[setupstructure,dat] same as logAndSaveIntermediate but for parallel evaluation."
concurrentLogAndSave::usage = "concurrentLogAndSave[setupstructure,dat] same as logAndSave but for parallel evaluation."

listDirectory::usage = "listDirectory[setupstructure] list files in directory associated with setupstructure."

saveFinalData::usage = "saveAndClean[bs,expr] evaluates and times expression, saves data, writes overall time to logfile and resets logcounter.
Note that if this is used as a pure function the timing will not be accurate."

fetchIntermediateData::usage = "fetchIntermediateData[setupstructure,rfield,fieldname] returns intermediate data with indeces from rfield and fieldname."

fetchInputData::usage = "fetchInputData[setupstructure] returns a list {{fieldname,value},...}.
fetchInputData[setupstructure,{field1,field2,...}] only returns given fields.
fetchInputData[\"objectname\",{field1,field2,...}] works the same as above."

listInputDataFields::usage = "listInputDataFields[setupstructure] returns a list of fieldnames."

listObjectNames::usage = "listObjectNames[directory] list all objectids associated with log files in directory.
listObjectNames[setupstructure] list all objectids associated with log files in directory defined by setupstructure."

(*dummy symbol used by setupLog*)
printToLogFile

formatInputData::usage = "formatInputData[inputdata] prints input data. Typical usage: formatInputData@fetchInputData[setupstructure]."


Begin["`Private`"]


(* ::Section:: *)
(*batchSetup object*)


(*only keeps characters in [A,Z] and [a,z]*)
filterNonAlpha[___]:=Sequence[];
filterNonAlpha[string_String]:=
	FromCharacterCode@
		Select[ToCharacterCode@string, (65 <= # <= 90 || 97 <= # <= 122 &)]


generateObjectName[prefix_String:""]:=
	filterNonAlpha@prefix<>ToString@AbsoluteTime@DateString[];


generateObjectId[prefix___String]:=ToString@Unique@filterNonAlpha@prefix;


(*define batchSetup datastructure*)
(*
	Note: Due to the implementation of the mutable fields the value of logcounter
	can't be shared with multiple kernels, as updated downvalues are not propagated
	to the other kernels.
*)
With[
	{
		(*Note thate these definitions have to be updated if the batchSetup structure is changed!*)
		sq=Sequence[
				objectid_String,(*identifier*)
				{objectname_String,directory_String,outputsuffix_String,inputsuffix_String,logsuffix_String,tmpinfix_String}(*filestructure setup*)
			]
	},
	Unprotect[batchSetup];
	
	(*user setup*)
 	batchSetup/:batchSetup[sq]["objectid"]:=objectid;
 	batchSetup/:batchSetup[sq]["objectname"]:=objectname;
 	batchSetup/:batchSetup[sq]["directory"]:=directory;
 	batchSetup/:batchSetup[sq]["outputsuffix"]:=outputsuffix;
 	batchSetup/:batchSetup[sq]["inputsuffix"]:=inputsuffix;
 	batchSetup/:batchSetup[sq]["logsuffix"]:=logsuffix;
 	batchSetup/:batchSetup[sq]["tmpinfix"]:=tmpinfix;
 	 	
 	(*derived*)
	batchSetup/:batchSetup[sq]["logfilename"]:=
		FileNameJoin[{directory, objectname<>"."<>logsuffix}];
	batchSetup/:batchSetup[sq]["outputfilename"]:= 
		FileNameJoin[{directory, objectname<>"."<>outputsuffix}];
	batchSetup/:batchSetup[sq]["inputfilename"]:=
		FileNameJoin[{directory, objectname<>"."<>inputsuffix}];
	batchSetup/:batchSetup[sq]["tmpfilename"]:=
		FileNameJoin[{directory, objectname<>tmpinfix<>"."<>outputsuffix}];
  	
  	Module[
  		(*define mutable fields*)
  		{logcounter,logfunction},

		(*initialize*)
  		logcounter[_]=0;
  		logfunction[_]=Print;
  		
  		(*get fields*)
  		batchSetup/:batchSetup[sq]["logcounter"]:=logcounter[objectid];
  		batchSetup/:batchSetup[sq]["logfunction"]:=logfunction[objectid];
  		
  		(*set fields - this has to be done for every field*)
  		batchSetup/:setField[batchSetup[sq],"logcounter",fieldvalue_]:=
  			logcounter[objectid]=fieldvalue;
  		batchSetup/:setField[batchSetup[sq],"logfunction",fieldvalue_]:=
  			logfunction[objectid]=fieldvalue;
  			
  		shareFields[]:=SetSharedFunction[logcounter,logfunction];
  	];
  		
	(*constructor*)
	batchSetup[Evaluate@Last@{sq}]:=
  		batchSetup[generateObjectId[],{objectname,directory,outputsuffix,inputsuffix,logsuffix,tmpinfix}];
	
	Protect[batchSetup];
]


generateBatchSetup[objectname_String,options:OptionsPattern[]]:=
	Module[
		{
			directory=OptionValue[Directory],
			outputsuffix=OptionValue[OutputSuffix],
			inputsuffix=OptionValue[Inputsuffix],
			logsuffix=OptionValue[LogSuffix],
			tmpinfix=OptionValue[TmpInfix],
			bs
		},
		bs=batchSetup[{objectname,directory,outputsuffix,inputsuffix,logsuffix,tmpinfix}];
 		If[
			$OperatingSystem==="Unix"&&Run["test -w "<>bs["directory"]]=!=0,
			Message[generateBatchSetup::nwrt,bs["directory"]]
		];
		bs
	];
generateBatchSetup[options:OptionsPattern[]]:=
	Module[
		{prefix=OptionValue[Prefix]},
		generateBatchSetup[generateObjectName[prefix],options]
	];
(*set defaults*)
Options[generateBatchSetup]=
	{
		Directory->Directory[],
		Prefix->"batch",
		OutputSuffix->"out.dat",
		Inputsuffix->"in.dat",
		LogSuffix->"log",
		TmpInfix->".tmp"
	};
generateBatchSetup::nwrt="Specified directory `1` does not exist or is not writable.";


(* ::Section:: *)
(*Write data*)


(*should this replace the existing field value?*)
saveField[filename_String,objectname_String,{field__},dat_,savef:Save|DumpSave:Save]:=
	(*this construct is needed, as no code is evaluated within the 
	first element of Block*)
	With[{sym=Evaluate@Symbol@objectname},
		Block[
			{sym},
			Switch[Length@Hold@field,
				1,sym[field]=dat,
				2,(sym[#1][#2]=dat)&@field,
				3,(sym[#1][#2][#3]=dat)&@field];
			savef[filename,sym];
			(*TODO: test this!!*)			
			Clear[sym];
		];
		dat
	];
saveField[filename_String,objectname_String,field_String,dat_,savef:Save|DumpSave:Save]:=
	saveField[filename,objectname,{field},dat,savef];


(*
	should this reset logcounter?
	changes:
	- method options are now Append, Write instead of "Append", "Replace"
*)
saveData[bs_batchSetup,dat_,OptionsPattern[]]:=
	Module[
		{
			method=OptionValue[Method],
			fieldname=OptionValue[Field],
			filename=bs["outputfilename"],
			savewith=OptionValue[SaveWith]
		},
		If[FileExistsQ[filename]&&method===Write,DeleteFile[filename]];
		saveField[filename,bs["objectname"],{"output",fieldname},dat,savewith]
	];
Options[saveData]={Method->Append,Field->"data",SaveWith->Save};


saveIntermediateData[bs_batchSetup,dat_,OptionsPattern[]]:=
	Module[
		{fieldname=OptionValue[Field]}, 
		saveField[bs["tmpfilename"],bs["objectname"],
			{"output",fieldname,bs["logcounter"]},dat]
	]
Options[saveIntermediateData]={Field->"tmpdata"};


(*TODO: This call signature needs to be fixed!!*)
saveInputData[bs_batchSetup,fieldname_String,dat_]:=
	saveField[bs["inputfilename"],bs["objectname"],{"input",fieldname},dat];
saveInputData[bs_batchSetup,datlist_List]/;MatchQ[datlist,{{_String,_}..}]:=
	saveInputData[bs,First@#,Last@#]&/@datlist;
saveInputData[bs_batchSetup,dats__]:=
	saveInputData[bs,{dats}];


(*note: returning expr instead of result would lead to expr being exectued twice!*)
saveFinalData[bs_batchSetup,expr_,opts:OptionsPattern[saveData]]:=
	Module[{time,result},
			{time,result}=AbsoluteTiming@expr;
			(*write log message and clean up*)
			bs["logfunction"][StringForm[saveFinalData::logmsg,N@time]];
			resetLogCounter[bs];
			(*save and return*)
			saveData[bs,result,opts]
		];
SetAttributes[saveFinalData,HoldRest];
saveFinalData::logmsg="Calculation finished in `1`s";


(* ::Section:: *)
(*Read data*)


(*
	returns list of keys for subvalues of symbol associated to 
	objectname on levels _deeper_ than given fields!
*)
fieldIndexList[filename_String,objectname_String,{field__}]:=
	With[{sym=Evaluate@Symbol@objectname},
		Block[{sym},
			Get[filename];
			With[
				{pat=Evaluate@
					Switch[Length@Hold@field,
						1,sym[field],
						2,(sym[#1][#2])&@field]},				
				Cases[SubValues[sym],HoldPattern[pat[x_]]:>x,{3}]
			]
		]
	];
		

fetchField[filename_String,objectname_String,{field__},rfield_List:{}]:=
	(*this construct is needed, as no code is evaluated within the 
	first element of Block*)
	With[{sym=Evaluate@Symbol@objectname},
		Block[{sym},
			Get[filename];
			With[
				{pat=Switch[Length@Hold@field,
					1,sym[field],
					2,(sym[#1][#2])&@field,
					3,(sym[#1][#2][#3])&@field]},
				(*if there is a non-empty running field list map symbol to it*)
				Switch[rfield,
					{},pat,
					{All},pat/@Cases[SubValues[sym],HoldPattern[pat[x_]]:>x,{3}],
					_,pat/@rfield
				]
			]
		]
	]
fetchField[filename_String,objectname_String,field_]:=
	fetchField[filename,objectname,{field}];
fetchField[filename_String,bs_batchSetup,{field__}]:=
	fetchField[filename,bs["objectname"],{field}];
fetchField[filename_String,bs_batchSetup,field_]:=
	fetchField[filename,bs,{field}];


(*TODO: test this*)
fetchData[bs_batchSetup,nr_List:{},field_String:"data"]:=
	fetchField[bs["outputfilename"],bs["objectname"],{"output",field},nr];
fetchData[objectname_String,nr_List:{},field_String:"data",OptionsPattern[]]:=
	fetchData[generateBatchSetup[objectname,Directory->OptionValue[Directory]],nr,field];
Options[fetchData]={Directory->Directory[]};


(*TODO: test this*)
fetchIntermediateData[bs_batchSetup,nr_List:{},fieldname_String:"tmpdata"]:=
	fetchField[bs["tmpfilename"],bs["objectname"],{"output",fieldname},nr];
fetchIntermediateData[objectname_String,nr_List:{},fieldname_String:"tmpdata",OptionsPattern[]]:=
	fetchIntermediateData[generateBatchSetup[objectname,Directory->OptionValue[Directory]],nr,fieldname];
Options[fetchIntermediateData]={Directory->Directory[]};


fetchInputData[bs_batchSetup,fields_List:{}]:=
	(*this construct is needed, as no code is evaluated within the 
	first element of Block*)
	With[{sym=Evaluate@Symbol@bs["objectname"]},
		Block[{sym},
			loadInputData[bs];
			{#,sym["input"][#]}&/@
				If[fields=!={},fields,Cases[SubValues[sym],HoldPattern[sym["input"][x_]]:>x,{3}]]
		]
	];
fetchInputData[objectname_String,fields_List:{},OptionsPattern[]]:=
	fetchInputData[generateBatchSetup[objectname,Directory->OptionValue[Directory]],fields];
Options[fetchInputData]={Directory->Directory[]};


listInputDataFields[bs_batchSetup]:=
	(*this construct is needed, as no code is evaluated within the 
	first element of Block*)
	With[{sym=Evaluate@Symbol@bs["objectname"]},
		Block[{sym},
			loadInputData[bs];
			Cases[SubValues[sym],HoldPattern[sym["input"][x_]]:>x,{3}]
		]
	];
listInputDataFields[objectname_String,OptionsPattern[]]:=
	listInputDataFields[generateBatchSetup[objectname,Directory->OptionValue[Directory]]];
Options[listInputDataFields]={Directory->Directory[]};


(* 
	Note: These functions clutter the context with symbols,
	or add definitions to symbols. They should only be used within
	an appropriate scoping construct or not at all!!
*)


loadIntermediateData[bs_batchSetup]:=(
	Get[bs["tmpfilename"]];
	Symbol@bs["objectname"]
);
loadIntermediateData[objectname_String,OptionsPattern[]]:=
	loadIntermediateData[generateBatchSetup[objectname,Directory->OptionValue[Directory]]];
Options[loadIntermediateData]={Directory->Directory[]};


loadInputData[bs_batchSetup]:=(
	Get[bs["inputfilename"]];
	Symbol@bs["objectname"]
);
loadInputData[objectname_String,OptionsPattern[]]:=
	loadInputData[generateBatchSetup[objectname,Directory->OptionValue[Directory]]];
Options[loadInputData]={Directory->Directory[]};


loadData[bs_batchSetup]:=(
	Get[bs["outputfilename"]];
	Symbol@bs["objectname"]
);
loadData[objectname_String,OptionsPattern[]]:=
	loadData[generateBatchSetup[objectname,Directory->OptionValue[Directory]]];
Options[loadData]={Directory->Directory[]};


(* ::Section:: *)
(*Log progress*)


(*
changes:
 - moved method argument to options
 - method option now Append or Write
 - change order of arguments expr<>filename
*) 
printToFile[filename_String,expr__,options:OptionsPattern[]]:=
	Module[
		{
			fh,
			pw=OptionValue[PageWidth],
			method=OptionValue[Method]
		},
		method=If[!MemberQ[{Append,Write},method],Message[printToFile::wmeth];Append,method];
		With[{fo=Options[OpenAppend]},
			If[method===Append,
				fh=OpenAppend[filename, Method->Automatic, Evaluate@FilterRules[{options},fo]],
				fh=OpenWrite[filename, Method->Automatic, Evaluate@FilterRules[{options},fo]]
			];
		];
		If[fh=!=$Failed,
			SetOptions[fh, PageWidth->pw];
			Write[fh,ReleaseHold@expr];
			Close@fh,
			$Failed
		]
	];
Options[printToFile]=Join[Union[Options[OpenWrite],Options[OpenAppend]],{Method->Append}];
SetOptions[printToFile,PageWidth->Infinity];
SetAttributes[printToFile,HoldRest];
printToFile::wmeth="Method is unknown. Falling back to Append";


appendToLog[bs_batchSetup,expr__]:=
	bs["logfunction"][expr];
SetAttributes[appendToLog,HoldRest];


printLogHeaderGeneric[bs_batchSetup]:=
	Block[{},
		appendToLog[bs,"Running ",Check[FileNameTake@NotebookFileName[],"batch file."]];
		appendToLog[bs,DateString[]];
		appendToLog[bs,"Log file: ",bs["logfilename"]];
		appendToLog[bs,"Output data file: ",bs["outputfilename"]];
		appendToLog[bs,"Input data file:",bs["inputfilename"]]
	];


(*
	customize this function like this
	(note that this should be done before running setupLog):
	
	printLogHeader[bs_batchSetup]:=With[
		{print=bs["logfunction"]},
		printLogHeaderGeneric[bs];
		print["Parameters: ",parameters];
		print["Description: ",description];
	];
	DistributeDefinitions@printLogHeader;
*)
printLogHeader[bs_batchSetup]:=printLogHeaderGeneric[bs];


resetLogCounter[bs_batchSetup]:=
	setField[bs,"logcounter",0];
preIncrementLogCounter[bs_batchSetup]:=
	setField[bs,"logcounter",bs["logcounter"]+1];


(*note: returning expr instead of result would lead to expr being exectued twice!*)
logAbsoluteTiming[bs_batchSetup,expr_,pr___]:=
	Module[{time,result},
			{time,result}=AbsoluteTiming@expr;
			appendToLog[bs,StringForm[logAbsoluteTiming::logmsg,bs["logcounter"],N@time,pr]];
			(*return*)
			result
	];
SetAttributes[logAbsoluteTiming,HoldRest];	
logAbsoluteTiming::logmsg="n=`1`, `2`s";


logAndSave[bs_batchSetup,expr_,pr___] := 
	Module[{}, 
		preIncrementLogCounter@bs;
		saveData[bs,logAbsoluteTiming[bs,expr,pr],Field->Sequence["data",bs["logcounter"]]]
	]
SetAttributes[logAndSave,HoldRest];


logAndSaveIntermediate[bs_batchSetup,expr_,pr___] := 
	Module[{}, 
		preIncrementLogCounter@bs;
		saveIntermediateData[bs,logAbsoluteTiming[bs,expr,pr]]
	]
SetAttributes[logAndSaveIntermediate,HoldRest];


concurrentLogAndSave[bs_batchSetup,expr_,pr___]:= (
  CriticalSection[{nlock}, preIncrementLogCounter@bs];
  saveData[bs,logAbsoluteTiming[bs,expr,pr],Field->Sequence["data",bs["logcounter"]]]
);
SetAttributes[concurrentLogAndSave,HoldRest];


concurrentLogAndSaveIntermediate[bs_batchSetup,expr_,pr___,opts:OptionsPattern[saveIntermediateData]] := (
  CriticalSection[{nlock}, preIncrementLogCounter@bs];
	saveIntermediateData[bs,logAbsoluteTiming[bs,expr,pr],opts]
);
SetAttributes[concurrentLogAndSaveIntermediate,HoldRest];


(*TODO: the distribution to parallel kernels should be made optional/automatic*)
setupLog[bs_batchSetup,options:OptionsPattern[]]:=
 	With[{fn=bs["logfilename"]},
 		(*create wrapper for printToFile set bs["logfunction"]*)
 		printToLogFile[bs["objectid"]][exp__]:=
 			printToFile[fn,Hold@exp,FormatType->StandardForm,Method->Append];
 		SetAttributes[printToLogFile,HoldRest];
 		setField[bs,"logfunction",printToLogFile[bs["objectid"]]];

 		(*if parallel*)
 		If[Not[OptionValue[ParallelEvaluate]===False],
	 		shareFields[];
			DistributeDefinitions["BatchProcessor`"];
 		];
 		
 		(*print logheader at the beginning of file*)
 		printLogHeader[bs];
 	]
Options[setupLog]={ParallelEvaluate->Automatic};


cleanLogSetup[bs_batchSetup]:=(
	(*reset bs["logfunction"] and clear wrappers*)
	setField[bs,"logfunction",Print];	
	Clear@printToLogFile;
)


listDirectory[directory_String,form_String:"*"]:=
	{First@#, DateString@Last@#}&/@SortBy[Map[{FileNameTake@#, FileDate@#} &,FileNames[form, directory]], Last]//TableForm
	(*TableForm@Map[{FileNameTake@#, DateString@FileDate@#}&,FileNames[form,directory]]*)
listDirectory[bs_batchSetup,form_String:"*"]:=
	listDirectory[bs["directory"],form];


listObjectNames[directory_String,form_String:"*.log"]:=
	{First@#, DateString@Last@#}&/@SortBy[Map[{FileBaseName@#, FileDate@#} &,FileNames[form, directory]], Last]//TableForm
	(*TableForm@Map[{FileBaseName@#, DateString@FileDate@#}&,FileNames[form,directory]];*)
listObjectNames[bs_batchSetup,form_String:"*.log"]:=
	listObjectNames[bs["directory"],form];


formatInputData[dat_List]:=(Print[#[[1]] <> ": ", #[[2]]]&/@dat;)


(* ::Section::Closed:: *)
(*Package setup end*)


End[]

EndPackage[]
