(* Mathematica Test File *)

options=
	{"directory" -> "/tmp/", "prefix" -> "batch", 
 	"outputsuffix" -> "dat", "inputsuffix" -> "in", "logsuffix" -> "log",
  	"tmpdatainfix" -> "-tmp"};
bs = generateBatchSetup["foo",options];

Test[
	Map[bs,{"objectid", "directory", "outputsuffix", "inputsuffix", 
  		"logsuffix", "tmpdatainfix"}]
	,
	{"foo", "/tmp/", "dat", "in", "log", "-tmp"}
	,
	TestID->"BatchTest-20111027-R6H3K4"
]