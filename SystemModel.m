(* ::Package:: *)

(* ::Title:: *)
(*System model data structure*)


(* :Title: SystemModel.m  *)

(* :Mathematica Version: Mathematica 8 *)

(* :Author: Sebastian Hofer *)

(* :Package Version: 0.21 *)

(* :Discussion:

	This basically implements a simple version of StateSpaceModel, which is better suited for our purposes.
	A systemModel can directly be converted to a StateSpaceModel (and therefore also to a TransferFunctionModel). 
	Functions from the "Control Systems" package can't be overloaded due to being ReadProtected, but some functions 
	are defined as UpValues of systemModel. This includes StateSpaceModel and TransferFunction Model, so these can 
	be used directly to convert systemModel. Note that systemModel, in addition to what is contained in StateSpaceModel
	and TransferFunctionModel, may also hold information about the noise processes and the control cost function!
    
    A systemModel describes the following system:

    x'[t]=F.x[t]+G.u[t]+L.w[t]
    z[t]=H.x[t]+n[t]
    where x is the state vector, u is a determenistic input and w and n are noise processes obeying a normal distribution with w~\[ScriptCapitalN](0,W) and n~\[ScriptCapitalN](0,N) respectively. 
    The cross-correlation of the noise processes is given by M=E[w.n^T]. 
    The cost function for the corresponding LQ-Regulator is J=1/2 \[Integral]dt {x[t] Q (x^T)[t]+u[t]R (u^T)[t]},where Q,R\[GreaterEqual]0 are the state and control cost respecitvely.
*)

BeginPackage["SystemModel`",{"GeneralToolbox`"}]

systemModel::usage="systemModel[{{F,G,L,H,D},{W,N,M},{Q,R}}] defines the system model including measurement and feedback.
systemModel[{{F,L,H},{W,N,M}}] defines the measurement model only.";
checkSystemModel::usage = "checkSystemModel[{{F,G,L,H},{W,N,M},{Q,R}}] checks if dimensions of the model matrices are compatible. If the model is numerical, W,N,M,Q and R are checked for positivity.
checkSystemModel[sm] checks systemModel sm and returns sm if correct or aborts otherwise.";
systemModelTransform::usage = "systemModelTransform[sm,sx,sz] applies affine transforms sx, sz to system and output respectively.";
systemModelCompactify::usage = "systemModelCompactify[sm] removes spurious zero-columns/rows from systemModel."
systemModelConcatenate::usage = "systemModelConcatenate[sm1,sm2,...] concatenates system models without connecting them."
systemModelSeriesConnect::usage = "systemModelSeriesConnect[sm1,sm2,{output1,input1},...] connects outputs {output1,...} of systemModel sm1 to inputs {input1,...} of sm2."
systemModelReplacePart::usage = "systemModelReplacePart[sm,{\"spec\"->value}] replaces sm[\"spec\"] with value."
systemModelToStandardForm::usage = "systemModelToStandardForm[sm,StandardForm->stdf] converts systemModel sm to standard form stdf, where stdf=1,2.
stdf = 1: Convert to model with unit-gain noise input.
stdf = 2: Covert to model with unit-variance noise process."

systemModelSelectIO::usage = "systemModelSelectIO[sm,OutputChannels->spec] selects output channels according to spec. spec has to be a valid specification for Part.
Note that single channel specification have to be wrapped in braces, e.g. {i}."

(* Begin Private Context *)
Begin["`Private`"]


(* public functions *)
With[
	{
		(*internal systemModel structure*)
		sqp=Sequence[
			{F_List, G_List, L_List,H_List,DM_List:{}},
			{W_List,NM_List,M_List:{}},{Q_List,R_List,U_List:{}}],
		sqpl=Sequence[{"F", "G", "L","H","D"},{"W","N","M"},{"Q","R","U"}]
	},
	Unprotect[systemModel];Clear[systemModel];

	(*define systemModel*)
	systemModel/:systemModel[sqp]["F"]:=	(*state*)
		If[Length@F==0, ConstantArray[0,{#,#}&@If[Length@H==0,1,Dimensions[H][[2]]]],F];
	systemModel/:systemModel[sqp]["G"]:=	(*control*)
		If[Length@G==0, ConstantArray[0,{If[Length@F==0,1,Length@F],1}],G];
	systemModel/:systemModel[sqp]["L"]:=	(*process noise input*)
		If[Length@L==0, IdentityMatrix@If[Length@W==0,1,Length@W],L];
	systemModel/:systemModel[sqp]["H"]:=	(*measurement*)
		If[Length@H==0, ConstantArray[0,{1,If[Length@F==0,1,Length@F]}],H];
	systemModel/:systemModel[sqp]["D"]:=	(*transmission*)
		If[Length@DM==0, ConstantArray[0,{If[Length@H==0,1,Length@H],If[Length@G==0,1,Dimensions[G][[2]]]}],DM];

	systemModel/:systemModel[sqp]["W"]:=	(*process noise covariance*)
		If[Length@W==0, Message[systemModel::emptypnoise];ConstantArray[0,{#,#}]&@If[Length@L==0 || Dimensions[L][[2]]==0,1,Dimensions[L][[2]]],W];
	systemModel/:systemModel[sqp]["N"]:=	(*measurement noise covariance*)
		If[Length@NM==0,Message[systemModel::emptymnoise];ConstantArray[0,{#,#}]&@If[Length@H==0,1,Length@H],NM];
	systemModel/:systemModel[sqp]["M"]:=	(*noise cross-correlations*)
		If[Length@M==0, ConstantArray[0,{If[Length@L==0,1,Dimensions[L][[2]]],If[Length@H==0,1,Length@H]}],M];

	systemModel/:systemModel[sqp]["Q"]:=	(*state cost*)
		If[Length@Q==0,Message[systemModel::emptycarg];ConstantArray[0,{#,#}]&@Length@F,Q];
	systemModel/:systemModel[sqp]["R"]:=	(*control cost*)
		If[Length@R==0,Message[systemModel::emptycarg];
			ConstantArray[0,{#,#}]&@If[Length@G==0,1,Dimensions[G][[2]]],R];
	systemModel/:systemModel[sqp]["U"]:=	(*noise cross-correlations*)
		If[Length@U==0,
			If[Length@R==0,Message[systemModel::emptycarg]];
				ConstantArray[0,{Length@F,If[Length@G==0,1,Dimensions[G][[2]]]}],U];


	(*these should be treated as private, as they are subject to change!*)
	systemModel[x__]["statespacemodel"]:=Map[systemModel[x],{sqpl}[[1]]];
	systemModel[x__]["noisevariance"]:=Map[systemModel[x],{sqpl}[[2]]];
	systemModel[x__]["controlcostfunction"]:=Quiet[Map[systemModel[x],{sqpl}[[3]]],{systemModel::emptycarg}];
	systemModel[x__]["all"]:=Quiet[Map[systemModel[x],{sqpl},{2}],{systemModel::emptycarg}];

	systemModel[x__]["inports"]:=Dimensions[List[x][[1,2]]][[2]];
	systemModel[x__]["outports"]:=Length@List[x][[1,4]];
	systemModel[x__]["ports"]:={Dimensions[List[x][[1,2]]][[2]],Length@List[x][[1,4]]};

	(*constructor*)
	systemModel[ss_StateSpaceModel]:=systemModel[{{ss[[1,1]],ss[[1,2]],ConstantArray[0,{Dimensions[ss[[1,1]]][[1]],1}],ss[[1,3]],ss[[1,4]]},{{},{},{}},{{},{},{}}}];
	systemModel[{{F_List,L_List,H_List},{W_List,NM_List,M_List:{}}}]:=systemModel[{{F,{},L,H,{}},{W,NM,M},{{},{},{}}}];
	systemModel[{{F_List,L_List,H_List}}]:=systemModel[{{F,{},L,H,{}},{{},{},{}},{{},{},{}}}];
	systemModel[{sqp}]:=
		If[
			checkSystemModel[{{F,G,L,H,DM},{W,NM,M},{Q,R,U}}],
			systemModel[{F,G,L,H,DM},{W,NM,M},{Q,R,U}],
			Abort[]
		];

	checkSystemModel[sm_systemModel]:=
		If[TrueQ@checkSystemModel[List@@sm],sm,Message[checkSystemModel::nosm];Abort[]];
	checkSystemModel[{sqp}]:=Module[
		{f,g,l,h,w,n,m,q,r,u,d,msg},
		{f,g,l,h,w,n,m,q,r,u,d}=Dimensions/@{F,G,L,H,W,NM,M,Q,R,U,DM};

		Which[
			(*check dimensions*)
			Not@TrueQ[
				msg=Which[
					Not[SameQ@@f],"F not square",
					Not[SameQ@@w],"W not square",
					Not@Or[SameQ@@n,SameQ[{0},n]],"N not square",
					Not@Or[SameQ@@q,SameQ[{0},q]],"Q not square",
					Not@Or[SameQ@@r,SameQ[{0},r]],"R not square",

					Not@Or[SameQ[First@f,First@g],SameQ[{0},g],SameQ[{0},f]],"F incompatible with G",
					Not@Or[SameQ[First@f,First@l],SameQ[{0},l],SameQ[{0},f]],"F incompatible with L",
					Not@Or[SameQ[First@f,Last@h],SameQ[{0},h],SameQ[{0},f]],"H incompatible with F",
					Not@Or[SameQ[First@d,First@h],SameQ[{0},h],SameQ[{0},d]],"H incompatible with D",
					Not@Or[SameQ[Last@g,Last@d],SameQ[{0},g],SameQ[{0},d]],"G incompatible with D",

					Not@Or[SameQ[First@h,Last@n],SameQ[{0},h],SameQ[{0},n]],"H incompatible with N",
					Not@Or[SameQ[Last@l,First@w],SameQ[{0},l],SameQ[{0},w]],"L incompatible with W",
					Not@Or[SameQ[First@w,First@m],SameQ[{0},m]],"M incompatible with W",
					Not@Or[SameQ[Last@n,Last@m],SameQ[{0},m]],"M incompatible with N",

					Not@Or[SameQ[First@q,First@f],SameQ[{0},q]],"Q incompatible with F",
					Not@Or[SameQ[Last@r,Last@g],SameQ[{0},r],SameQ[{0},g]&&SameQ[{0},r]],"R incompatible with G",
					Not@Or[SameQ[First@q,First@u],SameQ[{0},u]],"U incompatible with Q",
					Not@Or[SameQ[Last@r,Last@u],SameQ[{0},u]],"U incompatible with R",

					True,True
				]
			],
			Message[systemModel::dimerr,msg];False,
			(*check positivity*)
			Not@TrueQ[
				msg=Which[
					(*only works for numeric matrices*)
					TrueQ@Not[And@@NumericQ/@Flatten@{NM,R,W,Q,L}],True,
					TrueQ@Not@Or[NM==={},PositiveDefiniteMatrixQ@NM],"N not positive definite.",
					TrueQ@Not@Or[W==={},L==={},positiveSemiDefiniteMatrixQ[L.W.Transpose@L]],"L.W.L' not positive semidefinite.",
					TrueQ@Not@Or[R==={},PositiveDefiniteMatrixQ@R],"R not positive definite.",
					TrueQ@Not@Or[Q==={},positiveSemiDefiniteMatrixQ@Q],"Q not positive semidefinite.",
					True, True
				]
			],
			Message[systemModel::posdef,msg];False,
			(*all checks passed*)
			True, True
		]
	];
	checkSystemModel::nosm="Not a valid systemModel";

	(*these functions can't be overloaded due to being ReadProtected*)
	systemModel/:StateSpaceModel[systemModel[x__]]:=toStateSpaceModel[systemModel[x],AppendNoiseInput->True];
	systemModel/:ObservableModelQ[sm_systemModel]:=ObservableModelQ@StateSpaceModel@sm;
	systemModel/:ControllableModelQ[sm_systemModel]:=ControllableModelQ@StateSpaceModel@sm;
	systemModel/:TransferFunctionModel[sm_systemModel]:=TransferFunctionModel@StateSpaceModel@sm;

	(*derived functions*)
	systemModel/:systemModel[seq__][str_List]:=Map[systemModel[seq],str];

	(*inspection*)
	systemModel/:systemModel[sqp]["controlledQ"]:=And[Q=!={},R=!={},G=!={}];

	systemModelReplacePart[sm_systemModel,rule_Rule]:=systemModelReplacePart[sm,{rule}];
	systemModelReplacePart[sm_systemModel,rules:{_Rule..}]:=
		checkSystemModel@
			ReplacePart[sm,Thread@Rule[Position[Hold@sqpl,#][[1]]&/@rules[[All,1]],rules[[All,2]]]];

	systemModel::emptycarg="No control has been defined for this systemModel.";
	systemModel::dimerr="Dimension error: `1`";
	systemModel::posdef="`1`";
	systemModel::emptypnoise="No process noise model has been defined for this systemModel.";
	systemModel::emptymnoise="No measurement noise model has been defined for this systemModel.";
]

Clear@systemModelCompactify;
systemModelCompactify[sm_systemModel]:=Module[
	{
		fhor,fver,
		hhor,hver,
		lver,gver,
		dhor,dver,
		stateinhor,stateinver,
		delrow,delcolumn,
		f,h,g,l,d,w,m,n,q,r
	},
	(*don't delete the last row*)
	delrow[mat_List,pos_List]:=ReplacePart[mat,Thread[If[Length@pos<Length@mat,pos,Most@pos] -> Sequence[]]];
	delcolumn[mat_List,pos_List]:=
		If[Length@mat!=0,
			Transpose@ReplacePart[Transpose@mat,Thread[If[Length@pos<Length@Transpose@mat,pos,Most@pos] -> Sequence[]]],
			{}];

	(*find zero columns/rows*)
	{fhor, fver} = Flatten@Position[#, {0 ..}] & /@ {#, Transpose@#} &@sm["F"];
  	{hhor, hver} = Flatten@Position[#, {0 ..}] & /@ {#, Transpose@#} &@sm["H"];
  	{dhor, dver} = Flatten@Position[#, {0 ..}] & /@ {#, Transpose@#} &@sm["D"];
  	lver = Flatten@Position[Transpose@sm["L"], {0 ..}];
  	gver = Flatten@Position[Transpose@sm["G"], {0 ..}];
  	{stateinhor, stateinver} = Flatten@Position[#, {0 ..}] & /@ {#, Transpose@#} &@ ArrayFlatten[{{sm["G"], sm["L"]}}];

  	(*delete matching zero-rows/columns*)
  	With[{compactf=Length@Intersection[fver, hver]==Length@Intersection[fhor, stateinhor]},
	  	f = If[compactf,delrow[delcolumn[sm["F"],Intersection[fver, hver]],Intersection[fhor, stateinhor]],sm["F"]];
	  	(*f = delrow[f,Intersection[fhor, stateinhor]];*)
	  	h = delrow[delcolumn[sm["H"],If[compactf,Intersection[fver, hver],{}]],Intersection[hhor, dhor]];
	  	(*h = delrow[h,Intersection[hhor, dhor]];*)
	  	g = delrow[delcolumn[sm["G"],Intersection[dver, gver]],If[compactf,Intersection[fhor, stateinhor],{}]];
	  	(*g = delrow[g,Intersection[fhor, stateinhor]];*)
	  	l = delrow[delcolumn[sm["L"],lver],If[compactf,Intersection[fhor, stateinhor],{}]];
	  	(*l = delrow[l,Intersection[fhor, stateinhor]];*)
	  	d = delrow[delcolumn[sm["D"],Intersection[dver, gver]],Intersection[hhor, dhor]];
	  	(*d = delrow[d,Intersection[hhor, dhor]];*)
  	];

  	(*if system is noisy*)
  	Quiet[
	  	w=Check[
		  	delrow[delcolumn[sm["W"],#],#]&@lver(*;w = delrow[w,lver]*),{},
		  	{systemModel::emptypnoise}
	  	];
	  	n=Check[
	  		delrow[delcolumn[sm["N"],#],#]&@Intersection[hhor, dhor],{},
		  	{systemModel::emptymnoise}
	  	];
	  	m=Check[
	  		{sm["W"],sm["N"]};
	  		delrow[delcolumn[sm["M"],Intersection[hhor, dhor]],lver],{},
		  	{systemModel::emptymnoise,systemModel::emptypnoise}
	  	],
	  	{systemModel::emptymnoise,systemModel::emptypnoise}
  	];

  	(*TODO: TEST THIS*)
  	Quiet[
	  	r=Check[
	  		delrow[delcolumn[sm["R"],#],#]&@Intersection[dver, gver],{},
		  	{systemModel::emptycarg}
	  	];
	  	q=Check[
	  		delrow[delcolumn[sm["Q"],#],#]&@Intersection[fhor, stateinhor],{},
		  	{systemModel::emptycarg}
	  	];
	  	{systemModel::emptycarg}
  	];

	(*return*)
  	systemModel[{{f,g,l,h,d},{w,n,m},{q,r,{}}}]
];


(*
	Note:
	- after concatenation zero matrices in noise and control matrices are replaced
	  with {} and are thus removed from the systemModel
*)
Clear@systemModelConcatenate;
systemModelConcatenate[sm__systemModel]:=Module[{f,g,l,h,d,w,m,n,q,r,u},
	Quiet[
		{{f,g,l,h,d},{w,n,m},{q,r,u}}=concatenateMatrices[sm,"all"],
		{systemModel::emptymnoise,systemModel::emptypnoise}
	];
	systemModelCompactify@systemModel[{{f,g,l,h,d},{w,n,m}/.{{(0)..}..}->{},{q,r,u}/.{{(0)..}..}->{}}]
];


Clear@systemModelSeriesConnect
systemModelSeriesConnect[sm1_systemModel,sm2_systemModel,options:OptionsPattern[]]:=
	systemModelSeriesConnect[sm1,sm2,Sequence@@Thread[{#,#}]&@Range@Min[sm1["outports"],sm2["inports"]],options];
systemModelSeriesConnect[sm__systemModel,options:OptionsPattern[]]:=
	Fold[systemModelSeriesConnect,First@{sm},Rest@{sm}];
systemModelSeriesConnect[sm1_systemModel,sm2_systemModel,spec:{_Integer,_Integer}..,OptionsPattern[]]:=Module[
	{
		inspec,outspec, (*I/O ports specs to connect*)
		mlenin,mlenout, (*number of I/O ports in sm*)
		h1connect,h1keep, (*partitioned output matrix*)
		d1connect,d1keep, (*partitioned input matrix*)
		g2connect,g2keeptrans, (*partitioned input matrix*)
		d2connect,d2keeptrans, (*partitioned input matrix*)
		n1connect,n1keep, (*partitioned measurement noise*)
		m1connect, m1keep, (*partitioned noise cross correlations*)
		tmat, (*measurement noise transfmartion*)
		fconed,hconed,gconed,lconed,dconed,wconed,nconed,mconed,rconed,qconed (*connected model*)
	},
		(*normalize I/O specifications*)
		{outspec,inspec}={#[[1;;,1]],#[[1;;,2]]}&@DeleteDuplicates@List@spec;
		mlenin=Length/@Transpose/@Through@{sm1,sm2}@"G";
		mlenout=Length/@Through@{sm1,sm2}@"H";
		(*check if I/O specifications are valid*)
		Which[
			Or@@Thread[Length@{outspec}>Flatten@{mlenin,mlenout}],Message[systemModelSeriesConnect::wnio,
				"Too many in- and/or outputs."];Abort[],
			Max@outspec>mlenout[[1]],Message[systemModelSeriesConnect::wnio,"Output ID too high."];Abort[],
			Max@inspec>mlenin[[2]],Message[systemModelSeriesConnect::wnio,"Input ID too high."];Abort[]
		];
		(*partition input system*)
		h1connect=sm1["H"][[outspec,;;]];
		h1keep=Delete[sm1["H"],List/@outspec];
		d1connect=sm1["D"][[outspec,;;]];
		d1keep=Delete[sm1["D"],List/@outspec];

		(*partition output system*)
		g2connect=sm2["G"][[;;,inspec]];
		g2keeptrans=Delete[Transpose@sm2["G"],List/@inspec];
		d2connect=sm2["D"][[;;,inspec]];
		d2keeptrans=Delete[Transpose@sm2["D"],List/@inspec];

		(*connect system*)
		fconed=ArrayFlatten[{{sm1["F"],0},{g2connect.h1connect,sm2["F"]}}];
		gconed=ArrayFlatten[{
			(*insert Sequence[] if no inputs are kept*)
			ReleaseHold@{sm1["G"],If[Length@g2keeptrans!=0,0,Hold@Sequence[]]},
			ReleaseHold@{g2connect.d1connect,If[Length@#!=0,Transpose@#,Hold@Sequence[]]&@g2keeptrans}}
		];
		lconed=ArrayFlatten[{{sm1["L"],0,0},{0,g2connect,sm2["L"]}}];
		hconed=If[Length@h1keep!=0,
			ArrayFlatten[{{h1keep,0},{d2connect.h1connect,sm2["H"]}}], (*at least one output is kept*)
			ArrayFlatten[{{d2connect.h1connect,sm2["H"]}}] (*all outputs are connected*)
		];
		dconed=ArrayFlatten[{
			(*insert Sequence[] if no inputs are kept*)
			ReleaseHold@If[Length@d1keep!=0,{d1keep,If[Length@d2keeptrans!=0,0,Hold@Sequence[]]},Hold@Sequence[]],
			ReleaseHold@{d2connect.d1connect,If[Length@#!=0,Transpose@#,Hold@Sequence[]]&@d2keeptrans}}
		];

		Quiet[
			(*partition noise*)
			n1connect=sm1["N"][[outspec,outspec]];
			n1keep=sm1["N"][[#,#]]&@listComplement[Range@Length@sm1["N"],outspec];
			m1connect=sm1["M"][[;;,outspec]];
			m1keep=sm1["M"][[;;,listComplement[Range@Length@sm1["N"],outspec]]];

			(*connect noise*)
			wconed=ArrayFlatten[{{sm1["W"],m1connect,0},{Transpose@m1connect,n1connect,0},{0,0,sm2["W"]}}];
			tmat=If[Length@h1keep!=0,
				(*at least one output is kept*)
				ArrayFlatten[{
					{0,IdentityMatrix@Length@h1keep,0},
					{d2connect,0,IdentityMatrix@Length@sm2["N"]}
				}],
				(*no outputs are kept*)
				ArrayFlatten[{
					{d2connect,IdentityMatrix@Length@sm2["N"]}
				}]
			];
			nconed=(#.ArrayFlatten[{{sm1["N"],0},{0,sm2["N"]}}].Transpose@#)&@tmat;
			mconed=If[Length@n1keep!=0,
				(*at least one output is kept*)
				ArrayFlatten[{
						{m1connect,m1keep,0},
						{n1connect,sm1["N"][[outspec,listComplement[Range@Length@sm1["N"],outspec]]],0},
						{0,0,sm2["M"]}
					}].Transpose@tmat,
				(*no outputs are kept*)
				ArrayFlatten[{{m1connect,0},{n1connect,0},{0,sm2["M"]}}].Transpose@tmat
			];

			(*control*)
			(*TODO: test this!*)
			r2keeptrans=Delete[Transpose@sm2["R"],List/@inspec];
			rconed=If[Length@r2keeptrans!=0,
				ArrayFlatten[{{sm1["R"][[outspec,;;]],0},{0,Transpose@r2keeptrans}}],
				sm1["R"]
			];
			qconed=ArrayFlatten[{{sm1["Q"],0},{0,sm2["Q"]}}];
			,
			{systemModel::emptypnoise,systemModel::emptymnoise,systemModel::emptycarg}
		];

		(*return*)
		(*replace zero-noise matrices so they don't show up in systemModel and compactify system*)
		If[OptionValue[Compactify],systemModelCompactify,Identity]@
			With[{zeropat={{0..}..}->{}},
				systemModel[{{fconed,gconed,lconed,hconed,dconed},{wconed,nconed,mconed}/.zeropat,{qconed,rconed,{}}/.zeropat}]]
]
Options[systemModelSeriesConnect]={Compactify->True};
systemModelSeriesConnect::wnio="Wrong number of input/output ports specified: `1`"


(*Note: U is not transformed*)
systemModelTransform[sm_systemModel,None,sy_List]:=
	systemModelTransform[sm,IdentityMatrix@Length@sm["F"],sy];
systemModelTransform[sm_systemModel,sx_List,None]:=
	systemModelTransform[sm,sx,IdentityMatrix@Length@sm["H"]];
systemModelTransform[sm_systemModel,sx_List,sy_List]:=
	Module[{f,g,h,l,n,m,q,r},
		{f,h,l}={sx.sm["F"].Inverse@sx,sy.sm["H"].Inverse@sx,sx.sm["L"]};
		{n,m}={sy.sm["N"].Transpose@sy,sm["M"].Transpose@sy};
		{g,q,r}=If[sm["controlledQ"],{sx.sm["G"],Transpose@sx.sm["Q"].sx,sm["R"]},{{},{},{}}];
		systemModel[{{f,g,l,h},{sm["W"],n,m},{q,r,{}}}]
	];

(*Implement as option for systemModel?*)
(*systemModelToStandardForm[sm_systemModel]:=
	systemModel[{
		{sm["F"],sm["G"],IdentityMatrix@Length@Transpose@sm["L"],sm["H"]},
		{sm["L"].sm["W"].Transpose@sm["L"],sm["N"],sm["L"].sm["M"]},
   		If[sm["controlQ"], {sm["Q"], sm["R"]}, {{}, {}}]
   	}];
*)
systemModelToStandardForm[sm_systemModel,OptionsPattern[{StandardForm->1}]]:=
	systemModel[
	Switch[OptionValue[StandardForm],
		1,
		{{sm["F"], sm["G"],sm["L"].PseudoInverse@sm["L"],sm["H"]},
		{sm["L"].sm["W"].Transpose@sm["L"], sm["N"],sm["L"].sm["M"]},
   		If[sm["controlledQ"], {sm["Q"], sm["R"]}, {{}, {}}]},
		2,
		{{sm["F"], sm["G"],sm["L"].matrixSqrt@sm["W"],sm["H"]},
		{sm["W"].PseudoInverse@sm["W"], sm["N"],PseudoInverse[matrixSqrt@sm["W"]].sm["M"]},
   		If[sm["controlledQ"], {sm["Q"], sm["R"]}, {{}, {}}]}
	]
]


systemModelSelectIO[sm_systemModel,OptionsPattern[]]:=
	Module[{out=OptionValue[OutputChannels],in=OptionValue[InputChannels],nin=OptionValue[NoiseInputChannels]},
		systemModelReplacePart[sm,{
			"H"->sm["H"][[out]],"G"->sm["G"][[All,in]],"L"->sm["L"][[All,nin]],"D"->sm["D"][[out,in]],
			"W"->sm["W"][[nin,nin]],"N"->sm["N"][[out,out]],"M"->sm["M"][[nin,out]],
			If[sm["controlledQ"],"R"->sm["R"][[in,in]],Unevaluated@Sequence[]]
		}]
	];
Options[systemModelSelectIO] = {OutputChannels->All,InputChannels->All,NoiseInputChannels->All};


(*private functions*)
Clear@matrixSqrt;
matrixSqrt[mat_List]:= Module[{e,v},
	(*for symmetric matrix mat, mat==matrixSqrt[mat].matrixSqrt[mat]*)
	If[Not@SymmetricMatrixQ@mat,Message[matrixSqrt::nsym]];
	(*mat=Transpose[v].DiagonalMatrix[e].v*)
	{e,v}=Reverse/@Eigensystem[mat];
	(*for exact values, vectors are not normalized*)
	v=v/Norm/@v;
	Transpose@v.Sqrt@DiagonalMatrix[e].v
]
matrixSqrt::nsym="Matrix not symmetric.";

(*
	Note
	- blockDiagonalMatrix has to be treated as a function which takes a sequence as an argument
	  for MapThread to work correctly, thus the Hold
	- possible specs are: "all", "statespacemodel", "noisevariance", "controlcostfunction"
*)
concatenateMatrices[sm__systemModel,spec_String:"all"]:=
	ReleaseHold@Map[If[spec==="all",Thread,Identity],MapThread[Hold@blockDiagonalMatrix,Through@{sm}@spec]];


(*systemModel can directly be converted to a StateSpaceModel*)
Clear@toStateSpaceModel;
toStateSpaceModel[sm_systemModel,OptionsPattern[{AppendNoiseInput->False}]]:=
	If[OptionValue[AppendNoiseInput] && Not@MatchQ[Flatten@sm["L"],{0..}],
		StateSpaceModel[{sm["F"],ArrayFlatten[{{sm["G"],sm["L"]}}],sm["H"],
			ArrayFlatten[{{sm["D"],ConstantArray[0,{Length@sm["D"],Dimensions[sm["L"]][[2]]}]}}]}],
		StateSpaceModel[{sm["F"],sm["G"],sm["H"],sm["D"]}]
	];	
		
(*formating functions*)
gridBoxFormat[ls_List,{sr_List,sc_List},super_:""]:=gridBoxFormat[ls,{False,{sr,sc}},super];
gridBoxFormat[ls_List,{def_,{sr_List,sc_List}},super_:""]:= 
	Module[{rd, cd},
		rd = ReplacePart[ConstantArray[def, First@Dimensions@ls + 1], sr];
		cd = ReplacePart[ConstantArray[def, Last@Dimensions@ls + 1], sc];
		DisplayForm@SuperscriptBox[MatrixForm@List@
      		GridBox[Developer`FromPackedArray@ls, 
       			GridBoxDividers -> {"Rows" -> rd, "Columns" -> cd}],
       		super
		]
	];

covMatrixFormat[{A_List,B_List,C_List},super_:""]:=
	gridBoxFormat[
		ArrayFlatten@{{A,C},{Transpose@C,B}},
		{{(Length@A+1)->True},{(Length@Transpose@A+1)->True}},
		super
	];

(*
	WARNING:
	The pattern in format must be defined very carefully and
	as specific as possible! (Infinite recursions may occur
	for example. In this specific case, DownValues[systemModel]
	made problems.)
*)
nEmptyQ[list_List]:=Not[list === {}];
Format[sm_systemModel/;MatchQ[sm,systemModel[{__List},{{}..},{{}..}]]]:=StateSpaceModel@sm;
Format[sm_systemModel/;MatchQ[sm,systemModel[{__List},{__List},{{}..}]]]:=
	Quiet[
		Flatten[
			List[
				StateSpaceModel@sm,
				covMatrixFormat[{sm["W"],sm["N"],sm["M"]},"\[ScriptCapitalN]"]
			],
			{1}
		],
	systemModel::emptymnoise];
Format[sm_systemModel/;MatchQ[sm,systemModel[{__List},{__List},{_List?nEmptyQ..,_List}]]]:=Flatten[
	List[
		StateSpaceModel@sm,
		covMatrixFormat[{sm["W"],sm["N"],sm["M"]},"\[ScriptCapitalN]"],
		covMatrixFormat[{sm["Q"],sm["R"],sm["U"]},"\[ScriptCapitalC]"]
	],
	{1}
];


Protect[systemModel];

End[] (* End Private Context *)

EndPackage[]
