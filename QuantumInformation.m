(* ::Package:: *)

(* :Title: QuantumInformation.m  *)

(* :Mathematica Version: Mathematica 8 *)

(* :Author: Sebastian Hofer *)

(* :Discussion:

Asorted QI functions for continuous variable systems. We assume that:

- the quadratures are ordered (X_1,P_1,X_2,P_2,...) such that J={{0,1},{-1,0}}
- the ground state has a variance of 1/1 (equivalent to the commutator relations above)

*)

BeginPackage["QuantumInformation`", {"GeneralToolbox`"}]

physicalStateQ::usage="physicalStateQ[cm] returns True iff input is bona fide covariance matrix, i.e. CM+\!\(\*FractionBox[\(\[ImaginaryI]\), \(2\)]\)J\[GreaterEqual]0, where J is the symplectic matrix. CM must be normalized such that shot noise unit is 1/2.
This only works with numeric matrices! With option Verbose\[Rule]True sow eigenvalues.";

logarithmicNegativity::usage="logarithmicNegativity[cm] calculates logarithmic Negativity of given bipartite covariance matrix. (Note that it is not bounded by 0 from below!)";

gaussianPurity::usage = "gaussianPurity[cm] gives the purity of the Gaussian state described by the covariance matrix cm. The option CommutationRelations specifies [X,P]=i*OptionValue[CommutationRelations].";

eprVariance::usage = "eprVariance[V] calculates EPR variance, maximized through passive local transformations (=rotations). Optimal basis is found diagonalization of the correlation block by SVD.
With option Verbose\[Rule]True, return transformation matrix T, where \!\(\*SuperscriptBox[\(T\), \(T\)]\).V.T is in the optimal basis. For canonical commutation relations [x,p]=i, a Gaussian state is entangled if eprVariance is smaller than 2. This gives a sufficient criterion for entanglement."

duanVariance::usage = "duanVariance[V] calculates EPR variance, maximized through local Bogolubov transformations (=rotations + squeezing) + global squeezing.
For canonical commutation relations [x,p]=i, a Gaussian state is entangled iff duanVariance is smaller than 2. This gives a sufficient and necessary criterion for entanglement."

nptCriterion::usage = "nptCriterion[cm,mode] returns True if the (multipartite) covariance matrix cm is 'negative partial transpose' with respect to partial transposition of mode (defaults to 1).
With option Verbose\[Rule]True, return symplectic eigenvalues of partially transposed matrix. If at least one eigenvalue is negative, nptCriterion returns True."

symplecticMatrix::usage = "symplecticMatrix[modes] returns the skew-symmetric symplectic invariant for given number of modes.";
symplecticEigenvalues::usage = "symplecticEigenvalues[matrix, Method->method] gives the symplectic eigenvalues of
matrix. method can be either \"Numeric\" or \"Analytic\"w";
partialTranspose::usage = "partialTranspose[matrix, mode] gives the partial transposition of matrix with respect to
mode (integer).";


Begin["`Private`"] (* Begin Private Context *)


symplecticMatrix[modes_Integer]:=blockDiagonalMatrix@ConstantArray[{{0,1},{-1,0}},modes];


(*Test for bona fide covariance matrix, i.e. cov +IJ >= 0;*)
(*This must be explicitely fulfiled, i.e. it only works with numeric matrices!*)
Clear@physicalStateQ;
physicalStateQ[cov_?MatrixQ,options:OptionsPattern[]]:=Module[{J,m},
	J = symplecticMatrix[Length[cov]/2];
	m = cov+I*J/2;
	(*return*)
	(*m must be positive-SEMI-definite! if Verbose\[Rule]True always calculate and sow eigenvalues*)
	positiveSemiDefiniteMatrixQ[m,options]];
Options@physicalStateQ={Verbose->False};


logarithmicNegativity[cov_?MatrixQ, opts: OptionsPattern[]]/;Dimensions[cov]=={4,4}:=
	-Log[2*Min@symplecticEigenvalues[partialTranspose@cov, opts]]
Options[logarithmicNegativity] = {Method->"Eigenvalues"};


symplecticEigenvaluesEigenvalues[cov_?MatrixQ] :=
    With[{J = symplecticMatrix[Length[cov]/2]}, Abs@Eigenvalues[I*J.cov]];
symplecticEigenvaluesAnalytic[cov_?MatrixQ]/;Dimensions[cov]=={4,4} :=
    Flatten[{#,#}]&[{Sqrt[#+Sqrt[#^2-4Det@cov]]/Sqrt@2, Sqrt[#-Sqrt[#^2-4Det@cov]]/Sqrt@2}&[Det@Take[cov,2,2]+Det@Take[cov,-2,
      -2]+2Det@Take[cov,-2,2]]];
symplecticEigenvalues[cov_?MatrixQ, OptionsPattern[]] :=
    Switch[OptionValue[Method],
      "Eigenvalues", symplecticEigenvaluesEigenvalues[cov],
      "Analytic", symplecticEigenvaluesAnalytic[cov]];
Options[symplecticEigenvalues] = {Method->"Eigenvalues"};


partialTranspose[mat_?MatrixQ, mode_Integer: 1] := Module[{ptm},
  ptm = DiagonalMatrix@ReplacePart[ConstantArray[1,{Length@mat}],2*mode->-1];
  ptm.mat.Transpose@ptm
]


Clear@nptCriterion
nptnot[x_]=Not@x; nptnot[{x_,l_List}]={Not@x,l};
nptCriterion[cm_?MatrixQ,mode_Integer]/;(2*mode>Length@cm):=(Message[nptCriterion::wdim];$Aborted)
nptCriterion[cm_?MatrixQ,mode_Integer:1,options:OptionsPattern[]]:= nptnot@physicalStateQ[partialTranspose@cm,options]
Options[nptCriterion]={Verbose->False};
nptCriterion::wdim = "2*mode must be smaller than Length[cm].";


gaussianPurity[cov_?MatrixQ,OptionsPattern[]]:=(OptionValue[CommutationRelations]/2)^(Length@cov/2)/Sqrt@Det@cov;
Options@gaussianPurity={CommutationRelations->1};


(*custom sign function: needs to be 1 for argument 0!*)
sign = (UnitStep@#-1/2)*2&;

Clear@eprVariance
eprVariance[V_?MatrixQ,OptionsPattern[]]:=Module[{u,w,v,s},
	(*s = (UnitStep@Det@V[[1;;2,3;;4]]-1/2)*2;*)
	s = sign@Det@V[[1;;2,3;;4]];
	{u,w,v} = SingularValueDecomposition@V[[1;;2,3;;4]]; (*find optimally correlated basis*)
	v = {1,s}*Transpose@v//Transpose; w={1,s}*w; (*correct for possible reflection by SVD*)
	(*return*)
	(*normalized such that entangled state has epr variance of <2!*)
	If[OptionValue@Verbose,{#,blockDiagonalMatrix[u,v]},#,#]&@
		(Tr@#-2#[[1,3]]+2#[[2,4]]&)@
			ArrayFlatten[{{Transpose@u.V[[1;;2,1;;2]].u,w},{w,Transpose@v.V[[3;;4,3;;4]].v}}]
]
Options@eprVariance={Verbose->False};


(*transforms (numeric) covariance matrix to Duan standard form 1*)
Clear@standardFormOne
standardFormOne[V_?MatrixQ]:=Module[{sq,diagMat,offDiagMat},
	If[Or@@ExactNumberQ/@Flatten@V,Message[standardFormOne::exnum]];
	sq = Apply[{{#,0},{0,1/#}}&@(#2/#1)^(1/4)&,Eigenvalues@#]&; (*squeezing of diagonal blocks*)
	diagMat = sq@#.DiagonalMatrix@Eigenvalues@#.sq@#&; (*rotation + squeezing of diagonal blocks*)
	offDiagMat = (sq@#).(Normalize/@Eigenvectors@#)&; (*diagonalize off-diagonal blocks*)
	(*return*)
	With[{
		am=diagMat@V[[1;;2,1;;2]],
		bm=diagMat@V[[3;;4,3;;4]],
		cm=({1,sign@Det@#}*DiagonalMatrix@SingularValueList[#]&)
			[offDiagMat@V[[1;;2,1;;2]].V[[1;;2,3;;4]].Transpose@offDiagMat@V[[3;;4,3;;4]]]
		},
		ArrayFlatten[{{am,cm},{Transpose@cm,bm}}
	]
]];


(*check if (numerics) covariance matrix is of Duan standard form 2*)
Clear@standardFormTwoQ
standardFormTwoQ[V_?MatrixQ,OptionsPattern[]]:=Module[{n1,m1,n2,m2,c1,c2},
	{n1,n2,m1,m2} = Diagonal@V;
	{c1,c2} = Diagonal@V[[1;;2,3;;4]];
	If[Or@@ExactNumberQ/@{n1,m1,n2,m2,c1,c2},Message[standardFormTwoQ::exnum]];
	(*return*)
	OptionValue[ZeroTest][(n1-1)/(m1-1)-(n2-1)/(m2-1)]&& (*definition of std form 2*)
		OptionValue[ZeroTest][Abs[c1]-Sqrt[(n1-1)(m1-1)]-Abs[c2]+Sqrt[(n2-1)(m2-1)]]
];
Options@standardFormTwoQ={ZeroTest->PossibleZeroQ};


(*transforms (numeric) covariance matrix to Duan standard form 2*)
Clear@standardFormTwo
standardFormTwo[V_?MatrixQ,options:OptionsPattern[NSolve]]:=Module[{V1,n,m,c,d,r1,r2,equs},
	If[Or@@ExactNumberQ/@Flatten@V,Message[standardFormTwo::exnum]];
	V1 = standardFormOne@V;
	{n,m,c,d} = Extract[V1,{{1,1},{3,3},{1,3},{2,4}}];
	equs = ((n/r1-1)/(n r1-1)==(m/r2-1)/(m r2-1))&& (*calculated required squeezing parameters*)
		(r1 r2 Abs@c-Abs@d==Sqrt[r1 r2]Sqrt[(n r1-1)(m r2-1)]-Sqrt[(n-r1)(m-r2)]);
	(*return*)
	#.V1.#&@Sqrt@DiagonalMatrix[{r1,1/r1,r2,1/r2}]/.NSolve[equs&&r1>0&&r2>0,{r1,r2},options][[1]]
]
Options@standardFormTwo=Options@NSolve;
standardFormTwo::exnum="Expressions involving exact numbers will be numericized automatically.";


duanVariance[V_?MatrixQ,options:OptionsPattern[]]:=Module[{V2,a0},
	V2 = If[standardFormTwoQ@V,V,standardFormTwo[V,options]];(*standardFormTwoQ checks for exact numbers*)
	a0 = Sqrt[(V2[[3,3]]-1)/(V2[[1,1]]-1)];(*a0^2 from the paper*)
	(*return*)
	(*normalized such that entangled state has duan variance of <2!*)
	(*note that there is a mistake in equ. (16) in the paper, which is missing a factor 2!*)
	(a0(V2[[1,1]]+V2[[2,2]])+(V2[[3,3]]+V2[[4,4]])/a0-2(Abs@V2[[1,3]]+Abs@V2[[2,4]]))/(a0+1/a0)*2
]


General::exnum="Expression involves exact numbers, result may be inaccurate. Try numericizing the expression first.";
General::obssym="This function is obsolete! Please change this to `1`!";


End[] (* End Private Context *)

EndPackage[]
